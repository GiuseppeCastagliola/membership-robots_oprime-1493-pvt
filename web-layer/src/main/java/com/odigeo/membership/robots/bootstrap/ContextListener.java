package com.odigeo.membership.robots.bootstrap;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.odigeo.commons.config.files.ConfigurationFilesManager;
import com.odigeo.commons.config.files.PropertiesLoader;
import com.odigeo.commons.messaging.KafkaTopicHealthCheck;
import com.odigeo.commons.monitoring.dump.ConfiguredBeanDumpState;
import com.odigeo.commons.monitoring.dump.DumpServlet;
import com.odigeo.commons.monitoring.dump.DumpStateRegistry;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckServlet;
import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.monitoring.dump.AutoDumpStateRegister;
import com.odigeo.commons.rest.monitoring.healthcheck.AutoHealthCheckRegister;
import com.odigeo.membership.client.enhanced.MembershipSearchApiModule;
import com.odigeo.membership.client.enhanced.MembershipServiceModule;
import com.odigeo.membership.offer.client.MembershipOfferServiceModule;
import com.odigeo.membership.robots.bootstrap.bookingapi.BookingApiServiceModule;
import com.odigeo.membership.robots.bootstrap.bookingapi.BookingSearchApiServiceModule;
import com.odigeo.membership.robots.configuration.MembershipRobotsModule;
import com.odigeo.membership.robots.consumer.BookingIdQueueConsumerFactory;
import com.odigeo.membership.robots.consumer.MembershipBookingTrackingProcessorFactory;
import com.odigeo.membership.robots.consumer.MembershipProcessorFactory;
import com.odigeo.robots.RobotFrameworkModule;
import com.odigeo.robots.RobotsBootstrap;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jboss.vfs.VFS;
import org.jboss.vfs.VFSUtils;
import org.jboss.vfs.VirtualFile;
import org.reflections.Reflections;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.function.BiFunction;

import static com.edreams.configuration.ConfigurationEngine.getInstance;
import static com.edreams.configuration.ConfigurationEngine.init;

public class ContextListener implements ServletContextListener {

    private static final String LOG4J_FILENAME = "/log4j.properties";
    private static final long LOG4J_WATCH_DELAY_MS = 1800000L;
    private static final Reflections REFLECTIONS = new Reflections("com.odigeo.membership.robots");
    private static final String CONNECTION_TIMEOUTS_PROPERTY_PATH = "/com/odigeo/rest/utils/url/configuration/ConnectionTimeouts.properties";
    private static final BiFunction<Properties, String, Integer> PROPERTY_TO_INT_FUNC = (prop, s) -> Integer.parseInt(prop.getProperty(s));

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();
        final HealthCheckRegistry baseCheckerRegistry = new HealthCheckRegistry();
        baseCheckerRegistry.register("Kafka BOOKING_UPDATES_v1 topic", new KafkaTopicHealthCheck("BOOKING_UPDATES_v1"));
        baseCheckerRegistry.register("Kafka CHARGEBACK_MOVEMENT_STORED topic", new KafkaTopicHealthCheck("CHARGEBACK_MOVEMENT_STORED"));
        final DumpStateRegistry dumpStateRegistry = new DumpStateRegistry();
        REFLECTIONS.getTypesAnnotatedWith(ConfiguredInPropertiesFile.class)
                .forEach(properties -> dumpStateRegistry.add(new ConfiguredBeanDumpState(properties)));
        final AutoHealthCheckRegister autoHealthCheckRegister = new AutoHealthCheckRegister(baseCheckerRegistry);
        final AutoDumpStateRegister autoDumpStateRegister = new AutoDumpStateRegister(dumpStateRegistry);

        servletContext.log("Bootstrapping .....");
        Logger logger = initLog4J(servletContext);
        initConfigurationEngine(autoHealthCheckRegister, autoDumpStateRegister);
        servletContext.setAttribute(DumpServlet.REGISTRY_KEY, dumpStateRegistry);
        servletContext.setAttribute(HealthCheckServlet.REGISTRY_KEY, baseCheckerRegistry);
        initMetrics();
        initRobotFramework();
        logger.info("Bootstrapping finished!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        getInstance(RobotsBootstrap.class).shutdownScheduler();
        event.getServletContext().log("context destroyed");
    }

    private static String getAppName() {
        try {
            return InitialContext.doLookup("java:app/AppName");
        } catch (NamingException e) {
            throw new IllegalStateException("cannot get AppName", e);
        }
    }

    private void initConfigurationEngine(ServiceNotificator... serviceNotificators) {
        URL configurationFileUrl = new ConfigurationFilesManager().getConfigurationFileUrl(CONNECTION_TIMEOUTS_PROPERTY_PATH);
        Properties connectionTimeoutsProperty;
        try {
            connectionTimeoutsProperty = new PropertiesLoader().loadPropertiesFromDisk(configurationFileUrl);
        } catch (IOException e) {
            throw new IllegalStateException("Connections timeout properties not found, module can't be initialized", e);
        }
        int membershipConnTimeout = PROPERTY_TO_INT_FUNC.apply(connectionTimeoutsProperty, "membershipConnTimeout");
        int membershipSockTimeout = PROPERTY_TO_INT_FUNC.apply(connectionTimeoutsProperty, "membershipSockTimeout");
        int bookingApiConnTimeout = PROPERTY_TO_INT_FUNC.apply(connectionTimeoutsProperty, "bookingApiConnTimeout");
        int bookingApiSockTimeout = PROPERTY_TO_INT_FUNC.apply(connectionTimeoutsProperty, "bookingApiSockTimeout");
        init(new RobotFrameworkModule(),
                new MembershipRobotsModule(),
                new MembershipSearchApiModule.Builder()
                        .withConnectionTimeoutInMillis(membershipConnTimeout)
                        .withSocketTimeoutInMillis(membershipSockTimeout)
                        .build(serviceNotificators),
                new MembershipServiceModule.Builder()
                        .withConnectionTimeoutInMillis(membershipConnTimeout)
                        .withSocketTimeoutInMillis(membershipSockTimeout)
                        .build(serviceNotificators),
                new MembershipOfferServiceModule.Builder()
                        .withConnectionTimeoutInMillis(membershipConnTimeout)
                        .withSocketTimeoutInMillis(membershipSockTimeout)
                        .build(serviceNotificators),
                new BookingApiServiceModule(bookingApiConnTimeout, bookingApiSockTimeout, serviceNotificators),
                new BookingSearchApiServiceModule(bookingApiConnTimeout, bookingApiSockTimeout, serviceNotificators),
                new FactoryModuleBuilder().build(BookingIdQueueConsumerFactory.class),
                new FactoryModuleBuilder().build(MembershipProcessorFactory.class),
                new FactoryModuleBuilder().build(MembershipBookingTrackingProcessorFactory.class)
        );
    }

    private Logger initLog4J(ServletContext servletContext) {
        ConfigurationFilesManager configurationFilesManager = new ConfigurationFilesManager();
        VirtualFile virtualFile = VFS.getChild(configurationFilesManager.getConfigurationFileUrl(LOG4J_FILENAME, this.getClass()).getFile());
        URL fileRealURL;
        try {
            fileRealURL = VFSUtils.getPhysicalURL(virtualFile);
            PropertyConfigurator.configureAndWatch(fileRealURL.getFile(), LOG4J_WATCH_DELAY_MS);
            servletContext.log("Log4j has been initialized with config file " + fileRealURL.getFile() + " and watch delay of " + (LOG4J_WATCH_DELAY_MS / 1000) + " seconds");
        } catch (IOException e) {
            throw new IllegalStateException("Log4j cannot be initialized: file " + LOG4J_FILENAME + " cannot be load", e);
        }
        return Logger.getLogger(ContextListener.class);
    }

    private void initRobotFramework() {
        getInstance(RobotsBootstrap.class).initScheduler("membership-com");
    }

    private void initMetrics() {
        MetricsManager.getInstance().addMetricsReporter(getAppName());
        MetricsManager.getInstance().changeReporterStatus(ReporterStatus.STARTED);
    }
}
