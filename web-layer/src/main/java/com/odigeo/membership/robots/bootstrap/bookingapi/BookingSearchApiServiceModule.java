package com.odigeo.membership.robots.bootstrap.bookingapi;

import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public class BookingSearchApiServiceModule extends AbstractRestUtilsModule {

    private final int connectionTimeout;
    private final int socketTimeout;

    public BookingSearchApiServiceModule(int connectionTimeout, int socketTimeout, ServiceNotificator... serviceNotificators) {
        super(BookingSearchApiService.class, serviceNotificators);
        this.connectionTimeout = connectionTimeout;
        this.socketTimeout = socketTimeout;
    }

    @Override
    protected ServiceConfiguration getServiceConfiguration(Class aClass) {
        return ServiceConfigurationBuilder.setupBookingService(BookingSearchApiService.class, connectionTimeout, socketTimeout);
    }
}
