package com.odigeo.membership.robots.bootstrap.bookingapi;

import com.odigeo.bookingapi.v12.BookingApiService;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public class BookingApiServiceModule extends AbstractRestUtilsModule {

    private final int connectionTimeout;
    private final int socketTimeout;

    public BookingApiServiceModule(int connectionTimeout, int socketTimeout, ServiceNotificator... serviceNotificators) {
        super(BookingApiService.class, serviceNotificators);
        this.connectionTimeout = connectionTimeout;
        this.socketTimeout = socketTimeout;
    }

    @Override
    protected ServiceConfiguration getServiceConfiguration(Class aClass) {
        return ServiceConfigurationBuilder.setupBookingService(BookingApiService.class, connectionTimeout, socketTimeout);
    }
}
