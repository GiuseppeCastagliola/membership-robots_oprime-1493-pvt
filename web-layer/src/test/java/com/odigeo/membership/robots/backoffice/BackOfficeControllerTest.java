package com.odigeo.membership.robots.backoffice;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.robots.consumer.BookingUpdatesMessageProcessor;
import com.odigeo.membership.robots.discard.MembershipDiscardService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.openMocks;

public class BackOfficeControllerTest {
    private static final List<Long> IDS = Arrays.asList(1L, 2L, 3L);
    @Mock
    private MembershipDiscardService membershipDiscardService;
    @Mock
    private BookingUpdatesMessageProcessor bookingUpdateMessageProcessor;
    private BackOfficeController backOfficeController;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        ConfigurationEngine.init(this::configure);
        backOfficeController = new BackOfficeController();
    }

    @Test
    public void discardMembershipsTest() {
        backOfficeController.discardMemberships(IDS);
        verify(membershipDiscardService).discardMemberships(eq(IDS));
    }

    @Test
    public void processBookingsTest() {
        backOfficeController.processBookings(IDS);
        verify(bookingUpdateMessageProcessor, times(IDS.size())).putBookingIdInProcessingQueue(anyLong());
    }

    private void configure(Binder binder) {
        binder.bind(MembershipDiscardService.class).toInstance(membershipDiscardService);
        binder.bind(BookingUpdatesMessageProcessor.class).toInstance(bookingUpdateMessageProcessor);
    }
}
