package com.odigeo.membership.robots.utils.mappers;

import com.odigeo.membership.request.tracking.MembershipBookingTrackingRequest;
import com.odigeo.membership.response.tracking.MembershipBookingTracking;
import org.mapstruct.Mapper;

@Mapper
public interface MembershipBookingTrackingMapper {

    MembershipBookingTracking fromRequestToTrackingResponse(MembershipBookingTrackingRequest membershipBookingTrackingRequest);
}
