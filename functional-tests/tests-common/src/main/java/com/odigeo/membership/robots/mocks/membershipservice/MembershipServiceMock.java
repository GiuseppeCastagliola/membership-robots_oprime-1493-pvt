package com.odigeo.membership.robots.mocks.membershipservice;

import com.google.common.collect.ImmutableMap;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.product.ActivationRequest;
import com.odigeo.membership.request.product.AddToBlackListRequest;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.request.product.UpdateMemberAccountRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.request.tracking.MembershipBookingTrackingRequest;
import com.odigeo.membership.response.BlackListedPaymentMethod;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipRenewal;
import com.odigeo.membership.response.MembershipStatus;
import com.odigeo.membership.response.WebsiteMembership;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.response.tracking.MembershipBookingTracking;
import com.odigeo.membership.robots.mocks.MockException;
import com.odigeo.membership.robots.mocks.ServiceMock;
import com.odigeo.membership.robots.mocks.membershipsearchapi.MembershipSearchApiEndpointName;
import com.odigeo.membership.robots.utils.FunctionalUtils;
import com.odigeo.membership.robots.utils.mappers.MembershipBookingTrackingMapper;
import com.odigeo.membership.robots.utils.mappers.MembershipMapper;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceEndpointName.ACTIVATE_MEMBERSHIP;
import static com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceEndpointName.CREATE_PENDING_TO_COLLECT;
import static com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceEndpointName.DELETE_BOOKING_TRACKING;
import static com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceEndpointName.GET_BOOKING_TRACKING;
import static com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceEndpointName.SAVE_BOOKING_TRACKING;
import static com.odigeo.membership.robots.utils.FunctionalUtils.MembershipStatus.EXPIRED;
import static com.odigeo.membership.robots.utils.FunctionalUtils.MembershipStatus.PENDING_TO_COLLECT;
import static java.util.Collections.singletonList;

@SuppressWarnings("PMD.TooManyStaticImports")
public class MembershipServiceMock extends ServiceMock<MembershipServiceEndpointName> implements MembershipService {

    private static final long REQUEST_TIMEOUT = 500L;
    private static final String MEMBERSHIP_TO_UPDATE_NOT_FOUND = "Membership to %s was not found";
    private static final String MULTIPLE_MEMBERSHIPS_FOUND = "Multiple memberships found for id ";
    private static final String RECURRING_ID = "RECID123";
    private static final String DEACTIVATED_STATUS = "DEACTIVATED";
    private static final String ENABLED = "ENABLED";
    private static final MembershipMapper MEMBERSHIP_MAPPER = Mappers.getMapper(MembershipMapper.class);
    private static final MembershipBookingTrackingMapper MEMBERSHIP_TRACKING_MAPPER = Mappers.getMapper(MembershipBookingTrackingMapper.class);
    private static final BiFunction<MembershipResponse, Long, MembershipResponse> EXPIRE_MATCHING_MEMBERSHIP = (membershipResponse, id) -> {
        if (membershipResponse.getId() == id) {
            membershipResponse.setStatus(EXPIRED.name());
        }
        return membershipResponse;
    };
    private static final BiFunction<MembershipResponse, Long, MembershipResponse> CONSUME_BALANCE_MATCHING_MEMBERSHIP = (membershipResponse, id) -> {
        if (membershipResponse.getId() == id) {
            membershipResponse.setBalance(BigDecimal.ZERO);
        }
        return membershipResponse;
    };
    private static final BiFunction<MembershipResponse, Long, MembershipResponse> SAVE_RECURRING_ID_IN_MEMBERSHIP = (membershipResponse, id) -> {
        if (membershipResponse.getId() == id) {
            membershipResponse.setRecurringId(RECURRING_ID);
        }
        return membershipResponse;
    };
    private static final BiFunction<MembershipResponse, Long, MembershipResponse> DEACTIVATE_MEMBERSHIP = (membershipResponse, id) -> {
        if (membershipResponse.getId() == id) {
            membershipResponse.setStatus(DEACTIVATED_STATUS);
        }
        return membershipResponse;
    };
    private static final BiFunction<MembershipResponse, Long, MembershipResponse> ENABLE_AUTO_RENEWAL = (membershipResponse, id) -> {
        if (membershipResponse.getId() == id) {
            membershipResponse.setAutoRenewal(ENABLED);
        }
        return membershipResponse;
    };
    private static final Map<MembershipServiceEndpointName, BiFunction<MembershipResponse, Long, MembershipResponse>> UPDATE_OPERATIONS = ImmutableMap.of(
            MembershipServiceEndpointName.EXPIRE_MEMBERSHIP, EXPIRE_MATCHING_MEMBERSHIP,
            MembershipServiceEndpointName.CONSUME_MEMBERSHIP_REMNANT_BALANCE, CONSUME_BALANCE_MATCHING_MEMBERSHIP,
            MembershipServiceEndpointName.INSERT_RECURRING_ID, SAVE_RECURRING_ID_IN_MEMBERSHIP,
            MembershipServiceEndpointName.DEACTIVATE_MEMBERSHIP, DEACTIVATE_MEMBERSHIP,
            MembershipServiceEndpointName.ENABLE_AUTO_RENEWAL, ENABLE_AUTO_RENEWAL
    );


    @Override
    public List<BlackListedPaymentMethod> getBlacklistedPaymentMethods(Long aLong) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public Membership getMembership(Long aLong, String s) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public WebsiteMembership getAllMembership(Long aLong) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public Boolean applyMembership(CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public MembershipStatus disableMembership(Long aLong) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public MembershipStatus reactivateMembership(Long aLong) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public MembershipRenewal disableAutoRenewal(Long aLong) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public MembershipRenewal enableAutoRenewal(Long aLong) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public Boolean isMembershipPerksActiveOn(String s) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public Long createMembership(CreateMembershipRequest createMembershipRequest) throws MembershipServiceException {

        /*
         *    TODO fix error throwing/mapping/handling
         *     Currently errors are simulated by causing a timeout which triggers an UndeclaredThrowableException in the calling method
         */

        if (createMembershipRequest instanceof CreatePendingToCollectRequest) {
            saveRequest(CREATE_PENDING_TO_COLLECT, createMembershipRequest);

            if (isExceptionResponse(CREATE_PENDING_TO_COLLECT)) {
                FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
            }

            return getPendingToCollectMembershipId((CreatePendingToCollectRequest) createMembershipRequest);
        } else {
            throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
        }
    }

    @Override
    public Boolean updateMemberAccount(UpdateMemberAccountRequest updateMemberAccountRequest) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public Boolean updateMembership(UpdateMembershipRequest updateMembershipRequest) {
        MembershipServiceEndpointName endpoint = MembershipServiceEndpointName.valueOf(updateMembershipRequest.getOperation());
        saveRequest(endpoint, updateMembershipRequest);

        if (isExceptionResponse(endpoint)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }

        long id = Long.parseLong(updateMembershipRequest.getMembershipId());

        Optional<Object> mockedErrorResponse = findMockedErrorResponse(endpoint, id);
        if (mockedErrorResponse.isPresent()) {
            return (Boolean) mockedErrorResponse.get();
        }

        List<MembershipResponse> memberships = findResponseEntities(MembershipSearchApiEndpointName.SEARCH_MEMBERSHIPS).stream()
                .map(MembershipResponse.class::cast)
                .peek(m -> UPDATE_OPERATIONS.get(endpoint).apply(m, id))
                .collect(Collectors.toList());

        verifySingleMembershipUpdatedAndStoreChange(id, memberships, endpoint.name());
        return true;
    }

    @Override
    public Boolean retryMembershipActivation(Long aLong) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public Boolean addToBlackList(Long aLong, AddToBlackListRequest addToBlackListRequest) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public String getNewVatModelDate() {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public Boolean isMembershipToBeRenewed(Long aLong) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public MembershipResponse getCurrentMembership(Long aLong, String s) throws MembershipServiceException {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public MembershipResponse activateMembership(long l, ActivationRequest activationRequest) throws MembershipServiceException {
        saveRequest(ACTIVATE_MEMBERSHIP, activationRequest);
        if (isExceptionResponse(ACTIVATE_MEMBERSHIP)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }

        MembershipResponse membershipResponse = new MembershipResponse();
        membershipResponse.setStatus(FunctionalUtils.MembershipStatus.ACTIVATED.name());
        membershipResponse.setBalance(activationRequest.getBalance());
        return membershipResponse;
    }

    @Override
    public Boolean isEligibleForFreeTrial(FreeTrialCandidateRequest freeTrialCandidateRequest) throws MembershipServiceException {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public MembershipBookingTracking getBookingTracking(long bookingId) throws MembershipServiceException {
        List<Object> response = saveRequestAndFindResponseEntities(bookingId, GET_BOOKING_TRACKING);
        if (isExceptionResponse(GET_BOOKING_TRACKING)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }
        return response.stream().map(MembershipBookingTracking.class::cast).filter(membershipBookingTracking -> bookingId == membershipBookingTracking.getBookingId())
                .findFirst().orElse(null);
    }

    @Override
    public MembershipBookingTracking addBookingTrackingUpdateBalance(MembershipBookingTrackingRequest membershipBookingTrackingRequest) throws MembershipServiceException {
        saveRequest(SAVE_BOOKING_TRACKING, membershipBookingTrackingRequest);
        if (isExceptionResponse(SAVE_BOOKING_TRACKING)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }
        MembershipBookingTracking bookingTracking = MEMBERSHIP_TRACKING_MAPPER.fromRequestToTrackingResponse(membershipBookingTrackingRequest);
        updateMembershipBalance(bookingTracking, membershipResponse -> membershipResponse.setBalance(membershipResponse.getBalance().add(bookingTracking.getAvoidFeeAmount())));
        addMockedResponseEntities(GET_BOOKING_TRACKING, singletonList(bookingTracking));
        return bookingTracking;
    }


    @Override
    public void deleteBookingTrackingUpdateBalance(long bookingId) throws MembershipServiceException {
        saveRequest(DELETE_BOOKING_TRACKING, bookingId);
        if (isExceptionResponse(DELETE_BOOKING_TRACKING)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }
        findResponseEntities(GET_BOOKING_TRACKING).stream()
                .map(MembershipBookingTracking.class::cast)
                .filter(bookingTracking -> bookingId == bookingTracking.getBookingId())
                .findFirst()
                .ifPresent(bookingTracking -> updateMembershipBalance(bookingTracking, membershipResponse -> membershipResponse.setBalance(membershipResponse.getBalance().subtract(bookingTracking.getAvoidFeeAmount()))));
        deleteMockedResponseEntities(GET_BOOKING_TRACKING);
    }

    private void updateMembershipBalance(MembershipBookingTracking bookingTracking, Consumer<MembershipResponse> balanceUpdater) {
        findResponseEntities(MembershipSearchApiEndpointName.GET_MEMBERSHIP)
                .stream()
                .map(MembershipResponse.class::cast)
                .filter(membershipResponse -> membershipResponse.getId() == bookingTracking.getMembershipId())
                .forEach(balanceUpdater);
    }


    private Long getPendingToCollectMembershipId(CreatePendingToCollectRequest pendingToCollectRequest) {
        long memberAccountId = pendingToCollectRequest.getMemberAccountId();

        Optional<Long> existingPendingToCollectId = findResponseEntities(MembershipSearchApiEndpointName.SEARCH_MEMBERSHIPS).stream()
                .map(MembershipResponse.class::cast)
                .filter(memberHasExistingPendingToCollect(pendingToCollectRequest, memberAccountId))
                .map(MembershipResponse::getId)
                .findFirst();

        return existingPendingToCollectId.orElse(createNewPendingToCollectMembership(pendingToCollectRequest, memberAccountId));
    }

    private Predicate<MembershipResponse> memberHasExistingPendingToCollect(CreatePendingToCollectRequest pendingToCollectRequest, long memberAccountId) {
        return membership ->
                memberAccountId == membership.getMemberAccountId()
                        && pendingToCollectRequest.getWebsite().equalsIgnoreCase(membership.getWebsite())
                        && PENDING_TO_COLLECT.name().equalsIgnoreCase(membership.getStatus());
    }

    /*
     * Creates and stores a new PENDING_TO_COLLECT membership in the mocked responses
     */
    private Long createNewPendingToCollectMembership(CreatePendingToCollectRequest pendingToCollectRequest, long memberAccountId) {
        Long newId = getNextMembershipId(memberAccountId);

        /* create and add the new membershipResponse with new id */
        addMockedResponseEntities(
                MembershipSearchApiEndpointName.SEARCH_MEMBERSHIPS,
                singletonList(MEMBERSHIP_MAPPER.createPendingToCollectRequestToMembershipResponse(pendingToCollectRequest, newId)));

        return newId;
    }

    private Long getNextMembershipId(long memberAccountId) {
        Long currentMaxMembershipId = findResponseEntities(MembershipSearchApiEndpointName.SEARCH_MEMBERSHIPS).stream()
                .map(MembershipResponse.class::cast)
                .filter(membership -> memberAccountId == membership.getMemberAccountId())
                .map(MembershipResponse::getId)
                .max(Long::compare)
                .orElseThrow(() -> new IllegalStateException("No existing membershipId found for member " + memberAccountId));

        return currentMaxMembershipId + 1;
    }

    private void verifySingleMembershipUpdatedAndStoreChange(long id, List<MembershipResponse> memberships, String operation) {
        long membershipsUpdated = memberships.stream().filter(m -> m.getId() == id).count();
        if (membershipsUpdated != 1) {
            throw new IllegalStateException(membershipsUpdated > 1 ? MULTIPLE_MEMBERSHIPS_FOUND + id : String.format(MEMBERSHIP_TO_UPDATE_NOT_FOUND, operation));
        }
        updateMockedResponseEntities(MembershipSearchApiEndpointName.SEARCH_MEMBERSHIPS, memberships);
    }
}
