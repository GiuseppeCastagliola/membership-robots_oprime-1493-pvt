package com.odigeo.membership.robots.steps;

import com.google.inject.Inject;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.response.tracking.MembershipBookingTracking;
import com.odigeo.membership.robots.mocks.ServiceMock;
import com.odigeo.membership.robots.mocks.membershipsearchapi.MembershipSearchApiEndpointName;
import com.odigeo.membership.robots.mocks.membershipsearchapi.MembershipSearchApiWorld;
import com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceEndpointName;
import com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceWorld;
import com.odigeo.membership.robots.utils.FunctionalUtils;
import com.odigeo.membership.robots.utils.mappers.MemberAccountMapper;
import com.odigeo.membership.robots.utils.mappers.MembershipMapper;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.mapstruct.factory.Mappers;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static com.odigeo.membership.robots.utils.FunctionalUtils.asMapWithNullsAndEmptyStrings;
import static com.odigeo.membership.robots.utils.FunctionalUtils.toLocalDateTimeString;
import static java.util.Collections.singletonList;

@SuppressWarnings("PMD.NullAssignment")
@ScenarioScoped
public class MembershipSteps {

    private static final int DEFAULT_MONTHS_DURATION = 12;
    private final MembershipMapper membershipMapper = Mappers.getMapper(MembershipMapper.class);
    private final MemberAccountMapper memberAccountMapper = Mappers.getMapper(MemberAccountMapper.class);
    private final MembershipServiceWorld membershipServiceWorld;
    private final MembershipSearchApiWorld membershipSearchApiWorld;
    private final BookingsSteps bookingsSteps;
    private final AtomicLong userId = new AtomicLong(6464000L);

    @Inject
    public MembershipSteps(MembershipServiceWorld membershipServiceWorld,
                           MembershipSearchApiWorld membershipSearchApiWorld, BookingsSteps bookingsSteps) {
        this.membershipServiceWorld = membershipServiceWorld;
        this.membershipSearchApiWorld = membershipSearchApiWorld;
        this.bookingsSteps = bookingsSteps;
    }

    /*
     * Generates a Membership and MemberAccount mocked response entities
     * DataTable should contain the relevant Membership/MemberAccount properties.
     */
    @Given("^the following membership exists:$")
    public void createMembership(DataTable dataTable) {
        Pair<Membership, String> membershipWithProductStatus = convertToMembership(asMapWithNullsAndEmptyStrings(dataTable));
        membershipSearchApiWorld.addMockedResponseEntities(MembershipSearchApiEndpointName.SEARCH_MEMBERSHIPS, createListOfMembershipResponses(singletonList(membershipWithProductStatus)));
        membershipSearchApiWorld.addMockedResponseEntities(MembershipSearchApiEndpointName.SEARCH_ACCOUNTS, createListOfMemberAccountResponses(singletonList(membershipWithProductStatus)));
        membershipSearchApiWorld.addMockedResponseEntities(MembershipSearchApiEndpointName.GET_MEMBERSHIP, singletonList(membershipMapper.membershipToMembershipResponse(membershipWithProductStatus.getLeft(), membershipWithProductStatus.getRight())));
    }

    /*
     * Generates 1 of each Membership, MemberAccount and subscription BookingSummary and BookingDetail mocked response entities
     * DataTable should contain the relevant Membership/MemberAccount properties.
     * Any desired Booking properties should be also included.
     * Any missing will be populated from BOOKING_DEFAULT_VALUES
     */
    @Given("^the following membership exists and has a prime subscription booking:$")
    public void createMembershipAndSubscriptionBooking(DataTable dataTable) {
        Map<String, String> membershipProperties = asMapWithNullsAndEmptyStrings(dataTable);
        Pair<Membership, String> membershipWithProductStatus = convertToMembership(membershipProperties);
        membershipSearchApiWorld.addMockedResponseEntities(MembershipSearchApiEndpointName.SEARCH_MEMBERSHIPS, createListOfMembershipResponses(singletonList(membershipWithProductStatus)));
        membershipSearchApiWorld.addMockedResponseEntities(MembershipSearchApiEndpointName.SEARCH_ACCOUNTS, createListOfMemberAccountResponses(singletonList(membershipWithProductStatus)));
        bookingsSteps.createSubscriptionBookingForMembership(membershipProperties);
    }

    /*
     * Generates a Membership, MemberAccount and subscription BookingSummary and BookingDetail mocked response entity for each table entry
     * DataTable should contain the relevant Membership/MemberAccount properties.
     * Any desired Booking properties should be also included.
     * Any missing will be populated from BOOKING_DEFAULT_VALUES
     */
    @Given("^the following memberships exist and have prime subscription bookings:$")
    public void createMembershipsWithSubscriptionBookings(DataTable dataTable) {
        List<Map<String, String>> membershipsProperties = FunctionalUtils.asMapsWithNullsAndEmptyStrings(dataTable);
        List<Pair<Membership, String>> membershipsWithProductStatus = membershipsProperties.stream().map(this::convertToMembership).collect(Collectors.toList());
        membershipSearchApiWorld.addMockedResponseEntities(MembershipSearchApiEndpointName.SEARCH_MEMBERSHIPS, createListOfMembershipResponses(membershipsWithProductStatus));
        membershipSearchApiWorld.addMockedResponseEntities(MembershipSearchApiEndpointName.SEARCH_ACCOUNTS, createListOfMemberAccountResponses(membershipsWithProductStatus));
        membershipsProperties.forEach(bookingsSteps::createSubscriptionBookingForMembership);
    }

    @Given("^the following membership booking tracking exists:$")
    public void createMembershipBookingTracking(DataTable dataTable) {
        Map<String, String> membershipBookingTrackingProperties = asMapWithNullsAndEmptyStrings(dataTable);
        MembershipBookingTracking membershipBookingTracking = convertToMembershipBookingTracking(membershipBookingTrackingProperties);
        membershipServiceWorld.addMockedResponseEntities(MembershipServiceEndpointName.GET_BOOKING_TRACKING, singletonList(membershipBookingTracking));
    }

    @Given("^updateMembership requests are always successful")
    public void updateMembershipsAlwaysSuccessful() {
        membershipServiceWorld.addMockedResponseGenerator(
                MembershipServiceEndpointName.EXPIRE_MEMBERSHIP, ServiceMock.alwaysReturn(Boolean.TRUE)
        );
    }

    @Given("^(EXPIRE_MEMBERSHIP|CONSUME_MEMBERSHIP_REMNANT_BALANCE|DEACTIVATE_MEMBERSHIP) endpoint of MembershipService returns an error response for membershipId (\\d+)")
    public void endpointOfMembershipServiceReturnsErrorResponse(String membershipEndpoint, long membershipId) {
        membershipServiceWorld.addMockedErrorResponse(MembershipServiceEndpointName.valueOf(membershipEndpoint), membershipId, Boolean.FALSE);
    }

    private Pair<Membership, String> convertToMembership(Map<String, String> membershipProperties) {
        Membership membership = new Membership();
        membership.setName(membershipProperties.get("name"));
        membership.setLastNames(membershipProperties.get("lastNames"));
        membership.setMemberAccountId(Long.parseLong(membershipProperties.get("memberAccountId")));
        membership.setMembershipId(Long.parseLong(membershipProperties.get("membershipId")));
        membership.setMembershipStatus(membershipProperties.get("membershipStatus"));
        membership.setActivationDate(membershipProperties.get("activationDate"));
        membership.setExpirationDate(membershipProperties.get("expirationDate"));
        membership.setAutoRenewalStatus(membershipProperties.get("autoRenewalStatus"));
        membership.setBalance(NumberUtils.createBigDecimal(membershipProperties.get("balance")));
        membership.setMembershipType(membershipProperties.get("membershipType"));
        membership.setSourceType(membershipProperties.get("sourceType"));
        membership.setWebsite(membershipProperties.getOrDefault("website", "FR"));
        membership.setRecurringId(membershipProperties.getOrDefault("recurringId", null));
        return Pair.of(membership, membershipProperties.getOrDefault("productStatus", null));
    }

    private MembershipBookingTracking convertToMembershipBookingTracking(Map<String, String> membershipBookingTrackingProperties) {
        MembershipBookingTracking membershipBookingTracking = new MembershipBookingTracking();
        membershipBookingTracking.setMembershipId(Long.parseLong(membershipBookingTrackingProperties.get("membershipId")));
        membershipBookingTracking.setBookingId(Long.parseLong(membershipBookingTrackingProperties.get("bookingId")));
        membershipBookingTracking.setBookingDate(membershipBookingTrackingProperties.get("bookingDate"));
        membershipBookingTracking.setAvoidFeeAmount(NumberUtils.createBigDecimal(membershipBookingTrackingProperties.get("avoidFeeAmount")));
        membershipBookingTracking.setCostFeeAmount(NumberUtils.createBigDecimal(membershipBookingTrackingProperties.get("costFeeAmount")));
        membershipBookingTracking.setPerksAmount(NumberUtils.createBigDecimal(membershipBookingTrackingProperties.get("perksAmount")));
        return membershipBookingTracking;
    }

    private List<MembershipResponse> createListOfMembershipResponses(List<Pair<Membership, String>> membershipsWithProductStatus) {
        return membershipsWithProductStatus.stream()
                .map(this::setDates)
                .map(pair -> membershipMapper.membershipToMembershipResponse(pair.getLeft(), pair.getRight()))
                .map(this::setTimestamp)
                .collect(Collectors.toList());
    }

    private List<MemberAccountResponse> createListOfMemberAccountResponses(List<Pair<Membership, String>> membershipsWithProductStatus) {
        return membershipsWithProductStatus.stream()
                .map(pair -> memberAccountMapper.membershipToMemberAccountResponse(pair.getLeft()))
                .map(this::setUserId)
                .collect(Collectors.toList());
    }

    private MemberAccountResponse setUserId(MemberAccountResponse memberAccount) {
        if (memberAccount.getUserId() == 0) {
            memberAccount.setUserId(userId.incrementAndGet());
        }
        return memberAccount;
    }

    private Pair<Membership, String> setDates(Pair<Membership, String> membershipsWithProductStatus) {
        Membership membership = membershipsWithProductStatus.getLeft();
        if (Objects.nonNull(membership.getExpirationDate())) {
            LocalDateTime expirationDate = FunctionalUtils.parseExpiryDate(membership.getExpirationDate());
            String activationDate = StringUtils.isEmpty(membership.getActivationDate())
                    ? toLocalDateTimeString(generateActivationDate(membership.getMonthsDuration(), expirationDate))
                    : membership.getActivationDate();
            membership.setExpirationDate(toLocalDateTimeString(expirationDate));
            membership.setActivationDate(activationDate);
        }
        return membershipsWithProductStatus;
    }

    private LocalDateTime generateActivationDate(int monthsDuration, LocalDateTime expirationDate) {
        return monthsDuration > 0
                ? expirationDate.minus(monthsDuration, ChronoUnit.MONTHS)
                : expirationDate.minus(DEFAULT_MONTHS_DURATION, ChronoUnit.MONTHS);
    }

    /*
     * Set a "realistic" time to the membership timestamp.
     * Based on date of activation and current time.
     */
    private MembershipResponse setTimestamp(MembershipResponse membership) {
        membership.setTimestamp(
                Objects.nonNull(membership.getActivationDate())
                        ? toLocalDateTimeString(LocalDateTime.of(LocalDateTime.parse(membership.getActivationDate()).toLocalDate(), LocalTime.now()))
                        : null);
        return membership;
    }


}
