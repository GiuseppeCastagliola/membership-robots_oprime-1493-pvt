package com.odigeo.membership.robots.mocks.bookingsearchapiservice;

import com.google.inject.Inject;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.membership.robots.mocks.InspectableWorld;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;
import java.util.Objects;

@ScenarioScoped
public class BookingSearchApiServiceWorld extends InspectableWorld<BookingSearchApiServiceMock, BookingSearchApiServiceServer, BookingSearchApiServiceEndpointName> {

    private static final String RULE_VALIDATE_ANY_REQUEST = "any request";
    private static final int REQUEST_BOOKING_SEARCH_API_INDEX = 3;

    @Inject
    public BookingSearchApiServiceWorld(BookingSearchApiServiceMock bookingSearchApiServiceMock, BookingSearchApiServiceServer bookingSearchApiServiceServer) {
        super(bookingSearchApiServiceMock, bookingSearchApiServiceServer);
        addValidatorFor(RULE_VALIDATE_ANY_REQUEST, Objects::nonNull);
        addValueValidatorFor("membershipId", (requestParams, expected) -> validateMembershipId((List<Object>) requestParams, (String) expected));
        addValueValidatorFor("isBookingSubscriptionPrime", (requestParams, expected) -> validateIsBookingSubscriptionPrime((List<Object>) requestParams, (String) expected));
    }

    private boolean validateMembershipId(List<Object> requestParams, String membershipId) {
        SearchBookingsRequest searchBookingsRequest = (SearchBookingsRequest) requestParams.get(REQUEST_BOOKING_SEARCH_API_INDEX);
        return searchBookingsRequest.getMembershipId().equals(Long.valueOf(membershipId));
    }

    private boolean validateIsBookingSubscriptionPrime(List<Object> requestParams, String isBookingSubscriptionPrime) {
        SearchBookingsRequest searchBookingsRequest = (SearchBookingsRequest) requestParams.get(REQUEST_BOOKING_SEARCH_API_INDEX);
        return searchBookingsRequest.getIsBookingSubscriptionPrime().equals(isBookingSubscriptionPrime);
    }

    @Override
    public BookingSearchApiServiceEndpointName findEndpointName(String endpointName) {
        return BookingSearchApiServiceEndpointName.valueOf(endpointName);
    }

    @Override
    public BookingSearchApiServiceEndpointName[] findEndpointNames() {
        return BookingSearchApiServiceEndpointName.values();
    }
}
