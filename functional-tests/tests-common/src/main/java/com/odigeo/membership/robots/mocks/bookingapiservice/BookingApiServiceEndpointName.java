package com.odigeo.membership.robots.mocks.bookingapiservice;

import com.odigeo.membership.robots.mocks.EndpointName;

public enum BookingApiServiceEndpointName implements EndpointName {
    GET_BOOKING,
    UPDATE_BOOKING
}
