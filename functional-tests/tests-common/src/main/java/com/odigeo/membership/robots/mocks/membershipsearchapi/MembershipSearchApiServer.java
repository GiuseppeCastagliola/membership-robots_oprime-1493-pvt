package com.odigeo.membership.robots.mocks.membershipsearchapi;

import com.google.inject.Singleton;
import com.odigeo.membership.robots.mocks.ServiceServer;

@Singleton
public class MembershipSearchApiServer extends ServiceServer {

    private static final String MEMBERSHIP_SEARCH_API_PATH = "/membership";
    private static final int MEMBERSHIP_SEARCH_API_PORT = 2334;

    public MembershipSearchApiServer() {
        super(MEMBERSHIP_SEARCH_API_PATH, MEMBERSHIP_SEARCH_API_PORT);
    }
}
