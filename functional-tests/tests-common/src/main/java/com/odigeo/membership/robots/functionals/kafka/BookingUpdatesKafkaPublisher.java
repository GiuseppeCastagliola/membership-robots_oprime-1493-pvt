package com.odigeo.membership.robots.functionals.kafka;

import com.google.inject.Singleton;
import com.odigeo.commons.messaging.BasicMessage;
import com.odigeo.commons.messaging.KafkaPublisher;
import com.odigeo.commons.messaging.MessagePublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class BookingUpdatesKafkaPublisher extends KafkaPublisherBasicMessage {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingUpdatesKafkaPublisher.class);
    private static final long KAFKA_PUBLISHER_WAIT_TIME = 5;

    @MessagePublisher(topic = "BOOKING_UPDATES_v1")
    private KafkaPublisher<BasicMessage> publisher;

    @Override
    protected KafkaPublisher<BasicMessage> getPublisher() {
        return publisher;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected long getKafkaPublisherWaitTime() {
        return KAFKA_PUBLISHER_WAIT_TIME;
    }
}
