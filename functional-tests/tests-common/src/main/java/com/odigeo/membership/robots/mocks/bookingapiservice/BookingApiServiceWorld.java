package com.odigeo.membership.robots.mocks.bookingapiservice;

import com.google.inject.Inject;
import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.membership.robots.mocks.InspectableWorld;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@ScenarioScoped
public class BookingApiServiceWorld extends InspectableWorld<BookingApiServiceMock, BookingApiServiceServer, BookingApiServiceEndpointName> {

    private static final int REQUEST_BOOKING_API_INDEX = 3;
    private static final int UPDATE_BOOKING_REQUEST_INDEX = 4;

    @Inject
    public BookingApiServiceWorld(BookingApiServiceMock bookingApiServiceMock, BookingApiServiceServer bookingApiServiceServer) {
        super(bookingApiServiceMock, bookingApiServiceServer);

        addValidatorFor("any request", Objects::nonNull);
        addValueValidatorFor("bookingId", (requestParams, expected) -> validateBookingId((List<Object>) requestParams, (String) expected));
        addValueValidatorFor("feeRequestCode", (requestParams, expected) -> validateFeeRequestCode((List<Object>) requestParams, (String) expected));
        addValueValidatorFor("feeRequestAmount", (requestParams, expected) -> validateFeeRequestAmount((List<Object>) requestParams, (String) expected));
    }

    private boolean validateBookingId(List<Object> requestParams, String expectedBookingId) {
        Long bookingId = (Long) requestParams.get(REQUEST_BOOKING_API_INDEX);
        return bookingId.equals(Long.valueOf(expectedBookingId));
    }

    private boolean validateFeeRequestCode(List<Object> requestParams, String expectedFeeCode) {
        List<FeeRequest> feeRequests = ((UpdateBookingRequest) requestParams.get(UPDATE_BOOKING_REQUEST_INDEX)).getBookingFees();
        return feeRequests.stream().anyMatch(fee -> fee.getSubCode().equalsIgnoreCase(expectedFeeCode));
    }

    private boolean validateFeeRequestAmount(List<Object> requestParams, String expectedAmount) {
        List<FeeRequest> feeRequests = ((UpdateBookingRequest) requestParams.get(UPDATE_BOOKING_REQUEST_INDEX)).getBookingFees();
        BigDecimal expectedFeeAmount = new BigDecimal(expectedAmount);
        return feeRequests.stream().anyMatch(fee -> fee.getAmount().equals(expectedFeeAmount));
    }

    @Override
    public BookingApiServiceEndpointName findEndpointName(String endpointName) {
        return BookingApiServiceEndpointName.valueOf(endpointName);
    }

    @Override
    public BookingApiServiceEndpointName[] findEndpointNames() {
        return BookingApiServiceEndpointName.values();
    }
}
