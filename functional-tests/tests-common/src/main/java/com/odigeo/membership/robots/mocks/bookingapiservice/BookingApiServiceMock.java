package com.odigeo.membership.robots.mocks.bookingapiservice;

import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.FeeBuilder;
import com.odigeo.bookingapi.mock.v12.response.FeeContainerBuilder;
import com.odigeo.bookingapi.v12.BookingApiService;
import com.odigeo.bookingapi.v12.requests.CorrectPaymentRequest;
import com.odigeo.bookingapi.v12.requests.CreateBookingFromGdsRequest;
import com.odigeo.bookingapi.v12.requests.CreateBookingFromLccRequest;
import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingapi.v12.responses.BookingByTicketNumberResponse;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.GetBookingChangeTrackingResponse;
import com.odigeo.bookingapi.v12.responses.MembershipPerks;
import com.odigeo.membership.robots.mocks.MockException;
import com.odigeo.membership.robots.mocks.ServiceMock;
import com.odigeo.membership.robots.utils.FunctionalUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.odigeo.membership.robots.mocks.bookingapiservice.BookingApiServiceEndpointName.GET_BOOKING;
import static com.odigeo.membership.robots.mocks.bookingapiservice.BookingApiServiceEndpointName.UPDATE_BOOKING;

public class BookingApiServiceMock extends ServiceMock<BookingApiServiceEndpointName> implements BookingApiService {

    private static final Logger LOGGER = Logger.getLogger(BookingApiServiceMock.class);
    private final AtomicLong idGenerator = new AtomicLong(5665000L);
    private static final long REQUEST_TIMEOUT = 500L;
    private static final Predicate<Object> OBJECT_IS_A_STRING_AND_LITERAL_NULL = obj -> obj instanceof String && "null".equals(obj);

    @Override
    public BookingDetail getBooking(String user, String password, Locale locale, long id) {
        List<Object> params = Arrays.asList(user, password, locale, id);
        LOGGER.info("Get booking id " + id);
        saveRequest(GET_BOOKING, params);

        if (isExceptionResponse(GET_BOOKING)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }

        Optional<Object> mockedErrorResponse = findMockedErrorResponse(GET_BOOKING, id);
        if (mockedErrorResponse.isPresent()) {
            Object mockedResponse = mockedErrorResponse.get();
            if (mockedResponse instanceof String && StringUtils.equals((String) mockedResponse, "null")) {
                return null;
            }
            return (BookingDetail) mockedResponse;
        }

        BookingDetailBuilder bookingDetailBuilder = getBookingDetailBuilder(id);
        return buildBookingDetail(bookingDetailBuilder);
    }

    @Override
    public BookingDetail updateBooking(String user, String password, Locale locale, long id, UpdateBookingRequest updateBookingRequest) {
        List<Object> params = Arrays.asList(user, password, locale, id, updateBookingRequest);
        BookingDetail bookingDetailResponse = null;
        /* Saving the request. Will retrieve the mocked BookingDetails from GET_BOOKING endpoint */
        saveRequest(UPDATE_BOOKING, params);
        if (isExceptionResponse(UPDATE_BOOKING)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }
        Optional<Object> mockedErrorResponse = findMockedErrorResponse(UPDATE_BOOKING, id);
        if (mockedErrorResponse.isPresent()) {
            Object mockedResponse = mockedErrorResponse.get();
            if (OBJECT_IS_A_STRING_AND_LITERAL_NULL.negate().test(mockedResponse)) {
                bookingDetailResponse = (BookingDetail) mockedResponse;
            }
        } else {
            List<Object> bookingDetailEntities = findResponseEntities(GET_BOOKING);
            bookingDetailResponse = retrieveResponse(id, updateBookingRequest, bookingDetailEntities);
        }
        return bookingDetailResponse;
    }

    private BookingDetail retrieveResponse(long id, UpdateBookingRequest updateBookingRequest, List<Object> bookingDetailEntities) {
        BookingDetail bookingDetailResponse;
        /*
         *  TODO the following logic emulates the BookingApiService in updating the mocked Booking responses.
         *   This could be argued as OUT OF SCOPE or overkill and could be removed
         */
        if (CollectionUtils.isNotEmpty(updateBookingRequest.getBookingFees())) {
            FeeContainerBuilder feeContainerBuilder = generateFeeContainerBuilder(updateBookingRequest);
            List<BookingDetailBuilder> bookingDetailBuilders = addNewFeeContainerToBookingDetailWithId(bookingDetailEntities, feeContainerBuilder, id);
            updateMockedResponseEntities(GET_BOOKING, bookingDetailBuilders);
            BookingDetailBuilder bookingDetailBuilder = getBookingDetailBuilder(id);
            bookingDetailResponse = buildBookingDetail(bookingDetailBuilder);
        } else {
            bookingDetailResponse = bookingDetailEntities.stream()
                    .map(BookingDetailBuilder.class::cast)
                    .filter(bookingDetailBuilder -> bookingDetailBuilder.getBookingBasicInfoBuilder().getId() == id)
                    .findFirst()
                    .map(this::buildBookingDetail)
                    .orElse(null);
        }
        return bookingDetailResponse;
    }

    @Override
    public BookingByTicketNumberResponse getBookingByTicketNumber(String s, String s1, Locale locale, String s2) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public GetBookingChangeTrackingResponse getBookingChangeTrackingByDates(String s, String s1, Date date, Date date1, Integer integer, String s2) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public GetBookingChangeTrackingResponse getBookingChangeTrackingByIdentifiers(String s, String s1, Long aLong, Integer integer, String s2) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public BookingDetail createBookingFromGDS(String s, String s1, Locale locale, CreateBookingFromGdsRequest createBookingFromGdsRequest) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public BookingDetail createBookingFromLCC(String s, String s1, CreateBookingFromLccRequest createBookingFromLccRequest) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public BookingDetail correctPayment(String s, String s1, Locale locale, long l, CorrectPaymentRequest correctPaymentRequest) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    private List<BookingDetailBuilder> addNewFeeContainerToBookingDetailWithId(List<Object> bookingDetailEntities, FeeContainerBuilder feeContainerBuilder, long id) {
        return bookingDetailEntities.stream()
                .map(BookingDetailBuilder.class::cast)
                .map(builder -> builder.getBookingBasicInfoBuilder().getId() == id ? addNewFeeContainer(builder, feeContainerBuilder) : builder)
                .collect(Collectors.toList());
    }

    private BookingDetailBuilder addNewFeeContainer(BookingDetailBuilder bookingDetailBuilder, FeeContainerBuilder newFeeContainerBuilder) {
        List<FeeContainerBuilder> feeContainerBuilders = new ArrayList<>(bookingDetailBuilder.getAllFeeContainers());
        feeContainerBuilders.add(newFeeContainerBuilder);
        return bookingDetailBuilder.allFeeContainers(feeContainerBuilders);
    }

    private FeeContainerBuilder generateFeeContainerBuilder(UpdateBookingRequest updateBookingRequest) {
        List<FeeBuilder> feeBuilders = updateBookingRequest.getBookingFees().stream()
                .map(this::toFeeBuilder)
                .collect(Collectors.toList());

        return new FeeContainerBuilder().id(idGenerator.incrementAndGet())
                .feeContainerType("BOOKING")
                .fees(feeBuilders)
                .totalAmount(BigDecimal.ZERO);
    }

    private FeeBuilder toFeeBuilder(FeeRequest feeRequest) {
        return new FeeBuilder().id(idGenerator.incrementAndGet())
                .subCode(feeRequest.getSubCode())
                .amount(feeRequest.getAmount())
                .currency(feeRequest.getCurrency())
                .feeLabel("ADJUSTMENT");
    }

    private BookingDetailBuilder getBookingDetailBuilder(long bookingDetailId) {
        List<Object> bookingDetailEntities = findResponseEntities(GET_BOOKING);

        List<BookingDetailBuilder> bookingDetailBuilders = bookingDetailEntities.stream()
                .map(BookingDetailBuilder.class::cast)
                .filter(builder -> builder.getBookingBasicInfoBuilder().getId() == bookingDetailId)
                .collect(Collectors.toList());

        if (bookingDetailBuilders.size() != 1) {
            throw new IllegalStateException(String.format("%s bookingDetails found for bookingId %s. There should %sbe one.", bookingDetailBuilders.size(), bookingDetailId, bookingDetailBuilders.size() > 1 ? "only " : ""));
        }

        return bookingDetailBuilders.get(0);
    }

    private BookingDetail buildBookingDetail(BookingDetailBuilder bookingDetailBuilder) {
        try {
            BookingDetail bookingDetail = bookingDetailBuilder.build(new Random());
            bookingDetail.getBookingBasicInfo().setMembershipId(Optional.ofNullable(bookingDetail.getMembershipPerks())
                    .map(MembershipPerks::getMemberId).orElse(null));
            return bookingDetail;
        } catch (BuilderException e) {
            LOGGER.error("BuilderException thrown for BookingDetailBuilder.BookingBasicInfo.Id: " + bookingDetailBuilder.getBookingBasicInfoBuilder().getId(), e);
        }
        return null;
    }
}
