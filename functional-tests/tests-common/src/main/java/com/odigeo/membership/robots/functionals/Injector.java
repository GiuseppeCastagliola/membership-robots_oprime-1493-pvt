package com.odigeo.membership.robots.functionals;

import com.google.inject.Guice;
import com.google.inject.Stage;
import com.odigeo.commons.messaging.MessagePublisherModule;
import com.odigeo.technology.docker.DockerModule;
import cucumber.api.guice.CucumberModules;
import cucumber.runtime.java.guice.InjectorSource;

public class Injector implements InjectorSource {
    @Override
    public com.google.inject.Injector getInjector() {
        return Guice.createInjector(Stage.PRODUCTION, CucumberModules.SCENARIO, new MessagePublisherModule(), new DockerModule());
    }
}
