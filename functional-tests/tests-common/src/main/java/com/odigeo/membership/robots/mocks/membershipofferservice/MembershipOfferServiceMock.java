package com.odigeo.membership.robots.mocks.membershipofferservice;

import com.odigeo.membership.offer.api.MembershipOfferService;
import com.odigeo.membership.offer.api.exception.MembershipOfferServiceException;
import com.odigeo.membership.offer.api.request.MembershipOfferRequest;
import com.odigeo.membership.offer.api.request.MembershipRenewalOfferRequest;
import com.odigeo.membership.offer.api.request.SearchCriteriaRequest;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.robots.mocks.MockException;
import com.odigeo.membership.robots.mocks.ServiceMock;
import com.odigeo.membership.robots.utils.FunctionalUtils;

import static com.odigeo.membership.robots.mocks.membershipofferservice.MembershipOfferServiceEndpointName.GET_OFFER;

public class MembershipOfferServiceMock extends ServiceMock<MembershipOfferServiceEndpointName> implements MembershipOfferService {

    private static final long REQUEST_TIMEOUT = 500L;

    @Override
    public MembershipSubscriptionOffer createMembershipSubscriptionOffer(SearchCriteriaRequest searchCriteriaRequest) throws MembershipOfferServiceException {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public MembershipSubscriptionOffer createMembershipSubscriptionOffer(MembershipOfferRequest membershipOfferRequest) throws MembershipOfferServiceException {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }



    @Override
    public MembershipSubscriptionOffer createMembershipRenewalOffer(MembershipRenewalOfferRequest membershipRenewalOfferRequest) throws MembershipOfferServiceException {
        saveRequest(GET_OFFER, membershipRenewalOfferRequest);

        if (isExceptionResponse(GET_OFFER)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }

        return findResponseEntities(GET_OFFER).stream()
                .map(MembershipSubscriptionOffer.Builder.class::cast)
                .map(MembershipSubscriptionOffer.Builder::build)
                .filter(offer -> membershipRenewalOfferRequest.getWebsiteCode().equalsIgnoreCase(offer.getWebsite()))
                .findFirst()
                .orElse(null);

    }

    @Override
    public MembershipSubscriptionOffer getMembershipSubscriptionOffer(String s) throws MembershipOfferServiceException {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }
}
