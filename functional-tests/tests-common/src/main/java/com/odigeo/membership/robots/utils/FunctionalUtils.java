package com.odigeo.membership.robots.utils;

import cucumber.api.DataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FunctionalUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionalUtils.class);
    private static final String NULL_VALUE = "NULL_VALUE";
    private static final String EMPTY_STRING = "EMPTY_STRING";

    public enum MembershipStatus {
        PENDING_TO_ACTIVATE, PENDING_TO_COLLECT, ACTIVATED, DEACTIVATED, EXPIRED
    }

    public enum RobotAdminOperations {
        EXECUTE_ONCE("executeOnce"),
        GET_ROBOT_STATUS("getRobotStatus"),
        GET_ROBOT_INFO_FOR_NAGIOS("getRobotInfoForNagios"),
        RESTART_ROBOTS("restartRobots"),
        SCHEDULE_ROBOT("scheduleRobot"),
        SHUTDOWN_ROBOTS("shutDownRobots"),
        STOP_ROBOT("stopRobot"),
        STOP_ALL_ROBOTS("stopAllRobots"),
        UNSCHEDULE_ROBOT("unscheduleRobot");

        private String operationName;

        RobotAdminOperations(String operation) {
            this.operationName = operation;
        }

        public String getOperationName() {
            return operationName;
        }
    }

    public enum RobotJobStatus {
        RUNNING,
        STOPPED,
        SCHEDULED,
        STOPPING
    }

    private static final Pattern LOCAL_DATE_PATTERN = Pattern.compile("20[12][0-9]-[01][0-9]-[0-3][0-9]T[0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9][0-9]");

    private static final EnumSet<ChronoUnit> CHRONO_UNITS = EnumSet.of(ChronoUnit.DAYS, ChronoUnit.WEEKS, ChronoUnit.MONTHS, ChronoUnit.YEARS);

    /*
     * Parses a string to support generation of relative dates
     * can be a Local Date Time pattern yyyy-MM-ddTHH-mm-ss.000, or a relative date: either "TODAY", or "n (DAYS|WEEKS|MONTHS|YEARS) (AGO|FROM NOW)"
     * e.g. 1 DAY AGO, 1 YEAR FROM NOW
     */
    public static LocalDateTime parseExpiryDate(String expiryDate) {
        LocalDateTime requestedLocalDateTime = LocalDateTime.now();
        if (!"TODAY".equals(expiryDate)) {
            if (LOCAL_DATE_PATTERN.matcher(expiryDate).matches()) {
                requestedLocalDateTime = toLocalDateTime(expiryDate);
            } else {
                String[] tokens = expiryDate.split(" ");
                int quantity = Integer.parseInt(tokens[0]);
                ChronoUnit chronoUnit = parseChronoUnit(tokens[1].toUpperCase(Locale.getDefault()));
                if (expiryDate.endsWith(" AGO")) {
                    requestedLocalDateTime = requestedLocalDateTime.minus(quantity, chronoUnit);
                } else if (expiryDate.endsWith(" FROM NOW")) {
                    requestedLocalDateTime = requestedLocalDateTime.plus(quantity, chronoUnit);
                }
            }
        }
        return requestedLocalDateTime;
    }

    private static ChronoUnit parseChronoUnit(String chronoUnitName) {
        return Optional.of(chronoUnitName.endsWith("S") ? chronoUnitName : chronoUnitName + "S")
                .map(ChronoUnit::valueOf)
                .filter(CHRONO_UNITS::contains)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ChronoUnit: " + chronoUnitName));
    }

    private static LocalDateTime toLocalDateTime(String isoLocalDateTimeString) {
        return LocalDateTime.parse(isoLocalDateTimeString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    public static String toLocalDateTimeString(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    public static void triggerRequestTimeout(long requestTimeout) {
        try {
            Thread.sleep(requestTimeout);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
        }
        throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
    }

    public static List<String> asListWithNullsAndEmptyStrings(DataTable dataTable) {
        List<String> asListMutable = new ArrayList<>(dataTable.asList(String.class));
        return replaceNullsAndEmptyStrings(asListMutable);
    }

    public static List<List<String>> asListsWithNullsAndEmptyStrings(DataTable dataTable) {
        List<List<String>> asLists = dataTable.asLists(String.class);

        return asLists.stream()
                .map(ArrayList::new)
                .map(FunctionalUtils::replaceNullsAndEmptyStrings)
                .collect(Collectors.toList());
    }

    public static Map<String, String> asMapWithNullsAndEmptyStrings(DataTable dataTable) {
        Map<String, String> asMapMutable = new HashMap<>(dataTable.asMap(String.class, String.class));
        return replaceNullsAndEmptyStrings(asMapMutable);
    }

    public static List<Map<String, String>> asMapsWithNullsAndEmptyStrings(DataTable dataTable) {
        List<Map<String, String>> asMaps = dataTable.asMaps(String.class, String.class);

        return asMaps.stream()
                .map(HashMap::new)
                .map(FunctionalUtils::replaceNullsAndEmptyStrings)
                .collect(Collectors.toList());
    }

    private static List<String> replaceNullsAndEmptyStrings(List<String> mutableList) {
        int nullIndex;
        while (mutableList.contains(NULL_VALUE)) {
            nullIndex = mutableList.indexOf(NULL_VALUE);
            mutableList.remove(nullIndex);
            mutableList.add(nullIndex, null);
        }
        mutableList.replaceAll(e -> EMPTY_STRING.equals(e) ? "" : e);
        return mutableList;
    }

    private static Map<String, String> replaceNullsAndEmptyStrings(Map<String, String> mutableMap) {
        mutableMap.keySet().forEach(k -> {
            mutableMap.replace(k, NULL_VALUE, null);
            mutableMap.replace(k, EMPTY_STRING, "");
        });
        return mutableMap;
    }
}
