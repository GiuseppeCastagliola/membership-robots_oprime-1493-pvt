package com.odigeo.membership.robots.utils.mappers;

import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.search.MembershipResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Optional;

@Mapper
public interface MembershipMapper {

    @Mapping(target = "id", source = "membershipId")
    @Mapping(target = "website", source = "pendingToCollectRequest.website")
    @Mapping(target = "monthsDuration", source = "pendingToCollectRequest.monthsToRenewal")
    @Mapping(target = "sourceType", source = "pendingToCollectRequest.sourceType")
    @Mapping(target = "membershipType", expression = "java(getMembershipType(pendingToCollectRequest))")
    @Mapping(target = "memberAccountId", source = "pendingToCollectRequest.memberAccountId")
    @Mapping(target = "expirationDate", source = "pendingToCollectRequest.expirationDate")
    @Mapping(target = "totalPrice", source = "pendingToCollectRequest.subscriptionPrice")
    @Mapping(target = "currencyCode", source = "pendingToCollectRequest.currencyCode")
    @Mapping(target = "recurringId", source = "pendingToCollectRequest.recurringId")
    @Mapping(target = "status", constant = "PENDING_TO_COLLECT")
    @Mapping(target = "autoRenewal", constant = "ENABLED")
    @Mapping(target = "productStatus", constant = "INIT")
    MembershipResponse createPendingToCollectRequestToMembershipResponse(CreatePendingToCollectRequest pendingToCollectRequest, Long membershipId);

    @Mapping(target = "id", source = "membership.membershipId")
    @Mapping(target = "website", source = "membership.website")
    @Mapping(target = "status", source = "membership.membershipStatus")
    @Mapping(target = "autoRenewal", source = "membership.autoRenewalStatus")
    @Mapping(target = "memberAccountId", source = "membership.memberAccountId")
    @Mapping(target = "expirationDate", source = "membership.expirationDate")
    @Mapping(target = "activationDate", source = "membership.activationDate")
    @Mapping(target = "membershipType", expression = "java(getMembershipType(membership))")
    @Mapping(target = "balance", source = "membership.balance")
    @Mapping(target = "currencyCode", constant = "EUR")
    @Mapping(target = "monthsDuration", expression = "java(getMonthsDuration(membership))")
    @Mapping(target = "sourceType", source = "membership.sourceType")
    @Mapping(target = "recurringId", source = "membership.recurringId")
    @Mapping(target = "productStatus", source = "productStatus")
    MembershipResponse membershipToMembershipResponse(Membership membership, String productStatus);

    default String getMembershipType(CreatePendingToCollectRequest pendingToCollectRequest) {
        return Optional.ofNullable(pendingToCollectRequest.getMembershipType()).orElse("BASIC");
    }

    default String getMembershipType(Membership membership) {
        return Optional.ofNullable(membership.getMembershipType()).orElse("BASIC");
    }

    default int getMonthsDuration(Membership membership) {
        return membership.getMonthsDuration() > 0 ? membership.getMonthsDuration() : 12;
    }
}
