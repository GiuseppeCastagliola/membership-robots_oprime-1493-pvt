package com.odigeo.membership.robots.mocks;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Singleton
public class ResponseMocks {

    private final Multimap<EndpointName, Object> mockedResponseEntities;
    private final Map<EndpointName, Map<Long, Object>> errorResponses;
    private final List<EndpointName> exceptionResponses;

    public ResponseMocks() {
        mockedResponseEntities = LinkedListMultimap.create();
        errorResponses = new HashMap<>();
        exceptionResponses = new ArrayList<>();
    }

    <U> boolean putAll(EndpointName endpointName, Iterable<U> responseEntities) {
        return mockedResponseEntities.putAll(endpointName, responseEntities);
    }

    <U> List<Object> updateEntities(EndpointName endpointName, Iterable<U> responseEntities) {
        return new ArrayList<>(mockedResponseEntities.replaceValues(endpointName, responseEntities));
    }

    List<Object> getMockedResponseEntities(EndpointName endpointName) {
        return new ArrayList<>(mockedResponseEntities.get(endpointName));
    }

    void deleteMockedEntities(EndpointName endpointName) {
        mockedResponseEntities.removeAll(endpointName);
    }

    void putErrorResponse(EndpointName endpointName, Long membershipId, Object errorResponse) {
        errorResponses.putIfAbsent(endpointName, new HashMap<>());
        errorResponses.get(endpointName).put(membershipId, errorResponse);
    }

    void throwExceptionForNextCallToEndpoint(EndpointName endpointName) {
        exceptionResponses.add(endpointName);
    }

    boolean isExceptionResponse(EndpointName endpointName) {
        return exceptionResponses.remove(endpointName);
    }

    Optional<Object> getErrorResponse(EndpointName endpointName, Long membershipId) {
        return Optional.ofNullable(errorResponses.get(endpointName))
                .map(errorMap -> errorMap.containsKey(membershipId) ? Optional.of(errorMap.remove(membershipId)) : Optional.empty())
                .orElse(Optional.empty());
    }
}
