package com.odigeo.membership.robots.functionals.jboss;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.robots.ServerConfiguration;
import com.odigeo.technology.docker.ContainerException;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Optional;

@ScenarioScoped
public class JbossServerWorld {

    private static final Logger LOGGER = Logger.getLogger(JbossServerWorld.class);
    private static final String DEFAULT_CONTEXT_ROOT = "/membership-robots";
    private static final String ENGINEERING_PATH = "/engineering/";

    private final String contextRoot;

    public JbossServerWorld() {
        contextRoot = Optional.ofNullable(System.getProperty("contextRoot")).orElse(DEFAULT_CONTEXT_ROOT);
    }

    public void installScenario() {
        LOGGER.info("Start install jboss scenario");
        refresh();
        LOGGER.info("End install jboss scenario");
    }

    private void refresh() {
        LOGGER.info("Start refresh jboss");
        try {
            HttpClient httpClient = new HttpClient();
            final String refreshUrl = "http://" + ConfigurationEngine.getInstance(ServerConfiguration.class).getServer() + contextRoot + ENGINEERING_PATH + "refresh";
            httpClient.executeMethod(new GetMethod(refreshUrl));
        } catch (IOException | ContainerException e) {
            LOGGER.error("Error refreshing jboss: ", e);
        }
        LOGGER.info("End refresh jboss");
    }
}
