package com.odigeo.membership.robots.mocks.membershipofferservice;

import com.google.inject.Inject;
import com.odigeo.membership.offer.api.request.MembershipRenewalOfferRequest;
import com.odigeo.membership.robots.mocks.InspectableWorld;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Objects;

@ScenarioScoped
public class MembershipOfferServiceWorld extends InspectableWorld<MembershipOfferServiceMock, MembershipOfferServiceServer, MembershipOfferServiceEndpointName> {

    @Inject
    public MembershipOfferServiceWorld(MembershipOfferServiceMock membershipOfferServiceMock, MembershipOfferServiceServer membershipOfferServiceServer) {
        super(membershipOfferServiceMock, membershipOfferServiceServer);
        addValidatorFor("any request", Objects::nonNull);
        addValueValidatorFor("website", (request, expectedWebsite) -> validateWebsite((MembershipRenewalOfferRequest) request, (String) expectedWebsite));
        addValueValidatorFor("membershipId", (request, expected) -> validateMembershipId((MembershipRenewalOfferRequest) request, (String) expected));
    }

    private boolean validateWebsite(MembershipRenewalOfferRequest request, String website) {
        return request.getWebsiteCode().equalsIgnoreCase(website);
    }

    private boolean validateMembershipId(MembershipRenewalOfferRequest request, String membershipId) {
        return request.getMembershipId().equals(membershipId);
    }

    @Override
    public MembershipOfferServiceEndpointName findEndpointName(String endpointName) {
        return MembershipOfferServiceEndpointName.valueOf(endpointName);
    }

    @Override
    public MembershipOfferServiceEndpointName[] findEndpointNames() {
        return MembershipOfferServiceEndpointName.values();
    }
}
