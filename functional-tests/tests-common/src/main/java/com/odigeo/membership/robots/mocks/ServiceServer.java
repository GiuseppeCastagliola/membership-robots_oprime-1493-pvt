package com.odigeo.membership.robots.mocks;

import com.odigeo.membership.robots.server.JaxRsServiceHttpServer;

public class ServiceServer extends JaxRsServiceHttpServer {
    public ServiceServer(String path, int port) {
        super(path, port);
    }
}
