package com.odigeo.membership.robots.utils.mappers;

import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.search.MemberAccountResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface MemberAccountMapper {

    @Mapping(target = "id", source = "memberAccountId")
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "lastNames", source = "lastNames")
    @Mapping(target = "name", source = "name")
    MemberAccountResponse membershipToMemberAccountResponse(Membership membership);
}
