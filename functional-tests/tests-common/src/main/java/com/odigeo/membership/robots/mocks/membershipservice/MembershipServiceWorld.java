package com.odigeo.membership.robots.mocks.membershipservice;

import com.google.inject.Inject;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.robots.mocks.InspectableWorld;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

import static com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceEndpointName.CONSUME_MEMBERSHIP_REMNANT_BALANCE;
import static com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceEndpointName.DEACTIVATE_MEMBERSHIP;
import static com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceEndpointName.EXPIRE_MEMBERSHIP;
import static com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceEndpointName.INSERT_RECURRING_ID;
import static com.odigeo.membership.robots.utils.FunctionalUtils.parseExpiryDate;

@SuppressWarnings("PMD.TooManyStaticImports")
@ScenarioScoped
public class MembershipServiceWorld extends InspectableWorld<MembershipServiceMock, MembershipServiceServer, MembershipServiceEndpointName> {

    @Inject
    public MembershipServiceWorld(MembershipServiceMock membershipServiceMock, MembershipServiceServer membershipServiceServer) {
        super(membershipServiceMock, membershipServiceServer);
        addValidatorFor("any request", Objects::nonNull);
        addValueValidatorFor("memberAccountId", (request, expected) -> validateMemberAccountId((CreatePendingToCollectRequest) request, (String) expected));
        addValueValidatorFor("newExpirationDate", (request, expected) -> validateNewExpirationDate((CreatePendingToCollectRequest) request, (String) expected));
        addValueValidatorFor("subscriptionPrice", (request, expected) -> validateSubscriptionPrice((CreatePendingToCollectRequest) request, (String) expected));
        addValueValidatorFor("membershipIdToExpire", (request, expected) -> validateUpdateMembership((UpdateMembershipRequest) request, EXPIRE_MEMBERSHIP.name(), (String) expected));
        addValueValidatorFor("membershipIdToConsume", (request, expected) -> validateUpdateMembership((UpdateMembershipRequest) request, CONSUME_MEMBERSHIP_REMNANT_BALANCE.name(), (String) expected));
        addValueValidatorFor("membershipIdRecurringAdd", (request, expected) -> validateUpdateMembership((UpdateMembershipRequest) request, INSERT_RECURRING_ID.name(), (String) expected));
        addValueValidatorFor("membershipIdToDeactivate", (request, expected) -> validateUpdateMembership((UpdateMembershipRequest) request, DEACTIVATE_MEMBERSHIP.name(), (String) expected));
    }

    private boolean validateMemberAccountId(CreatePendingToCollectRequest request, String expectedMemberAccountId) {
        return request.getMemberAccountId() == Long.parseLong(expectedMemberAccountId);
    }

    private boolean validateNewExpirationDate(CreatePendingToCollectRequest request, String expectedNewExpirationDate) {
        LocalDateTime newExpirationDate = parseExpiryDate(expectedNewExpirationDate).truncatedTo(ChronoUnit.MINUTES);
        return LocalDateTime.parse(request.getExpirationDate()).truncatedTo(ChronoUnit.MINUTES).isEqual(newExpirationDate);
    }

    private boolean validateSubscriptionPrice(CreatePendingToCollectRequest request, String expectedSubscriptionPrice) {
        return request.getSubscriptionPrice().equals(new BigDecimal(expectedSubscriptionPrice));
    }

    private boolean validateUpdateMembership(UpdateMembershipRequest request, String operation, String expectedMembershipIdToUpdate) {
        return request.getOperation().equalsIgnoreCase(operation)
                && request.getMembershipId().equals(expectedMembershipIdToUpdate);
    }

    @Override
    public MembershipServiceEndpointName findEndpointName(String endpointName) {
        return MembershipServiceEndpointName.valueOf(endpointName);
    }

    @Override
    public MembershipServiceEndpointName[] findEndpointNames() {
        return MembershipServiceEndpointName.values();
    }
}
