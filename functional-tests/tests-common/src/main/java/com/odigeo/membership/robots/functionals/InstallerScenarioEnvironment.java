package com.odigeo.membership.robots.functionals;

import com.google.inject.Inject;
import com.odigeo.membership.robots.functionals.jboss.JbossServerWorld;
import com.odigeo.membership.robots.mocks.bookingapiservice.BookingApiServiceWorld;
import com.odigeo.membership.robots.mocks.bookingsearchapiservice.BookingSearchApiServiceWorld;
import com.odigeo.membership.robots.mocks.membershipofferservice.MembershipOfferServiceWorld;
import com.odigeo.membership.robots.mocks.membershipsearchapi.MembershipSearchApiWorld;
import com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceWorld;
import com.odigeo.membership.robots.server.ServerStopException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.log4j.Logger;

@ScenarioScoped
public class InstallerScenarioEnvironment {

    private static final Logger LOGGER = Logger.getLogger(InstallerScenarioEnvironment.class);

    @Inject
    private JbossServerWorld jbossServerWorld;
    @Inject
    private MembershipServiceWorld membershipServiceWorld;
    @Inject
    private BookingApiServiceWorld bookingApiServiceWorld;
    @Inject
    private BookingSearchApiServiceWorld bookingSearchApiServiceWorld;
    @Inject
    private MembershipSearchApiWorld membershipSearchApiWorld;
    @Inject
    private MembershipOfferServiceWorld membershipOfferServiceWorld;

    @Before
    public void install() throws ServerStopException {
        LOGGER.info("Start install environment for a scenario");
        jbossServerWorld.installScenario();
        bookingApiServiceWorld.install();
        bookingSearchApiServiceWorld.install();
        membershipSearchApiWorld.install();
        membershipOfferServiceWorld.install();
        membershipServiceWorld.install();
        LOGGER.info("End install environment for a scenario");
    }

    @After
    public void uninstall() {
        LOGGER.info("Start uninstall environment for a scenario");
        bookingApiServiceWorld.uninstall();
        bookingSearchApiServiceWorld.uninstall();
        membershipServiceWorld.uninstall();
        membershipSearchApiWorld.uninstall();
        membershipOfferServiceWorld.uninstall();
        LOGGER.info("End uninstall environment for a scenario");
    }

}
