package com.odigeo.membership.robots.steps;

import com.google.inject.Inject;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.robots.mocks.membershipofferservice.MembershipOfferServiceEndpointName;
import com.odigeo.membership.robots.mocks.membershipofferservice.MembershipOfferServiceWorld;
import com.odigeo.membership.robots.utils.FunctionalUtils;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@SuppressWarnings("PMD.NullAssignment")
@ScenarioScoped
public class MembershipOfferSteps {

    private static final String CURRENCY_CODE = "currencyCode";
    private static final String WEBSITE = "website";
    private static final String PRICE = "price";
    private static final String RENEWAL_PRICE = "renewalPrice";
    private static final String DURATION = "duration";
    private static final String RENEWAL_DURATION = "renewalDuration";
    private static final String PRODUCT_ID = "productId";
    private static final String DEFAULT_CURRENCY_CODE = "EUR";
    private static final String DEFAULT_WEBSITE = "FR";
    private static final String DEFAULT_RENEWAL_PRICE = "54.99";
    private static final String DEFAULT_DURATION = "12";
    private static final String DEFAULT_RENEWAL_DURATION = "12";
    private static final String DEFAULT_PRODUCT_ID = "50505";


    private final MembershipOfferServiceWorld membershipOfferServiceWorld;
    private final AtomicLong offerId = new AtomicLong(9191000L);

    @Inject
    public MembershipOfferSteps(MembershipOfferServiceWorld membershipOfferServiceWorld) {
        this.membershipOfferServiceWorld = membershipOfferServiceWorld;
    }

    @Given("^the following membership renewal offers exist:")
    public void membershipOffersExist(DataTable offerDataTable) {
        List<Map<String, String>> membershipOfferParameters = FunctionalUtils.asMapsWithNullsAndEmptyStrings(offerDataTable);

        List<MembershipSubscriptionOffer.Builder> offers = membershipOfferParameters.stream()
                .map(this::toSubscriptionOfferBuilder)
                .collect(Collectors.toList());
        membershipOfferServiceWorld.addMockedResponseEntities(MembershipOfferServiceEndpointName.GET_OFFER, offers);
    }

    private MembershipSubscriptionOffer.Builder toSubscriptionOfferBuilder(Map<String, String> offerParameters) {
        return new MembershipSubscriptionOffer.Builder()
                .setCurrencyCode(offerParameters.getOrDefault(CURRENCY_CODE, DEFAULT_CURRENCY_CODE))
                .setPrice(Optional.ofNullable(offerParameters.get(PRICE)).map(BigDecimal::new).orElse(null))
                .setRenewalPrice(new BigDecimal(offerParameters.getOrDefault(RENEWAL_PRICE, DEFAULT_RENEWAL_PRICE)))
                .setWebsite(offerParameters.getOrDefault(WEBSITE, DEFAULT_WEBSITE))
                .setDuration(Integer.parseInt(offerParameters.getOrDefault(DURATION, DEFAULT_DURATION)))
                .setRenewalDuration(Integer.parseInt(offerParameters.getOrDefault(RENEWAL_DURATION, DEFAULT_RENEWAL_DURATION)))
                .setOfferId(String.valueOf(offerId.incrementAndGet()))
                .setProductId(Long.valueOf(offerParameters.getOrDefault(PRODUCT_ID, DEFAULT_PRODUCT_ID)));
    }
}
