package com.odigeo.membership.robots.mocks.membershipsearchapi;

import com.google.inject.Inject;
import com.odigeo.membership.robots.mocks.InspectableWorld;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Objects;

@ScenarioScoped
public class MembershipSearchApiWorld extends InspectableWorld<MembershipSearchApiMock, MembershipSearchApiServer, MembershipSearchApiEndpointName> {

    @Inject
    public MembershipSearchApiWorld(MembershipSearchApiMock membershipSearchApiMock, MembershipSearchApiServer membershipSearchApiServer) {
        super(membershipSearchApiMock, membershipSearchApiServer);
        addValidatorFor("any request", Objects::nonNull);
    }

    @Override
    public MembershipSearchApiEndpointName findEndpointName(String endpointName) {
        return MembershipSearchApiEndpointName.valueOf(endpointName);
    }

    @Override
    public MembershipSearchApiEndpointName[] findEndpointNames() {
        return MembershipSearchApiEndpointName.values();
    }
}
