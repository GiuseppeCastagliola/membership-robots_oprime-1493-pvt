Feature:  Membership subscription offer service returns invalid responses

  Scenario Outline: Null MembershipSubscriptionOffer returned, or offer with null elements which are required
    Given the following membership exists and has a prime subscription booking:
      | name              | Javi                    |
      | lastNames         | Harvey                  |
      | membershipId      | 20001                   |
      | memberAccountId   | 2001                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 22.22                   |
      | website           | <memberWebsite>         |
      | sourceType        | POST_BOOKING            |

    And the following membership renewal offers exist:
      | website | currencyCode   | price   |
      | FR      | <currencyCode> | <price> |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200
    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "<memberWebsite>"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "20001"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was never called
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was never called
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was never called
    And the GET_BOOKING endpoint of BOOKING_API service was never called
    And the UPDATE_BOOKING endpoint of BOOKING_API service was never called
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called

    Examples:
      | memberWebsite | currencyCode | price      |
      | ZZ            | EUR          | 54.99      |
      | FR            | NULL_VALUE   | 54.99      |
      | FR            | EUR          | NULL_VALUE |
      | FR            | NULL_VALUE   | NULL_VALUE |
