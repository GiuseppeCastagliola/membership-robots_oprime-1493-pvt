Feature: Edge cases caused by irregular data or failures during previous execution handled correctly

  Scenario Outline: No membershipType label in feeContainer but membership fee is found and processed correctly
    Given the following membership exists and has a prime subscription booking:
      | name              | Pablo                   |
      | lastNames         | Gomez                   |
      | membershipId      | 31333                   |
      | memberAccountId   | 3133                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 4.40                    |
      | website           | OPFR                    |
      | sourceType        | POST_BOOKING            |
      | membershipType    | EMPTY_STRING            |
      | subCode           | <feeSubCode>            |

    And the following membership renewal offers exist:
      | website | price |
      | OPFR    | 54.99 |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "OPFR"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "31333"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "3133"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 1 times
    And the GET_BOOKING endpoint of BOOKING_API service was called 1 times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called 1 times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestCode value "<feeSubCode>"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-4.40"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestCode value "AC12"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "4.40"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called 1 times

    Examples:
      | feeSubCode |
      | AE10       |
      | AE11       |


  Scenario: Expiration robot consumes remnant balance successfully after failure of CONSUME_MEMBERSHIP_REMNANT_BALANCE during preceding execution
    Given the following membership exists and has a prime subscription booking:
      | name              | Pablo                   |
      | lastNames         | Gomez                   |
      | membershipId      | 80008                   |
      | memberAccountId   | 8008                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 22.22                   |
      | website           | OPFR                    |
      | sourceType        | POST_BOOKING            |

    And the following membership renewal offers exist:
      | website | price |
      | OPFR    | 54.99 |

    And CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MembershipService returns an error response for membershipId 80008

    # On the first run the CONSUME_MEMBERSHIP_REMNANT_BALANCE will return and error.
    When the MembershipExpiryRobot is executed
    And the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    # There is now an EXPIRED membership with a positive balance which will be found on second run
    And the MembershipExpiryRobot is executed
    And the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    Then the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 12 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "OPFR"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "80008"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called 1 times
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "8008"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 2 times
    And the GET_BOOKING endpoint of BOOKING_API service was called 2 times
    # On second run we find the AC12 fee that was added during first run, so UPDATE_BOOKING isn't called a second time
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called 1 times
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called 2 times


  Scenario: Remnant balance consumed correctly if membership is EXPIRED with positive balance, but remnant fee has already been processed.
    Given the following membership exists:
      | name              | Pablo                   |
      | lastNames         | Gomez                   |
      | membershipId      | 11111                   |
      | memberAccountId   | 1111                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | EXPIRED                 |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 3.33                    |
      | website           | OPFR                    |
      | sourceType        | POST_BOOKING            |

    And the following membership renewal offers exist:
      | website | price |
      | OPFR    | 54.99 |

    And the following booking exists:
      | bookingId                  | 11011 |
      | membershipId               | 11111 |
      | memberAccountId            | 1111  |
      | isBookingSubscriptionPrime | true  |
      | currencyCode               | EUR   |

    And booking 11011 has custom feeContainers
    And booking 11011 has the following feeContainers:
      | feeContainerId | feeContainerType        | totalAmount |
      | 999001         | MEMBERSHIP_SUBSCRIPTION | 54.99       |

    And feeContainer 999001 in booking 11011 has the following fees:
      | subCode | amount |
      | AE11    | 54.99  |

    And booking 11011 has the following feeContainers:
      | feeContainerId | feeContainerType | totalAmount |
      | 999002         | BOOKING          | 100         |

    And feeContainer 999002 in booking 11011 has the following fees:
      | subCode | amount |
      | AE11    | -3.33  |
      | AC12    | 3.33   |

    When the MembershipExpiryRobot is executed
    And the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    Then the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was never called
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was never called
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was never called
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 1 times
    And the GET_BOOKING endpoint of BOOKING_API service was called 1 times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called 0 times
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called 1 times
