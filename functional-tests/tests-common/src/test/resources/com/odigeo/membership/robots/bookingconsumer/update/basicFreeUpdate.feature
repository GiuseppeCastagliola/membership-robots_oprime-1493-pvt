Feature: basic free update

  Scenario Outline: when a booking is made using a basic-free type of subscription we need to check if is the first one,
  and in this case update the booking adding the membership product and enable the auto-renewal. We need to be sure that was the 1st booking (no other subscription booking exists for that membership),
  that auto-renewal is not already enabled, and booking should be contract and membership activated.

    Given the following booking exists:
      | bookingId                  | 6653452         |
      | membershipId               | 909090          |
      | memberAccountId            | 1111            |
      | currencyCode               | EUR             |
      | bookingStatus              | <bookingStatus> |
      | isBookingSubscriptionPrime | false           |

    And booking 6653452 has the following feeContainers:
      | feeContainerId | feeContainerType | totalAmount |
      | 3243223        | MEMBERSHIP_PERKS | -28         |

    And the following booking exists:
      | bookingId                  | 676462                       |
      | membershipId               | 909090                       |
      | memberAccountId            | 1111                         |
      | currencyCode               | EUR                          |
      | bookingStatus              | CONTRACT                     |
      | isBookingSubscriptionPrime | <isBookingSubscriptionPrime> |

    And the following membership exists:
      | name              | Pablo               |
      | lastNames         | Gomez               |
      | userId            | 65000               |
      | membershipId      | 909090              |
      | memberAccountId   | 1111                |
      | membershipType    | BASIC_FREE          |
      | membershipStatus  | <membershipStatus>  |
      | autoRenewalStatus | <autoRenewalStatus> |

    When the BookingUpdatesConsumer is executed
    And the BookingUpdatesConsumer status is RUNNING
    And following booking ids are published in BOOKING_UPDATES_v1
      | 6653452 |

    Then the GET_BOOKING endpoint of BOOKING_API service was <getBookingCalls>
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was <searchBookingsCalls>
    And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was <getMembershipCalls>
    And the UPDATE_BOOKING endpoint of BOOKING_API service was <updateBookingCalls>
    And the ENABLE_AUTO_RENEWAL endpoint of MEMBERSHIP service was <updateMembershipCalls>

    Examples:
      | bookingStatus | membershipStatus | autoRenewalStatus | isBookingSubscriptionPrime | getBookingCalls | getMembershipCalls | searchBookingsCalls | updateBookingCalls | updateMembershipCalls |
      | CONTRACT      | ACTIVATED        | DISABLED          | false                      | called 1 times  | called 1 time      | called 1 time       | called 1 time      | called 1 time         |
      | CONTRACT      | ACTIVATED        | DISABLED          | true                       | called 1 times  | called 1 time      | called 1 time       | never called       | never called          |
      | CONTRACT      | ACTIVATED        | ENABLED           | false                      | called 1 times  | called 1 time      | never called        | never called       | never called          |
      | CONTRACT      | DISCARDED        | DISABLED          | false                      | called 1 times  | called 1 time      | never called        | never called       | never called          |
      | INIT          | ACTIVATED        | DISABLED          | false                      | called 1 times  | called 1 time      | never called        | never called       | never called          |