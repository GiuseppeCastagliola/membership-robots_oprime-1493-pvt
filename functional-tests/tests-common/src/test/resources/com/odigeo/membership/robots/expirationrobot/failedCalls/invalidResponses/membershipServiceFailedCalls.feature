Feature: Membership Service returns invalid responses

  Scenario: Previous membership has null expiration date
    Given the following membership exists and has a prime subscription booking:
      | name              | Stan                    |
      | lastNames         | Lee                     |
      | membershipId      | 60001                   |
      | memberAccountId   | 6001                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-01T02:40:00.000 |
      | expirationDate    | NULL_VALUE              |
      | balance           | 9.99                    |
      | website           | FR                      |
      | sourceType        | POST_BOOKING            |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200
    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "FR"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "60001"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was never called
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was never called
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was never called
    And the GET_BOOKING endpoint of BOOKING_API service was never called
    And the UPDATE_BOOKING endpoint of BOOKING_API service was never called
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called


  Scenario Outline: Update membership call fails
    Given the following membership exists and has a prime subscription booking:
      | name              | Pablo                   |
      | lastNames         | Gomez                   |
      | membershipId      | 80008                   |
      | memberAccountId   | 8008                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 22.22                   |
      | website           | OPFR                    |
      | sourceType        | POST_BOOKING            |

    And the following membership renewal offers exist:
      | website | price |
      | OPFR    | 54.99 |

    And <endpoint> endpoint of MembershipService returns an error response for membershipId 80008

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "OPFR"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "80008"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "8008"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called <postExpireCalls> times
    And the GET_BOOKING endpoint of BOOKING_API service was called <postExpireCalls> times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called <postExpireCalls> times
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called <postExpireCalls> times

    Examples:
      | endpoint                           | postExpireCalls |
      | EXPIRE_MEMBERSHIP                  | 0               |
      | CONSUME_MEMBERSHIP_REMNANT_BALANCE | 1               |
