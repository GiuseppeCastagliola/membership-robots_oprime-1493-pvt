Feature: Chargeback logic, to deactivate a membership with chargeback:
  - be prime booking standalone
  - be subscription or renewal booking

  Scenario Outline: deactivate membership by chargeback
    Given the following booking exists:
      | bookingId                  | 6653452          |
      | membershipId               | 909090           |
      | memberAccountId            | 1111             |
      | isBookingSubscriptionPrime | <isSubscription> |
      | isRenewalBooking           | <isRenewal>      |
      | currencyCode               | EUR              |
      | bookingStatus              | CONTRACT         |

      And the following membership exists:
        | name             | Pablo     |
        | lastNames        | Gomez     |
        | membershipId     | 909090    |
        | memberAccountId  | 1111      |
        | membershipStatus | ACTIVATED |

    When the BookingChargebackConsumerRobot is executed
      And following movement with action chargeback is published in CHARGEBACK_MOVEMENT_STORED
        | bookingId | 6653452      |
        | status    | CHARGEBACKED |
        | action    | CHARGEBACK   |

    Then the GET_BOOKING endpoint of BOOKING_API service was called 1 time
      And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was called 1 time
      And the DEACTIVATE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 time
      And the DEACTIVATE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToDeactivate value "909090"
      And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called 1 times

    Examples:
      | isSubscription | isRenewal |
      | true           | false     |
      | false          | true      |

  Scenario Outline: an error occurred when the membership was going to be deactivated
    Given the following booking exists:
      | bookingId                  | 6653452          |
      | membershipId               | 909090           |
      | memberAccountId            | 1111             |
      | isBookingSubscriptionPrime | <isSubscription> |
      | isRenewalBooking           | <isRenewal>      |
      | currencyCode               | EUR              |
      | bookingStatus              | CONTRACT         |

      And the following membership exists:
        | name             | Pablo     |
        | lastNames        | Gomez     |
        | membershipId     | 909090    |
        | memberAccountId  | 1111      |
        | membershipStatus | ACTIVATED |

      And DEACTIVATE_MEMBERSHIP endpoint of MembershipService returns an error response for membershipId 909090

    When the BookingChargebackConsumerRobot is executed
      And following movement with action chargeback is published in CHARGEBACK_MOVEMENT_STORED
        | bookingId | 6653452      |
        | status    | CHARGEBACKED |
        | action    | CHARGEBACK   |

    Then the GET_BOOKING endpoint of BOOKING_API service was called 1 time
      And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was called 1 time
      And the DEACTIVATE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 time
      And the DEACTIVATE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToDeactivate value "909090"
      And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called

    Examples:
      | isSubscription | isRenewal |
      | true           | false     |
      | false          | true      |


  Scenario Outline: the membership is not deactivated because:
  - the product is not standalone
  - the membership is already deactivated
    Given the following booking exists:
      | bookingId                  | 6653452                    |
      | membershipId               | 909090                     |
      | memberAccountId            | 1111                       |
      | isBookingSubscriptionPrime | true                       |
      | isThereAdditionalProduct   | <isThereAdditionalProduct> |
      | currencyCode               | EUR                        |
      | bookingStatus              | CONTRACT                   |

      And the following membership exists:
        | name             | Pablo              |
        | lastNames        | Gomez              |
        | membershipId     | 909090             |
        | memberAccountId  | 1111               |
        | membershipStatus | <membershipStatus> |

    When the BookingChargebackConsumerRobot is executed
      And following movement with action chargeback is published in CHARGEBACK_MOVEMENT_STORED
        | bookingId | 6653452      |
        | status    | CHARGEBACKED |
        | action    | CHARGEBACK   |

    Then the GET_BOOKING endpoint of BOOKING_API service was called 1 time
      And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was <getMembershipCalls>
      And the DEACTIVATE_MEMBERSHIP endpoint of MEMBERSHIP service was never called
      And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was <consumeMembershipRemnantBalanceCalls>

    Examples:
      | membershipStatus | isThereAdditionalProduct | getMembershipCalls | consumeMembershipRemnantBalanceCalls |
      | ACTIVATED        | true                     | never called       | never called                         |
      | DEACTIVATED      | false                    | called 1 time      | called 1 time                        |


  Scenario: the product is not prime
    Given the following booking exists:
      | bookingId                  | 6653452  |
      | membershipId               | 0        |
      | memberAccountId            | 0        |
      | isBookingSubscriptionPrime | false    |
      | isRenewalBooking           | false    |
      | isThereAdditionalProduct   | true     |
      | currencyCode               | EUR      |
      | bookingStatus              | CONTRACT |

    When the BookingChargebackConsumerRobot is executed
      And following movement with action chargeback is published in CHARGEBACK_MOVEMENT_STORED
        | bookingId | 6653452      |
        | status    | CHARGEBACKED |
        | action    | CHARGEBACK   |

    Then the GET_BOOKING endpoint of BOOKING_API service was called 1 time
      And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was never called
      And the DEACTIVATE_MEMBERSHIP endpoint of MEMBERSHIP service was never called
      And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called


  Scenario: the movement is not chargebacked

    When the BookingChargebackConsumerRobot is executed
      And following movement with action chargeback is published in CHARGEBACK_MOVEMENT_STORED
        | bookingId | 6653452                |
        | status    | CHARGEBACKED_CANCELLED |
        | action    | CHARGEBACK             |

    Then the GET_BOOKING endpoint of BOOKING_API service was never called
      And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was never called
      And the DEACTIVATE_MEMBERSHIP endpoint of MEMBERSHIP service was never called
      And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called
