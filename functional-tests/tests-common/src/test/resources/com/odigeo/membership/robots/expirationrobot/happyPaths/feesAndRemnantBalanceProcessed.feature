Feature: Booking fees, membership fees, and membership remnant balance successfully processed

  Scenario Outline: Positive balance of RENEWABLE membership is successfully consumed
    Given the following membership exists and has a prime subscription booking:
      | name              | Javi                    |
      | lastNames         | Harvey                  |
      | membershipId      | 20001                   |
      | memberAccountId   | 2001                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 22.22                   |
      | website           | <website>               |
      | sourceType        | POST_BOOKING            |
      | membershipType    | <membershipType>        |

    And the following membership renewal offers exist:
      | website | price |
      | FR      | 54.99 |
      | ES      | 44.99 |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "<website>"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "20001"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called 1 times
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "2001"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with newExpirationDate value "1 YEAR FROM NOW"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called with membershipId value "20001"
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called with isBookingSubscriptionPrime value "true"
    And the GET_BOOKING endpoint of BOOKING_API service was called 1 times
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "20001"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called 1 times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestCode value "<feeRequestCode>"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-22.22"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestCode value "AC12"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "22.22"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called 1 times

    Examples:
      | membershipType          | feeRequestCode | website |
      | MEMBERSHIP_SUBSCRIPTION | AE11           | FR      |
      | MEMBERSHIP_RENEWAL      | AE10           | ES      |
    #    Using 2 different website offers as the first one called (French) will be cached in the websiteOffers Map of ExternalModuleManagerBean after the first call.
    #    That means GET_OFFER endpoint would not be called for the same country in the 2nd scenario


  Scenario Outline: Positive balance of NON-RENEWABLE membership is successfully consumed
    Given the following membership exists and has a prime subscription booking:
      | name              | <name>              |
      | lastNames         | <lastNames>         |
      | membershipId      | <membershipId>      |
      | memberAccountId   | <memberAccountId>   |
      | autoRenewalStatus | <autoRenewalStatus> |
      | membershipStatus  | <membershipStatus>  |
      | activationDate    | <activationDate>    |
      | expirationDate    | <expirationDate>    |
      | balance           | <balance>           |
      | website           | FR                  |
      | sourceType        | <sourceType>        |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200
    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was never called
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was never called
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 1 times
    And the GET_BOOKING endpoint of BOOKING_API service was called 1 times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called 1 times
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called 1 times

    Examples:
      | name  | lastNames | membershipId | memberAccountId | autoRenewalStatus | membershipStatus    | activationDate          | expirationDate | balance | sourceType   |
      | Alice | Doe       | 10001        | 1001            | ENABLED           | PENDING_TO_COLLECT  | 2019-12-13T02:00:00.000 | TODAY          | 11.11   | POST_BOOKING |
      | Max   | Poker     | 30001        | 3001            | DISABLED          | ACTIVATED           | 2019-12-13T02:00:00.000 | TODAY          | 2.22    | POST_BOOKING |
      | Ana   | Lana      | 40001        | 4001            | ENABLED           | PENDING_TO_ACTIVATE | 2019-12-13T02:00:00.000 | TODAY          | 3.33    | POST_BOOKING |
      | Dee   | Artiva    | 50001        | 5001            | ENABLED           | DEACTIVATED         | 2019-12-13T02:00:00.000 | TODAY          | 44.44   | POST_BOOKING |


  Scenario: Positive balance of EXPIRED NON-RENEWABLE membership is successfully consumed
    Given the following membership exists and has a prime subscription booking:
      | name              | Stan                    |
      | lastNames         | Lee                     |
      | membershipId      | 60001                   |
      | memberAccountId   | 6001                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | EXPIRED                 |
      | activationDate    | 2019-10-01T02:40:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 9.99                    |
      | website           | FR                      |
      | sourceType        | POST_BOOKING            |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200
    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was never called
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was never called
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was never called
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 1 times
    And the GET_BOOKING endpoint of BOOKING_API service was called 1 times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called 1 times
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called 1 times
