Feature: Recurring processing handle correctly exception returned by service calls

  Scenario Outline: call update membership only if no exceptions occurs and schedule retries for exception received
    Given the following booking exists:
      | bookingId                       | <bookingId> |
      | membershipId                    | 909090      |
      | memberAccountId                 | 1111        |
      | isBookingSubscriptionPrime      | true        |
      | currencyCode                    | EUR         |
      | withRecurringAndValidCollection | true        |
      | bookingStatus                   | CONTRACT    |

    And the following membership exists:
      | name            | Pablo  |
      | lastNames       | Gomez  |
      | membershipId    | 909090 |
      | memberAccountId | 1111   |

    And <endpoint> endpoint of <service> service will throw an exception

    When the BookingUpdatesConsumer is executed
    And the BookingUpdatesConsumer status is RUNNING
    And following booking ids are published in BOOKING_UPDATES_v1
      | <bookingId> |

    Then after 1 seconds
    And the GET_BOOKING endpoint of BOOKING_API service was <bookingCalls>
    And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was <getMembershipCalls>
    And the INSERT_RECURRING_ID endpoint of MEMBERSHIP service was <recurringCalls>
    And the BookingUpdatesConsumer status is RUNNING


    Examples:
      | endpoint            | service               | bookingCalls  | getMembershipCalls | recurringCalls | bookingId |
      | GET_BOOKING         | BOOKING_API           | called 2 time | called 1 time      | called 1 time  | 6650000   |
      | GET_MEMBERSHIP      | MEMBERSHIP_SEARCH_API | called 2 time | called 2 time      | called 1 time  | 6651111   |
      | INSERT_RECURRING_ID | MEMBERSHIP            | called 2 time | called 2 time      | called 2 time  | 6652222   |
