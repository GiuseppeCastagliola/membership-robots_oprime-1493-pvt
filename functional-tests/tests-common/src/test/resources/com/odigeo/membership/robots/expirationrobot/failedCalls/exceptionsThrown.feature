Feature: Exceptions thrown by endpoints

  Scenario Outline: Test ExpirationRobot tolerance of endpoint exceptions
    Given the following membership exists and has a prime subscription booking:
      | name              | Javi                    |
      | lastNames         | Harvey                  |
      | membershipId      | 20001                   |
      | memberAccountId   | 2001                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 22.22                   |
      | website           | ES                      |
      | sourceType        | POST_BOOKING            |
      | membershipType    | MEMBERSHIP_SUBSCRIPTION |

    And the following membership renewal offers exist:
      | website | price |
      | ES      | 44.99 |

    And <endpoint> endpoint of <service> service will throw an exception

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was <getOfferCalled>
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was <createPendingToCollectCalled>
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was <expireMembershipCalled>
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was <searchBookingsCalled>
    And the GET_BOOKING endpoint of BOOKING_API service was <getBookingCalled>
    And the UPDATE_BOOKING endpoint of BOOKING_API service was <updateBookingCalled>
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was <consumeBalanceCalled>

    Examples:
      | endpoint                           | service               | getOfferCalled | createPendingToCollectCalled | expireMembershipCalled | searchBookingsCalled | getBookingCalled | updateBookingCalled | consumeBalanceCalled |
      | SEARCH_MEMBERSHIPS                 | MEMBERSHIP_SEARCH_API | never called   | never called                 | never called           | never called         | never called     | never called        | never called         |
      | GET_OFFER                          | MEMBERSHIP_OFFER      | called 1 time  | never called                 | never called           | never called         | never called     | never called        | never called         |
      | CREATE_PENDING_TO_COLLECT          | MEMBERSHIP            | called 1 time  | called 1 time                | never called           | never called         | never called     | never called        | never called         |
      | EXPIRE_MEMBERSHIP                  | MEMBERSHIP            | called 1 time  | called 1 time                | called 1 time          | never called         | never called     | never called        | never called         |
      | SEARCH_BOOKINGS                    | BOOKING_SEARCH_API    | called 1 time  | called 1 time                | called 1 time          | called 1 time        | never called     | never called        | never called         |
      | GET_BOOKING                        | BOOKING_API           | called 1 time  | called 1 time                | called 1 time          | called 1 time        | called 1 time    | never called        | never called         |
      | UPDATE_BOOKING                     | BOOKING_API           | called 1 time  | called 1 time                | called 1 time          | called 1 time        | called 1 time    | called 1 time       | never called         |
      | CONSUME_MEMBERSHIP_REMNANT_BALANCE | MEMBERSHIP            | called 1 time  | called 1 time                | called 1 time          | called 1 time        | called 1 time    | called 1 time       | called 1 time        |
