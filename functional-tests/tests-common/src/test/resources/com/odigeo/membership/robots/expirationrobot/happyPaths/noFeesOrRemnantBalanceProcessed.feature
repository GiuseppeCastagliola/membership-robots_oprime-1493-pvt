Feature: No booking fees, membership fees, or membership remnant balance processed

  Scenario Outline: Membership and booking endpoints not called when no RENEWABLE or NON-RENEWABLE memberships found
    Given the following membership exists and has a prime subscription booking:
      | name              | <name>              |
      | lastNames         | <lastNames>         |
      | membershipId      | <membershipId>      |
      | memberAccountId   | <memberAccountId>   |
      | autoRenewalStatus | <autoRenewalStatus> |
      | membershipStatus  | <membershipStatus>  |
      | activationDate    | <activationDate>    |
      | expirationDate    | <expirationDate>    |
      | balance           | <balance>           |
      | website           | FR                  |
      | sourceType        | <sourceType>        |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200
    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was never called
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was never called
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was never called
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was never called
    And the GET_BOOKING endpoint of BOOKING_API service was never called
    And the UPDATE_BOOKING endpoint of BOOKING_API service was never called
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called

    Examples:
      | name  | lastNames | membershipId | memberAccountId | autoRenewalStatus | membershipStatus | activationDate          | expirationDate | balance | sourceType   |
      | Exp   | Tomoro    | 10001        | 1001            | ENABLED           | ACTIVATED        | 2019-12-13T02:00:00.000 | 1 DAY FROM NOW | 11.11   | POST_BOOKING |
      | Exp   | Yest      | 30001        | 3001            | ENABLED           | EXPIRED          | 2019-12-13T02:00:00.000 | 1 DAY AGO      | 2.22    | POST_BOOKING |
      | Activ | B4cutoff  | 40001        | 4001            | ENABLED           | EXPIRED          | 2019-09-30T00:00:00.000 | TODAY          | 3.33    | POST_BOOKING |
      | Zero  | Balance   | 50001        | 5001            | ENABLED           | EXPIRED          | 2019-12-13T02:00:00.000 | TODAY          | 0.00    | POST_BOOKING |


  Scenario: RENEWABLE membership with ZERO BALANCE is EXPIRED after PENDING_TO_COLLECT created. No fees processed.
    Given the following membership renewal offers exist:
      | website | price |
      | IT      | 54.99 |

    #    Adding Italian offer as the offers requested in scenarios above will already be cached in the websiteOffers Map of ExternalModuleManagerBean.
    #    That means GET_OFFER would not be called during this scenario
    And the following membership exists and has a prime subscription booking:
      | name              | Jose                    |
      | lastNames         | Harvey                  |
      | membershipId      | 20001                   |
      | memberAccountId   | 2001                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-03T02:40:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 0.00                    |
      | website           | IT                      |
      | sourceType        | POST_BOOKING            |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called 1 times
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was never called
    And the GET_BOOKING endpoint of BOOKING_API service was never called
    And the UPDATE_BOOKING endpoint of BOOKING_API service was never called
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called


  Scenario Outline: NON-RENEWABLE memberships with ZERO BALANCE are EXPIRED. No PENDING_TO_COLLECT created or fees processed.
    Given the following membership exists and has a prime subscription booking:
      | name              | <name>                  |
      | lastNames         | <lastNames>             |
      | membershipId      | <membershipId>          |
      | memberAccountId   | <memberAccountId>       |
      | autoRenewalStatus | <autoRenewalStatus>     |
      | membershipStatus  | <membershipStatus>      |
      | activationDate    | 2019-12-13T02:00:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 0.00                    |
      | website           | FR                      |
      | sourceType        | POST_BOOKING            |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200
    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was never called
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was never called
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was never called
    And the GET_BOOKING endpoint of BOOKING_API service was never called
    And the UPDATE_BOOKING endpoint of BOOKING_API service was never called
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called

    Examples:
      | name  | lastNames | membershipId | memberAccountId | autoRenewalStatus | membershipStatus    |
      | Alice | Doe       | 10001        | 1001            | ENABLED           | PENDING_TO_COLLECT  |
      | Max   | Poker     | 30001        | 3001            | DISABLED          | ACTIVATED           |
      | Ana   | Lana      | 40001        | 4001            | ENABLED           | PENDING_TO_ACTIVATE |
      | Dee   | Artiva    | 50001        | 5001            | ENABLED           | DEACTIVATED         |
