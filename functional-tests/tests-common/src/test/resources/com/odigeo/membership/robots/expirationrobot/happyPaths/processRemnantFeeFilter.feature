Feature: Process Remnant Fee Filter functions correctly

  Scenario Outline: Activation date predicate in PROCESS REMNANT FEE filter removes relevant memberships
    Given the following membership exists and has a prime subscription booking:
      | name              | Alfie                   |
      | lastNames         | Bravo                   |
      | membershipId      | 10101                   |
      | memberAccountId   | 1001                    |
      | autoRenewalStatus | <autoRenewalStatus>     |
      | membershipStatus  | <membershipStatus>      |
      | activationDate    | 2019-09-30T00:00:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 1.00                    |
      | website           | FR                      |
      | sourceType        | POST_BOOKING            |
    And the following membership renewal offers exist:
      | website | price |
      | FR      | 54.99 |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200
    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called <getOfferCalled> times
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called <createPendingToCollectCalled> times
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was never called
    And the GET_BOOKING endpoint of BOOKING_API service was never called
    And the UPDATE_BOOKING endpoint of BOOKING_API service was never called
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called

    Examples:
      | membershipStatus    | autoRenewalStatus | getOfferCalled | createPendingToCollectCalled |
      | ACTIVATED           | ENABLED           | 1              | 1                            |
      | DEACTIVATED         | ENABLED           | 0              | 0                            |
      | PENDING_TO_COLLECT  | ENABLED           | 0              | 0                            |
      | ACTIVATED           | DISABLED          | 0              | 0                            |
      | PENDING_TO_ACTIVATE | ENABLED           | 0              | 0                            |


  Scenario Outline: Predicates in PROCESS REMNANT FEE filter remove relevant memberships
    Given the following membership exists and has a prime subscription booking:
      | name              | Alfie               |
      | lastNames         | Bravo               |
      | membershipId      | 10101               |
      | memberAccountId   | 1001                |
      | autoRenewalStatus | <autoRenewalStatus> |
      | membershipStatus  | <membershipStatus>  |
      | activationDate    | <activationDate>    |
      | expirationDate    | TODAY               |
      | balance           | <balance>           |
      | website           | FR                  |
      | sourceType        | POST_BOOKING        |
      | productStatus     | <productStatus>     |
    And the following membership renewal offers exist:
      | website | price |
      | FR      | 54.99 |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200
    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called <getOfferCalled> times
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called <createPendingToCollectCalled> times
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was never called
    And the GET_BOOKING endpoint of BOOKING_API service was never called
    And the UPDATE_BOOKING endpoint of BOOKING_API service was never called
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called

    Examples:
      | membershipStatus    | autoRenewalStatus | activationDate          | balance    | productStatus | getOfferCalled | createPendingToCollectCalled |
      # test activated before cutover
      | ACTIVATED           | ENABLED           | 2019-09-30T00:00:00.000 | 1.00       | NULL_VALUE    | 1              | 1                            |
      | DEACTIVATED         | ENABLED           | 2019-09-30T00:00:00.000 | 1.00       | NULL_VALUE    | 0              | 0                            |
      | PENDING_TO_COLLECT  | ENABLED           | 2019-09-30T00:00:00.000 | 1.00       | NULL_VALUE    | 0              | 0                            |
      | ACTIVATED           | DISABLED          | 2019-09-30T00:00:00.000 | 1.00       | NULL_VALUE    | 0              | 0                            |
      | PENDING_TO_ACTIVATE | ENABLED           | 2019-09-30T00:00:00.000 | 1.00       | NULL_VALUE    | 0              | 0                            |
      # test positive balance
      | ACTIVATED           | ENABLED           | 2019-10-01T02:40:00.000 | 0.00       | NULL_VALUE    | 1              | 1                            |
      | ACTIVATED           | ENABLED           | 2019-10-01T02:40:00.000 | -4.44      | NULL_VALUE    | 1              | 1                            |
      | ACTIVATED           | ENABLED           | 2019-10-01T02:40:00.000 | NULL_VALUE | NULL_VALUE    | 1              | 1                            |
      | DEACTIVATED         | ENABLED           | 2019-10-01T02:40:00.000 | 0.00       | NULL_VALUE    | 0              | 0                            |
      | DEACTIVATED         | ENABLED           | 2019-10-01T02:40:00.000 | -4.44      | NULL_VALUE    | 0              | 0                            |
      | DEACTIVATED         | ENABLED           | 2019-10-01T02:40:00.000 | NULL_VALUE | NULL_VALUE    | 0              | 0                            |
      | PENDING_TO_COLLECT  | ENABLED           | 2019-10-01T02:40:00.000 | 0.00       | NULL_VALUE    | 0              | 0                            |
      | PENDING_TO_COLLECT  | ENABLED           | 2019-10-01T02:40:00.000 | -4.44      | NULL_VALUE    | 0              | 0                            |
      | PENDING_TO_COLLECT  | ENABLED           | 2019-10-01T02:40:00.000 | NULL_VALUE | NULL_VALUE    | 0              | 0                            |
      | ACTIVATED           | DISABLED          | 2019-10-01T02:40:00.000 | 0.00       | NULL_VALUE    | 0              | 0                            |
      | ACTIVATED           | DISABLED          | 2019-10-01T02:40:00.000 | -4.44      | NULL_VALUE    | 0              | 0                            |
      | ACTIVATED           | DISABLED          | 2019-10-01T02:40:00.000 | NULL_VALUE | NULL_VALUE    | 0              | 0                            |
      | PENDING_TO_ACTIVATE | ENABLED           | 2019-10-01T02:40:00.000 | 0.00       | NULL_VALUE    | 0              | 0                            |
      | PENDING_TO_ACTIVATE | ENABLED           | 2019-10-01T02:40:00.000 | -4.44      | NULL_VALUE    | 0              | 0                            |
      | PENDING_TO_ACTIVATE | ENABLED           | 2019-10-01T02:40:00.000 | NULL_VALUE | NULL_VALUE    | 0              | 0                            |
      # test INIT filter
      | ACTIVATED           | ENABLED           | 2019-10-01T02:40:00.000 | 9.99       | INIT          | 1              | 1                            |
      | DEACTIVATED         | ENABLED           | 2019-10-01T02:40:00.000 | 9.99       | INIT          | 0              | 0                            |
      | PENDING_TO_COLLECT  | ENABLED           | 2019-10-01T02:40:00.000 | 9.99       | INIT          | 0              | 0                            |
      | ACTIVATED           | DISABLED          | 2019-10-01T02:40:00.000 | 9.99       | INIT          | 0              | 0                            |
      | PENDING_TO_ACTIVATE | ENABLED           | 2019-10-01T02:40:00.000 | 9.99       | INIT          | 0              | 0                            |
