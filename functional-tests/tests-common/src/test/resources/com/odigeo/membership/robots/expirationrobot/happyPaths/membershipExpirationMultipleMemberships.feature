Feature: Expiration Robot called with multiple memberships existing

  Scenario: Endpoints called correct number of times and with expected values when expiration robot runs with multiple memberships

    #    FR, EX, IX values used for website to ensure the GET_OFFER request is called.
    #    Values for FR, ES, IT would have been saved any tests already executed
    Given the following memberships exist and have prime subscription bookings:
      | name  | lastNames | memberAccountId | membershipId | website | autoRenewalStatus | membershipStatus    | activationDate          | expirationDate | balance | sourceType   | membershipType          |
      | One   | ActEna    | 1101            | 11001        | FX      | ENABLED           | ACTIVATED           | 2019-10-01T02:40:00.000 | TODAY          | 1.01    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Two   | ActEna    | 1201            | 12001        | EX      | ENABLED           | ACTIVATED           | 2019-10-01T02:40:00.000 | TODAY          | 1.02    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Three | ActEna    | 1301            | 13001        | IX      | ENABLED           | ACTIVATED           | 2019-10-01T02:40:00.000 | TODAY          | 1.03    | POST_BOOKING | MEMBERSHIP_RENEWAL      |
      | One   | PendColl  | 2101            | 21001        | FX      | ENABLED           | PENDING_TO_COLLECT  | 2020-01-02T12:45:30.000 | TODAY          | 2.01    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Two   | PendColl  | 2201            | 22001        | EX      | ENABLED           | PENDING_TO_COLLECT  | 2020-01-02T12:45:30.000 | TODAY          | 2.02    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Three | PendColl  | 2301            | 23001        | FX      | ENABLED           | PENDING_TO_COLLECT  | 2020-01-02T12:45:30.000 | TODAY          | 2.03    | POST_BOOKING | MEMBERSHIP_RENEWAL      |
      | One   | ActDisab  | 3101            | 31001        | FX      | DISABLED          | ACTIVATED           | 2019-10-01T02:40:00.000 | TODAY          | 3.31    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Two   | ActDisab  | 3201            | 32001        | FX      | DISABLED          | ACTIVATED           | 2019-10-01T02:40:00.000 | TODAY          | 3.32    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Three | ActDisab  | 3301            | 33001        | FX      | DISABLED          | ACTIVATED           | 2019-10-01T02:40:00.000 | TODAY          | 3.33    | POST_BOOKING | MEMBERSHIP_RENEWAL      |
      | One   | PendAct   | 4101            | 41001        | IX      | ENABLED           | PENDING_TO_ACTIVATE | 2020-03-01T12:45:30.000 | TODAY          | 4.41    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Two   | PendAct   | 4201            | 42001        | IX      | ENABLED           | PENDING_TO_ACTIVATE | 2020-03-01T12:45:30.000 | TODAY          | 4.42    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Three | PendAct   | 4301            | 43001        | IX      | ENABLED           | PENDING_TO_ACTIVATE | 2020-03-01T12:45:30.000 | TODAY          | 4.43    | POST_BOOKING | MEMBERSHIP_RENEWAL      |
      | One   | DeActiv   | 5101            | 51001        | IX      | ENABLED           | DEACTIVATED         | 2019-10-01T02:40:00.000 | TODAY          | 5.01    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Two   | DeActiv   | 5201            | 52001        | IX      | ENABLED           | DEACTIVATED         | 2020-01-03T11:45:30.000 | TODAY          | 5.02    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Three | DeActiv   | 5301            | 53001        | IX      | ENABLED           | DEACTIVATED         | 2020-04-01T11:45:30.000 | TODAY          | 5.03    | POST_BOOKING | MEMBERSHIP_RENEWAL      |
      | One   | Expir     | 6101            | 61001        | FX      | ENABLED           | EXPIRED             | 2019-10-01T02:40:00.000 | TODAY          | 0.01    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Two   | Expir     | 6201            | 62001        | FX      | ENABLED           | EXPIRED             | 2020-01-03T11:45:30.000 | TODAY          | 0.02    | POST_BOOKING | MEMBERSHIP_SUBSCRIPTION |
      | Three | Expir     | 6301            | 63001        | FX      | ENABLED           | EXPIRED             | 2020-04-01T11:45:30.000 | TODAY          | 0.03    | POST_BOOKING | MEMBERSHIP_RENEWAL      |

    And the following membership renewal offers exist:
      | website | price |
      | FX      | 54.99 |
      | IX      | 54.99 |
      | EX      | 44.99 |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times

    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 3 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "FX"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "11001"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "EX"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "12001"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "IX"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "13001"

    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called 3 times
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with newExpirationDate value "1 YEAR FROM NOW" 2 times
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with newExpirationDate value "3 MONTHS FROM NOW" 1 times
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with subscriptionPrice value "54.99" 2 times
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with subscriptionPrice value "44.99" 1 time
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "1101"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "1201"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "1301"


    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 15 times
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "11001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "12001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "13001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "21001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "22001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "23001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "31001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "32001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "33001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "41001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "42001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "43001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "51001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "52001"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called with membershipIdToExpire value "53001"

    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 18 times
    And the GET_BOOKING endpoint of BOOKING_API service was called 18 times
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "11001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "12001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "13001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "21001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "22001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "23001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "31001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "32001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "33001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "41001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "42001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "43001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "51001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "52001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "53001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "61001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "62001"
    And the GET_BOOKING endpoint of BOOKING_API service was called with bookingId value "63001"

    And the UPDATE_BOOKING endpoint of BOOKING_API service was called 18 times
    #    All MEMBERSHIP_SUBSCRIPTION / AE11 feeCodes
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestCode value "AE11" 12 times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-1.01"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-1.02"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-2.01"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-2.02"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-3.31"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-3.32"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-4.41"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-4.42"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-5.01"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-5.02"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-0.01"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-0.02"
    #    All MEMBERSHIP_RENEWAL / AE10 feeCodes
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestCode value "AE10" 6 times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-1.03"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-2.03"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-3.33"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-4.43"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-5.03"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "-0.03"
    #    AC12 / positive value calls
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestCode value "AC12" 18 times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "1.01"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "1.02"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "1.03"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "2.01"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "2.02"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "2.03"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "3.31"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "3.32"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "3.33"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "4.41"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "4.42"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "4.43"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "5.01"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "5.02"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "5.03"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "0.01"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "0.02"
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called with feeRequestAmount value "0.03"

    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called 18 times
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "11001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "12001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "13001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "21001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "22001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "23001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "31001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "32001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "33001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "41001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "42001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "43001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "51001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "52001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "53001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "61001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "62001"
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was called with membershipIdToConsume value "63001"
