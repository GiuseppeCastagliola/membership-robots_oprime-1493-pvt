Feature: BookingsApi service returns invalid responses

  Scenario: SEARCH_BOOKINGS endpoint returns empty list
    Given the following membership exists:
      | name              | Pablo                   |
      | lastNames         | Gomez                   |
      | membershipId      | 77077                   |
      | memberAccountId   | 7707                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 22.22                   |
      | website           | OPFR                    |
      | sourceType        | POST_BOOKING            |

    And the following membership renewal offers exist:
      | website | price |
      | OPFR    | 54.99 |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "OPFR"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "77077"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "7707"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 1 times
    And the GET_BOOKING endpoint of BOOKING_API service was never called
    And the UPDATE_BOOKING endpoint of BOOKING_API service was never called
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called


  Scenario Outline: SEARCH_BOOKINGS endpoint returns error responses
    Given the following membership exists:
      | name              | Pablo                   |
      | lastNames         | Gomez                   |
      | membershipId      | 77077                   |
      | memberAccountId   | 7707                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 22.22                   |
      | website           | OPFR                    |
      | sourceType        | POST_BOOKING            |

    And the following membership renewal offers exist:
      | website | price |
      | OPFR    | 54.99 |

    And SEARCH_BOOKINGS endpoint of BookingsApi service returns a <responseType> response for membershipId 77077
    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "OPFR"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "77077"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "7707"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 1 times
    And the GET_BOOKING endpoint of BOOKING_API service was never called
    And the UPDATE_BOOKING endpoint of BOOKING_API service was never called
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called

    Examples:
      | responseType |
      | null         |
      | error        |


  Scenario Outline: BookingsApi service endpoints return error responses
    Given the following membership exists and has a prime subscription booking:
      | name              | Pablo                   |
      | lastNames         | Gomez                   |
      | membershipId      | 77077                   |
      | memberAccountId   | 7707                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 22.22                   |
      | website           | OPFR                    |
      | sourceType        | POST_BOOKING            |

    And the following membership renewal offers exist:
      | website | price |
      | OPFR    | 54.99 |

    And <endpoint> endpoint of BookingsApi service returns a <responseType> response for membershipId 77077

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "OPFR"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "77077"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "7707"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 1 times
    And the GET_BOOKING endpoint of BOOKING_API service was called <getBooking> times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was called <updateBooking> times
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called

    Examples:
      | endpoint        | responseType | getBooking | updateBooking |
      | SEARCH_BOOKINGS | null         | 0          | 0             |
      | SEARCH_BOOKINGS | error        | 0          | 0             |
      | GET_BOOKING     | null         | 1          | 0             |
      | GET_BOOKING     | error        | 1          | 0             |
      | UPDATE_BOOKING  | null         | 1          | 1             |
      | UPDATE_BOOKING  | error        | 1          | 1             |


  Scenario Outline: No membership feeContainer found in primeSubscriptionBooking
    Given the following membership exists and has a prime subscription booking:
      | name              | Pablo                   |
      | lastNames         | Gomez                   |
      | membershipId      | 20222                   |
      | memberAccountId   | 2022                    |
      | autoRenewalStatus | ENABLED                 |
      | membershipStatus  | ACTIVATED               |
      | activationDate    | 2019-10-13T13:20:00.000 |
      | expirationDate    | TODAY                   |
      | balance           | 22.22                   |
      | website           | OPFR                    |
      | sourceType        | POST_BOOKING            |
      | membershipType    | <membershipType>        |

    And the following membership renewal offers exist:
      | website | price |
      | OPFR    | 54.99 |

    When the MembershipExpiryRobot is executed

    Then the MembershipExpiryRobot status is STOPPED
    And the response has status code 200

    And the SEARCH_MEMBERSHIPS endpoint of MEMBERSHIP_SEARCH_API service was called 6 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called 1 times
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with website value "OPFR"
    And the GET_OFFER endpoint of MEMBERSHIP_OFFER service was called with membershipId value "20222"
    And the CREATE_PENDING_TO_COLLECT endpoint of MEMBERSHIP service was called with memberAccountId value "2022"
    And the EXPIRE_MEMBERSHIP endpoint of MEMBERSHIP service was called 1 times
    And the SEARCH_BOOKINGS endpoint of BOOKING_SEARCH_API service was called 1 times
    And the GET_BOOKING endpoint of BOOKING_API service was called 1 times
    And the UPDATE_BOOKING endpoint of BOOKING_API service was never called
    And the CONSUME_MEMBERSHIP_REMNANT_BALANCE endpoint of MEMBERSHIP service was never called

    Examples:
      | membershipType           |
      | EMPTY_STRING             |
      | SOME_NON_MEMBERSHIP_TYPE |
