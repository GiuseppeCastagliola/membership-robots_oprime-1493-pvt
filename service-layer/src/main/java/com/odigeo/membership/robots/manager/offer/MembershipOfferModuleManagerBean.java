package com.odigeo.membership.robots.manager.offer;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.offer.api.MembershipOfferService;
import com.odigeo.membership.offer.api.exception.InvalidParametersException;
import com.odigeo.membership.offer.api.exception.MembershipOfferServiceException;
import com.odigeo.membership.offer.api.request.MembershipRenewalOfferRequest;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.robots.apicall.ApiCall.Endpoint;
import com.odigeo.membership.robots.apicall.ApiCall.Result;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.mapper.request.MembershipOfferRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipOfferResponseMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

@Singleton
public class MembershipOfferModuleManagerBean implements MembershipOfferModuleManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipOfferModuleManagerBean.class);
    private static final String INVALID_MEMBERSHIP_OFFER = "Invalid MembershipSubscriptionOffer, null value returned. membershipSubscriptionOffer:%s, currencyCode:%s, price:%s.";
    private static final Predicate<MembershipSubscriptionOffer> NULL_MEMBERSHIP_OFFER = Objects::isNull;
    private static final Predicate<MembershipSubscriptionOffer> NULL_CURRENCY_CODE = membershipOffer -> Objects.isNull(membershipOffer.getCurrencyCode());
    private static final Predicate<MembershipSubscriptionOffer> NULL_PRICE = membershipOffer -> Objects.isNull(membershipOffer.getPrice());
    private static final Predicate<MembershipSubscriptionOffer> FAILED_MEMBERSHIP_OFFER = NULL_MEMBERSHIP_OFFER.or(NULL_CURRENCY_CODE).or(NULL_PRICE);
    private final MembershipOfferService membershipOfferService;
    private final MembershipOfferRequestMapper membershipOfferRequestMapper;
    private final MembershipOfferResponseMapper membershipOfferResponseMapper;

    @Inject
    public MembershipOfferModuleManagerBean(MembershipOfferService membershipOfferService, MembershipOfferRequestMapper membershipOfferRequestMapper, MembershipOfferResponseMapper membershipOfferResponseMapper) {
        this.membershipOfferService = membershipOfferService;
        this.membershipOfferRequestMapper = membershipOfferRequestMapper;
        this.membershipOfferResponseMapper = membershipOfferResponseMapper;
    }

    @Override
    public ApiCallWrapper<MembershipOfferDTO, MembershipDTO> getOffer(MembershipDTO membershipDTO) {
        ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO> getOfferWrapperBuilder = new ApiCallWrapperBuilder<>(Endpoint.GET_OFFER);
        getOfferWrapperBuilder.request(membershipDTO);
        MembershipRenewalOfferRequest membershipRenewalOfferRequest = membershipOfferRequestMapper.dtoToMembershipRenewalOfferRequest(membershipDTO);
        final MembershipSubscriptionOffer membershipSubscriptionOffer;
        try {
            membershipSubscriptionOffer = membershipOfferService.createMembershipRenewalOffer(membershipRenewalOfferRequest);
            validateMembershipOfferResponse(getOfferWrapperBuilder, membershipSubscriptionOffer);
        } catch (InvalidParametersException | MembershipOfferServiceException | UndeclaredThrowableException e) {
            LOGGER.error(e.getMessage(), e);
            getOfferWrapperBuilder.result(Result.FAIL).throwable(e).message(e.getMessage());
        }
        final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> getOfferWrapper = getOfferWrapperBuilder.build();
        getOfferWrapper.logResultAndMessageWithPrefix(LOGGER, getOfferWrapper.getEndpoint() + " for website : " + membershipDTO.getWebsite());
        return getOfferWrapper;
    }

    private void validateMembershipOfferResponse(ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO> getOfferWrapperBuilder, MembershipSubscriptionOffer membershipSubscriptionOffer) {
        getOfferWrapperBuilder.result(Result.SUCCESS).response(membershipOfferResponseMapper.subscriptionOfferToMembershipOfferDto(membershipSubscriptionOffer));

        if (FAILED_MEMBERSHIP_OFFER.test(membershipSubscriptionOffer)) {
            getOfferWrapperBuilder.result(Result.FAIL).message(
                    String.format(INVALID_MEMBERSHIP_OFFER,
                            membershipSubscriptionOffer,
                            NULL_MEMBERSHIP_OFFER.test(membershipSubscriptionOffer) ? StringUtils.EMPTY : membershipSubscriptionOffer.getCurrencyCode(),
                            NULL_MEMBERSHIP_OFFER.test(membershipSubscriptionOffer) ? StringUtils.EMPTY : Optional.ofNullable(membershipSubscriptionOffer.getPrice()).map(BigDecimal::toPlainString).orElse(StringUtils.EMPTY)));
        }
    }
}
