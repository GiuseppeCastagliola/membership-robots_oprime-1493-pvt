package com.odigeo.membership.robots.manager.membership;

import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

public interface MembershipModuleManagerTracking {
    ApiCallWrapper<BookingTrackingDTO, BookingDTO> getBookingTracking(BookingDTO bookingDTO);

    ApiCallWrapper<BookingTrackingDTO, BookingTrackingDTO> trackBookingAndUpdateBalance(BookingTrackingDTO bookingTrackingDTO);

    ApiCallWrapper<Void, BookingDTO> deleteBookingTrackingAndUpdateBalance(BookingDTO bookingDTO);
}
