package com.odigeo.membership.robots.manager.membership;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.product.ActivationRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.ExpirationConfiguration;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipDTOBuilder;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.mapper.request.MembershipRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import io.vavr.control.Try;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.UndeclaredThrowableException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;

import static com.odigeo.membership.robots.apicall.ApiCall.Endpoint;
import static com.odigeo.membership.robots.apicall.ApiCall.Result;

@Singleton
public class MembershipModuleManagerBean implements MembershipModuleManager {
    private static final String EXPIRED_MSG = " expired correctly";
    private static final String DISCARDED_MSG = " discarded correctly";
    private static final String DEACTIVATED_MSG = " deactivated correctly";
    private static final String CONSUMED_MSG = " balance consumed correctly";
    private static final String RECURRING_MSG = " recurring id saved correctly";
    private static final String ENABLED_AUTO_RENEWAL_MSG = " autoRenewal enabled correctly";
    private static final String ALREADY_EXPIRED_MSG = " was already expired correctly";
    private static final String ALREADY_DISCARDED_MSG = " was already discarded correctly";
    private static final String ALREADY_DEACTIVATED_MSG = " was already deactivated correctly";
    private static final String ALREADY_AUTORENEWAL_ON_MSG = " autorenewal was already enabled correctly";
    private static final String EXPIRED_STATUS = "EXPIRED";
    private static final String DISCARDED_STATUS = "DISCARDED";
    private static final String DEACTIVATED_STATUS = "DEACTIVATED";
    private static final String NO_EXPIRATION_DATE_MSG = "Expiration date should not be null. previousMembership id:%s, previousMembership expirationDate:%s, membershipOffer duration:%s";
    private static final String UPDATE_MEMBERSHIP_FAILED_MSG = "Update membership failed when %s was requested for membershipId %s";
    private static final String ACTIVATION_FAILED_MSG = "Activate membership %s with balance %s failed";
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipModuleManagerBean.class);
    private static final String COLON = " : ";
    private static final int MONTHLY_MARKET_EXPIRATION_MONTHS = 3;

    private final MembershipService membershipService;
    private final MembershipRequestMapper membershipRequestMapper;
    private final MembershipResponseMapper membershipResponseMapper;
    private final ImmutableMap<String, Function<MembershipDTO, MembershipDTO>> updateResponseFunctionMap;
    private final ExpirationConfiguration expirationConfiguration;

    @Inject
    public MembershipModuleManagerBean(MembershipService membershipService, MembershipRequestMapper membershipRequestMapper, MembershipResponseMapper membershipResponseMapper, ExpirationConfiguration expirationConfiguration) {
        this.membershipService = membershipService;
        this.membershipRequestMapper = membershipRequestMapper;
        this.membershipResponseMapper = membershipResponseMapper;
        this.updateResponseFunctionMap = ImmutableMap.<String, Function<MembershipDTO, MembershipDTO>>builder()
                .put("EXPIRE_MEMBERSHIP", membershipResponseMapper::dtoToExpiredDto)
                .put("DISCARD_MEMBERSHIP", membershipResponseMapper::dtoToDiscardedDto)
                .put("DEACTIVATE_MEMBERSHIP", membershipResponseMapper::dtoToDeactivatedDto)
                .put("CONSUME_MEMBERSHIP_REMNANT_BALANCE", membershipResponseMapper::dtoToConsumedBalanceDto)
                .put("ENABLE_AUTO_RENEWAL", membershipResponseMapper::dtoToEnabledAutoRenewalDto)
                .put("INSERT_RECURRING_ID", Function.identity())
                .build();
        this.expirationConfiguration = expirationConfiguration;
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembership(MembershipDTO membershipToExpire) {
        if (EXPIRED_STATUS.equalsIgnoreCase(membershipToExpire.getStatus())) {
            return alreadyUpdatedMembershipWrapper(membershipToExpire, Endpoint.EXPIRE_MEMBERSHIP, ALREADY_EXPIRED_MSG);
        }
        final UpdateMembershipRequest expireMembershipRequest = membershipRequestMapper.dtoToExpireRequest(membershipToExpire);
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> expireWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.EXPIRE_MEMBERSHIP)
                .request(membershipToExpire);
        return updateMembership(membershipToExpire, expireMembershipRequest, expireWrapperBuilder, EXPIRED_MSG);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> discardMembership(MembershipDTO membershipToDiscard) {
        if (DISCARDED_STATUS.equalsIgnoreCase(membershipToDiscard.getStatus())) {
            return alreadyUpdatedMembershipWrapper(membershipToDiscard, Endpoint.DISCARD_MEMBERSHIP, ALREADY_DISCARDED_MSG);
        }
        final UpdateMembershipRequest discardMembershipRequest = membershipRequestMapper.dtoToDiscardRequest(membershipToDiscard);
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> cancelWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.DISCARD_MEMBERSHIP)
                .request(membershipToDiscard);
        return updateMembership(membershipToDiscard, discardMembershipRequest, cancelWrapperBuilder, DISCARDED_MSG);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> deactivateMembership(MembershipDTO membershipToDeactivate) {
        if (DEACTIVATED_STATUS.equalsIgnoreCase(membershipToDeactivate.getStatus())) {
            return alreadyUpdatedMembershipWrapper(membershipToDeactivate, Endpoint.DEACTIVATE_MEMBERSHIP, ALREADY_DEACTIVATED_MSG);
        }
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> cancelWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.DEACTIVATE_MEMBERSHIP)
                .request(membershipToDeactivate);
        final UpdateMembershipRequest deactivateMembershipRequest = membershipRequestMapper.dtoToDeactivateRequest(membershipToDeactivate);
        return updateMembership(membershipToDeactivate, deactivateMembershipRequest, cancelWrapperBuilder, DEACTIVATED_MSG);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> enableAutoRenewal(MembershipDTO membershipDTO) {
        if ("ENABLED".equals(membershipDTO.getAutoRenewal())) {
            return alreadyUpdatedMembershipWrapper(membershipDTO, Endpoint.ENABLE_AUTO_RENEWAL, ALREADY_AUTORENEWAL_ON_MSG);
        }
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> enableAutoRenewalWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.ENABLE_AUTO_RENEWAL)
                .request(membershipDTO);
        final UpdateMembershipRequest enableAutoRenewalRequest = membershipRequestMapper.dtoToEnableAutoRenewalRequest(membershipDTO);
        return updateMembership(membershipDTO, enableAutoRenewalRequest, enableAutoRenewalWrapper, ENABLED_AUTO_RENEWAL_MSG);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> saveRecurringId(MembershipDTO membershipWithRecurring) {
        final UpdateMembershipRequest insertRecurringRequest = membershipRequestMapper.dtoToRecurringRequest(membershipWithRecurring);
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> insertRecurringWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.SAVE_RECURRING_ID)
                .request(membershipWithRecurring);
        return updateMembership(membershipWithRecurring, insertRecurringRequest, insertRecurringWrapper, RECURRING_MSG);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> createPendingToCollect(ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper) {
        MembershipDTO previousMembership = subscriptionOfferWrapper.getRequest();
        ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> apiCallWrapperBuilder;
        if (subscriptionOfferWrapper.isSuccessful()) {
            MembershipOfferDTO membershipOfferDTO = subscriptionOfferWrapper.getResponse();
            apiCallWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.CREATE_PENDING_TO_COLLECT, Result.FAIL)
                    .request(previousMembership);
            Optional<String> newExpirationDate = ptcExpirationDateByWebsite(previousMembership, membershipOfferDTO);
            if (newExpirationDate.isPresent()) {
                final CreatePendingToCollectRequest pendingToCollectRequest = membershipRequestMapper
                        .dtoToCreatePendingToCollectRequestBuilder(previousMembership, membershipOfferDTO, newExpirationDate.get())
                        .build();
                wrapPendingToCollectCreation(previousMembership, apiCallWrapperBuilder, pendingToCollectRequest);
            } else {
                apiCallWrapperBuilder.response(previousMembership)
                        .message(String.format(NO_EXPIRATION_DATE_MSG, previousMembership.getId(), previousMembership.getExpirationDate(), membershipOfferDTO.getDuration()));
            }
        } else {
            apiCallWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.GET_OFFER)
                    .result(Result.FAIL)
                    .request(previousMembership)
                    .throwable(subscriptionOfferWrapper.getThrowable())
                    .message(subscriptionOfferWrapper.getMessage());
        }
        return logResult(previousMembership, apiCallWrapperBuilder.build());
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantMembershipBalance(MembershipDTO membershipToConsume) {
        final UpdateMembershipRequest consumeBalanceRequest = membershipRequestMapper.dtoToResetBalanceRequest(membershipToConsume);
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> consumeBalanceWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE)
                .request(membershipToConsume);
        return updateMembership(membershipToConsume, consumeBalanceRequest, consumeBalanceWrapper, CONSUMED_MSG);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> activateMembership(MembershipDTO membershipToActivate, BookingDTO bookingDTO) {
        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setBalance(membershipToActivate.getBalance());
        activationRequest.setBookingId(bookingDTO.getId());
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> activateWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.ACTIVATE_MEMBERSHIP).request(membershipToActivate);
        Try.of(() -> membershipService.activateMembership(membershipToActivate.getId(), activationRequest))
                .onFailure(throwable -> activateWrapperBuilder.throwable(throwable).result(Result.FAIL)
                        .request(membershipToActivate)
                        .response(membershipToActivate)
                        .message(String.format(ACTIVATION_FAILED_MSG, membershipToActivate.getId(), membershipToActivate.getBalance())))
                .onSuccess(membershipResponse -> activateWrapperBuilder.response(membershipResponseMapper.membershipResponseToDto(membershipResponse))
                        .message(membershipResponse.getId() + " activated!")
                        .result(Result.SUCCESS));
        return logResult(membershipToActivate, activateWrapperBuilder.build());
    }

    private ApiCallWrapper<MembershipDTO, MembershipDTO> updateMembership(MembershipDTO membershipToUpdate, UpdateMembershipRequest updateMembershipRequest, ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> updateWrapper, String successMsg) {
        Try.of(() -> membershipService.updateMembership(updateMembershipRequest))
                .onFailure(throwable -> wrapUpdateException(membershipToUpdate, updateMembershipRequest, updateWrapper, throwable))
                .onSuccess(isUpdated -> wrapUpdateCall(membershipToUpdate, updateMembershipRequest, updateWrapper, successMsg, isUpdated));
        final ApiCallWrapper<MembershipDTO, MembershipDTO> result = updateWrapper.build();
        result.logResultAndMessageWithPrefix(LOGGER, result.getEndpoint() + StringUtils.SPACE + membershipToUpdate.getId() + COLON);
        return result;
    }

    private ApiCallWrapper<MembershipDTO, MembershipDTO> alreadyUpdatedMembershipWrapper(MembershipDTO membershipToUpdate, Endpoint endpoint, String message) {
        ApiCallWrapper<MembershipDTO, MembershipDTO> alreadyUpdatedWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(endpoint)
                .result(Result.SUCCESS)
                .response(membershipToUpdate)
                .message(membershipToUpdate.getId() + message)
                .request(membershipToUpdate).build();
        alreadyUpdatedWrapper.logResultAndMessageWithPrefix(LOGGER, alreadyUpdatedWrapper.getEndpoint() + StringUtils.SPACE + membershipToUpdate.getId() + COLON);
        return alreadyUpdatedWrapper;
    }

    private Optional<String> ptcExpirationDateByWebsite(MembershipDTO previousMembership, MembershipOfferDTO membershipOfferDTO) {
        return Optional.ofNullable(previousMembership.getExpirationDate())
                .map(expirationDate -> expirationDate
                        .plusMonths(expirationConfiguration.isMonthlyWebsite(previousMembership.getWebsite()) ? MONTHLY_MARKET_EXPIRATION_MONTHS : membershipOfferDTO.getDuration()))
                .map(DateTimeFormatter.ISO_DATE_TIME::format);
    }

    private void wrapUpdateException(MembershipDTO membershipToUpdate, UpdateMembershipRequest updateMembershipRequest, ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> updateWrapper, Throwable throwable) {
        String exceptionMessage = String.format(UPDATE_MEMBERSHIP_FAILED_MSG, updateMembershipRequest.getOperation(), updateMembershipRequest.getMembershipId());
        LOGGER.error(exceptionMessage, throwable);
        updateWrapper.result(Result.FAIL).response(membershipToUpdate).throwable(throwable).message(exceptionMessage);
    }

    private void wrapUpdateCall(MembershipDTO membershipToUpdate, UpdateMembershipRequest updateMembershipRequest, ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> updateWrapper, String successMsg, Boolean isUpdated) {
        String updateOperation = updateMembershipRequest.getOperation();
        updateWrapper.result(isUpdated ? Result.SUCCESS : Result.FAIL);
        if (isUpdated) {
            updateWrapper.response(updateResponseFunctionMap.getOrDefault(updateOperation, Function.identity()).apply(membershipToUpdate))
                    .message(membershipToUpdate.getId() + successMsg);
        } else {
            updateWrapper.response(membershipToUpdate)
                    .message(String.format(UPDATE_MEMBERSHIP_FAILED_MSG, updateOperation, updateMembershipRequest.getMembershipId()));
        }
    }

    private void wrapPendingToCollectCreation(MembershipDTO previousMembership, ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> createPendingToCollectWrapperBuilder, CreatePendingToCollectRequest pendingToCollectRequest) {
        try {
            final Long membershipId = membershipService.createMembership(pendingToCollectRequest);
            final MembershipDTO enrichedMembershipResponse = fillResponseMembershipDTO(previousMembership, pendingToCollectRequest, membershipId);
            createPendingToCollectWrapperBuilder.result(Result.SUCCESS).response(enrichedMembershipResponse).message("Membership PENDING_TO_COLLECT " + membershipId + " created");
        } catch (MembershipServiceException | UndeclaredThrowableException e) {
            LOGGER.error(e.getMessage(), e);
            createPendingToCollectWrapperBuilder.response(previousMembership).throwable(e).message(e.getMessage());
        }
    }

    private MembershipDTO fillResponseMembershipDTO(MembershipDTO previousMembership, CreatePendingToCollectRequest pendingToCollectRequest, Long membershipId) {
        return MembershipDTOBuilder.builderFromDto(previousMembership)
                .expirationDate(LocalDateTime.parse(pendingToCollectRequest.getExpirationDate()))
                .totalPrice(pendingToCollectRequest.getSubscriptionPrice())
                .currencyCode(pendingToCollectRequest.getCurrencyCode())
                .monthsDuration(pendingToCollectRequest.getMonthsToRenewal())
                .id(membershipId).build();
    }

    private ApiCallWrapper<MembershipDTO, MembershipDTO> logResult(MembershipDTO membershipDto, ApiCallWrapper<MembershipDTO, MembershipDTO> apiCallWrapper) {
        apiCallWrapper.logResultAndMessageWithPrefix(LOGGER, new StringJoiner(COLON).add(apiCallWrapper.getEndpoint().name())
                .add(" previous membership ")
                .add(String.valueOf(membershipDto.getId()))
                .add(StringUtils.SPACE)
                .toString());
        return apiCallWrapper;
    }
}
