package com.odigeo.membership.robots.consumer;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.robots.consumer.updater.Updater;
import com.odigeo.membership.robots.criteria.MembershipPredicateProvider;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MembershipProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipProcessor.class);
    private final MembershipPredicateProvider membershipPredicateProvider;
    private final BookingDTO bookingDTO;
    private MembershipDTO membershipDTO;
    private final Updater updater;
    private final ExternalModuleManager externalModuleManager;
    private final List<ConditionalConsumer<MembershipDTO, MembershipDTO>> conditionalConsumers;
    private final MembershipBookingTrackingProcessorFactory membershipBookingTrackingProcessorFactory;
    private final List<ConditionalConsumer<MembershipBookingTrackingProcessor, MembershipDTO>> conditionalConsumersTracking;

    @Inject
    protected MembershipProcessor(@Assisted BookingDTO bookingDTO, Updater updater, MembershipPredicateProvider membershipPredicateProvider,
                                  ExternalModuleManager externalModuleManager, MembershipBookingTrackingProcessorFactory membershipBookingTrackingProcessorFactory) {
        this.bookingDTO = bookingDTO;
        this.updater = updater;
        this.membershipPredicateProvider = membershipPredicateProvider;
        this.externalModuleManager = externalModuleManager;
        this.membershipBookingTrackingProcessorFactory = membershipBookingTrackingProcessorFactory;
        conditionalConsumers = new ArrayList<>();
        conditionalConsumersTracking = new ArrayList<>();
    }

    @Override
    public void process(long membershipId) {
        conditionalConsumers.forEach(consumer -> consumer.accept(getMembershipDto(), getMembershipDto()));
        if (CollectionUtils.isNotEmpty(conditionalConsumersTracking)) {
            MembershipBookingTrackingProcessor membershipBookingTrackingProcessor = membershipBookingTrackingProcessorFactory.create(bookingDTO, getMembershipDto());
            conditionalConsumersTracking.forEach(consumer -> consumer.accept(membershipBookingTrackingProcessor, getMembershipDto()));
            membershipBookingTrackingProcessor.process(bookingDTO.getId());
        }
    }

    void addRecurringSavingCandidate() {
        conditionalConsumers.add(ConditionalConsumer.ofMembership(membershipDto -> setMembershipDTO(updater.saveRecurringId(membershipDto, bookingDTO)),
                membershipPredicateProvider.hasRecurringId().negate()));
    }

    void addActivationCandidate() {
        conditionalConsumers.add(ConditionalConsumer.ofMembership(membershipDto -> setMembershipDTO(updater.activateMembership(membershipDto, bookingDTO)),
                membershipPredicateProvider.isPendingToActivate()));
    }

    void addTrackingCandidate() {
        conditionalConsumersTracking.add(ConditionalConsumer.ofMembershipBookingTracking(MembershipBookingTrackingProcessor::addTrackingCandidate, membershipPredicateProvider.isActivated()));
    }

    void addRebalanceNotContractBookingTrackingCandidate() {
        conditionalConsumersTracking.add(ConditionalConsumer.ofMembershipBookingTracking(MembershipBookingTrackingProcessor::addRebalanceNotContractBookingTrackingCandidate, membershipPredicateProvider.isActivated()));
    }

    void addBasicFreeUpdateCandidate() {
        conditionalConsumers.add(ConditionalConsumer.ofMembership(this::tryBookingUpdateForBasicFree, membershipPredicateProvider.isBasicFreeToProcess()));
    }

    private void tryBookingUpdateForBasicFree(MembershipDTO membershipDTO) {
        setMembershipDTO(updater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO));
    }

    private MembershipDTO retrieveMembershipDTOOrTriggerRetry() {
        setMembershipDTO(supplyDtoOrThrowException(() -> externalModuleManager.getMembership(bookingDTO.getMembershipId())));
        LOGGER.info("Membership {} retrieved after bookingId {} processing", bookingDTO.getMembershipId(), bookingDTO.getId());
        return membershipDTO;
    }

    private MembershipDTO getMembershipDto() {
        return Optional.ofNullable(membershipDTO).orElseGet(this::retrieveMembershipDTOOrTriggerRetry);
    }

    private void setMembershipDTO(MembershipDTO membershipDTO) {
        this.membershipDTO = membershipDTO;
    }

}
