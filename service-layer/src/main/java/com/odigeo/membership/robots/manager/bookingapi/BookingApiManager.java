package com.odigeo.membership.robots.manager.bookingapi;

import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

import java.util.List;

public interface BookingApiManager {
    ApiCallWrapper<BookingDTO, MembershipDTO> getPrimeSubscriptionBookingForMembership(MembershipDTO membershipDTO);

    ApiCallWrapper<BookingDTO, BookingDTO> getBooking(ApiCallWrapperBuilder<BookingDTO, BookingDTO> primeSubscriptionBookingDetailWrapperBuilder);

    ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookings(ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> bookingSummariesWrapperBuilder);

    ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBooking(long bookingId, ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> updateBookingWrapperBuilder);
}
