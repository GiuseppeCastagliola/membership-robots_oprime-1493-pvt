package com.odigeo.membership.robots.criteria;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import static java.util.Objects.nonNull;

@Singleton
public class MembershipTrackingBiPredicateProvider {

    @Inject
    private BookingTrackingPredicateProvider bookingTrackingPredicateProvider;
    @Inject
    private MembershipPredicateProvider membershipPredicateProvider;

    private BiPredicate<MembershipDTO, BookingTrackingDTO> loggedPredicate(String conditionText, BiPredicate<MembershipDTO, BookingTrackingDTO> membershipBookingTrackingBiPredicate) {
        BiFunction<MembershipDTO, BookingTrackingDTO, String> labelFunction = (membershipDTO, bookingTrackingDTO) -> {
            String trackingCheck = nonNull(bookingTrackingDTO) ? " Booking tracking : " + bookingTrackingDTO.getBookingId() : " Booking tracking null ";
            return "Membership " + membershipDTO.getId() + StringUtils.SPACE + trackingCheck + conditionText;
        };
        return LoggingPredicateProvider.loggingPredicate(labelFunction, membershipBookingTrackingBiPredicate);
    }

    public BiPredicate<MembershipDTO, BookingTrackingDTO> isActivatedAndBookingFeesAreUnbalancedAndNotTracked(BookingDTO bookingDTO) {
        return loggedPredicate("isActivatedAndBookingFeesAreUnbalancedAndNotTracked", (membershipDto, bookingTrackingDTO) -> Objects.isNull(bookingTrackingDTO)
                && membershipPredicateProvider.isActivated().test(membershipDto)
                && (membershipPredicateProvider.areFeesUnbalanced(bookingDTO).test(membershipDto)));
    }

    public BiPredicate<MembershipDTO, BookingTrackingDTO> isActivatedAndBookingFeesAreBalancedAndNotTracked(BookingDTO bookingDTO) {
        return loggedPredicate("isActivatedAndBookingFeesAreBalancedAndNotTracked", (membershipDto, bookingTrackingDTO) -> Objects.isNull(bookingTrackingDTO)
                && membershipPredicateProvider.isActivated().test(membershipDto) && membershipPredicateProvider.areFeesUnbalanced(bookingDTO).negate().test(membershipDto));
    }

    public BiPredicate<MembershipDTO, BookingTrackingDTO> isActivatedAndBookingIsTracked() {
        return loggedPredicate("isActivatedAndBookingIsTracked", (membershipDTO, bookingTrackingDTO) -> membershipPredicateProvider.isActivated().test(membershipDTO)
                && nonNull(bookingTrackingDTO));
    }

    public BiPredicate<MembershipDTO, BookingTrackingDTO> isActivatedAndBookingFeesAreTrackedAndChanged(BookingDTO bookingDTO) {
        return loggedPredicate("isActivatedAndBookingFeesAreTrackedAndChanged", (membershipDTO, bookingTrackingDTO) -> membershipPredicateProvider.isActivated().test(membershipDTO)
                && bookingTrackingPredicateProvider.isAlreadyTrackedAndFeesAreChanged(bookingDTO).test(bookingTrackingDTO));
    }

}
