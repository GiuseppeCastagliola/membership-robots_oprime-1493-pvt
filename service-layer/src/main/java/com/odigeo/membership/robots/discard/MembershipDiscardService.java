package com.odigeo.membership.robots.discard;

import java.util.List;

public interface MembershipDiscardService {

    void discardMemberships(List<Long> membershipIds);

}
