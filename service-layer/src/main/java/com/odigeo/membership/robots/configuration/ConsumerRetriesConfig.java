package com.odigeo.membership.robots.configuration;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.common.annotations.VisibleForTesting;

@ConfiguredInPropertiesFile
public class ConsumerRetriesConfig {
    private long delay;
    private long maxDelay;
    private long jitter;
    private int maxRetries;
    private long notFoundDelay;
    private int notFoundMaxRetries;
    private long notFoundJitter;
    private String chronoUnit;

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public long getMaxDelay() {
        return maxDelay;
    }

    public void setMaxDelay(long maxDelay) {
        this.maxDelay = maxDelay;
    }

    public long getJitter() {
        return jitter;
    }

    public void setJitter(long jitter) {
        this.jitter = jitter;
    }

    public int getMaxRetries() {
        return maxRetries;
    }

    public void setMaxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
    }

    public long getNotFoundDelay() {
        return notFoundDelay;
    }

    public void setNotFoundDelay(long notFoundDelay) {
        this.notFoundDelay = notFoundDelay;
    }

    public int getNotFoundMaxRetries() {
        return notFoundMaxRetries;
    }

    public void setNotFoundMaxRetries(int notFoundMaxRetries) {
        this.notFoundMaxRetries = notFoundMaxRetries;
    }

    public long getNotFoundJitter() {
        return notFoundJitter;
    }

    public void setNotFoundJitter(long notFoundJitter) {
        this.notFoundJitter = notFoundJitter;
    }

    @VisibleForTesting
    public ConsumerRetriesConfig defaultValues() {
        setDelay(1);
        setMaxDelay(2);
        setJitter(1);
        setMaxRetries(1);
        setNotFoundDelay(1);
        setNotFoundJitter(1);
        setNotFoundMaxRetries(1);
        setChronoUnit("MINUTES");
        return this;
    }

    public String getChronoUnit() {
        return chronoUnit;
    }

    public void setChronoUnit(String chronoUnit) {
        this.chronoUnit = chronoUnit;
    }
}
