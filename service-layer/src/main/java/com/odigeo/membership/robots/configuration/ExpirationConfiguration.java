package com.odigeo.membership.robots.configuration;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

import java.util.Arrays;
import java.util.List;

@Singleton
@ConfiguredInPropertiesFile
public class ExpirationConfiguration {
    private long daysInThePast;
    private String startingFrom;
    private String[] monthlyWebsites;

    public long getDaysInThePast() {
        return daysInThePast;
    }

    public void setDaysInThePast(long daysInThePast) {
        this.daysInThePast = daysInThePast;
    }

    public String getStartingFrom() {
        return startingFrom;
    }

    public void setStartingFrom(String startingFrom) {
        this.startingFrom = startingFrom;
    }

    public List<String> getMonthlyWebsitesList() {
        return Arrays.asList(monthlyWebsites);
    }

    public void setMonthlyWebsites(String... monthlyWebsites) {
        this.monthlyWebsites = monthlyWebsites.clone();
    }

    public boolean isMonthlyWebsite(String website) {
        return getMonthlyWebsitesList().contains(website);
    }
}
