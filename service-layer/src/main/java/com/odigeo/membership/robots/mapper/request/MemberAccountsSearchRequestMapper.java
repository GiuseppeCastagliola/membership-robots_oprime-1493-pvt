package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.robots.dto.SearchMemberAccountsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface MemberAccountsSearchRequestMapper {
    @Mapping(target = "withMemberships", ignore = true)
    MemberAccountSearchRequest.Builder searchAccountsDtoToSearchAccountsRequestBuilder(SearchMemberAccountsDTO searchMemberAccountsDTO);
}
