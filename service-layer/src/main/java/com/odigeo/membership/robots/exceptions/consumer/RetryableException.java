package com.odigeo.membership.robots.exceptions.consumer;

public class RetryableException extends RuntimeException {

    public RetryableException(Throwable e) {
        super(e);
    }

    public RetryableException(String s) {
        super(s);
    }
}
