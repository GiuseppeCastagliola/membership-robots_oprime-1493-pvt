package com.odigeo.membership.robots.consumer.updater;

import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

public interface Updater {

    MembershipDTO saveRecurringId(MembershipDTO membershipDTO, BookingDTO bookingDTO);

    MembershipDTO activateMembership(MembershipDTO membershipDTO, BookingDTO bookingDTO);

    BookingTrackingDTO trackBookingAndUpdateBalance(BookingDTO bookingDTO);

    void deleteBookingTracking(BookingDTO bookingDTO);

    void balanceBooking(BookingDTO bookingDTO, MembershipDTO membershipDTO);

    MembershipDTO updateBookingBasicFreeFirstBooking(BookingDTO bookingDTO, MembershipDTO membershipDTO);
}
