package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import static java.util.Objects.nonNull;

@Mapper
public interface MembershipSearchRequestMapper {

    @Mapping(target = "renewalPrice", ignore = true)
    @Mapping(target = "memberAccountId", ignore = true)
    @Mapping(target = "memberAccountSearchRequest", expression = "java(buildMemberAccountSearch(searchDTO))")
    @Mapping(target = "toCreationDate", ignore = true)
    @Mapping(target = "toActivationDate", ignore = true)
    @Mapping(target = "productStatus", ignore = true)
    @Mapping(target = "monthsDuration", ignore = true)
    @Mapping(target = "totalPrice", ignore = true)
    @Mapping(target = "sourceType", ignore = true)
    @Mapping(target = "membershipType", ignore = true)
    @Mapping(target = "maxBalance", ignore = true)
    @Mapping(target = "withStatusActions", ignore = true)
    @Mapping(target = "fromCreationDate", ignore = true)
    @Mapping(target = "currencyCode", ignore = true)
    MembershipSearchRequest.Builder searchDtoToSearchRequestBuilder(SearchMembershipsDTO searchDTO);

    default MemberAccountSearchRequest buildMemberAccountSearch(SearchMembershipsDTO searchMembershipsDTO) {
        MemberAccountSearchRequest memberAccountSearchRequest = null;
        if (nonNull(searchMembershipsDTO.getSearchMemberAccountsDTO())) {
            memberAccountSearchRequest = Mappers.getMapper(MemberAccountsSearchRequestMapper.class)
                    .searchAccountsDtoToSearchAccountsRequestBuilder(searchMembershipsDTO.getSearchMemberAccountsDTO()).build();
        }
        return memberAccountSearchRequest;
    }
}
