package com.odigeo.membership.robots.fees;

import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

public interface RemnantFeeService {

    ApiCallWrapper<BookingDTO, MembershipDTO> processPrimeSubscriptionBookingFees(ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper);

    ApiCallWrapper<MembershipDTO, MembershipDTO> consumeMembershipRemnantBalance(ApiCallWrapper<BookingDTO, MembershipDTO> processPrimeSubscriptionBookingFeesWrapper);
}
