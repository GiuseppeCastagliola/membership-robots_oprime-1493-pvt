package com.odigeo.membership.robots.mapper.response.consumer;

import com.odigeo.commons.messaging.BasicMessage;
import com.odigeo.membership.robots.consumer.CollectionSummaryChargebackNotifications;
import org.apache.commons.lang3.math.NumberUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static java.util.Objects.requireNonNull;

@Mapper(imports = {NumberUtils.class})
public interface DeactivateMembershipByChargebackMapper {

    @Mapping(target = "bookingId", expression = "java(obtainBookingId(basicMessage.getExtraParameters().get(\"bookingId\")))")
    @Mapping(target = "action", expression = "java(basicMessage.getExtraParameters().get(\"action\"))")
    @Mapping(target = "status", expression = "java(basicMessage.getExtraParameters().get(\"status\"))")
    @Mapping(target = "executionTimeInMillis", expression = "java(NumberUtils.createLong(basicMessage.getExtraParameters().get(\"executionTimeInMillis\")))")
    @Mapping(target = "storageTimeInMillis", expression = "java(NumberUtils.createLong(basicMessage.getExtraParameters().get(\"storageTimeInMillis\")))")
    CollectionSummaryChargebackNotifications basicMessageToCollectionSummaryChargebackNotifications(BasicMessage basicMessage);

    default Long obtainBookingId(String bookingId) {
        requireNonNull(bookingId, "bookingId");
        return NumberUtils.createLong(bookingId);
    }

}
