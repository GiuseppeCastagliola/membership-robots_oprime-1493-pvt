package com.odigeo.membership.robots.consumer;

import com.google.inject.Inject;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.manager.bookingapi.BookingApiManager;
import com.odigeo.membership.robots.manager.bookingapi.product.ProductMembershipPrimeBookingItemType;
import com.odigeo.membership.robots.manager.membership.MembershipModuleManager;
import com.odigeo.membership.robots.manager.membership.search.MembershipSearchModuleManager;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import io.vavr.control.Either;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.Objects.nonNull;

public class DeactivateMembershipByChargebackMessageConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeactivateMembershipByChargebackMessageConsumer.class);
    private static final String MESSAGE_BOOKING_NOT_PRIME_STANDALONE = "The booking with identifier {} is not associated with a prime standalone product";
    private static final int LENGTH_PRODUCT_STANDALONE = 1;

    protected static final String SUCCESS_UPDATE_MEMBERSHIP = "success";
    protected static final String FAIL_UPDATE_MEMBERSHIP = "fail";

    private final MembershipSearchModuleManager membershipSearchModuleManager;
    private final BookingApiManager bookingApiManager;
    private final MembershipModuleManager membershipModuleManager;
    private final MembershipRobotsMetrics membershipRobotsMetrics;

    private final Consumer<ApiCallWrapper<MembershipDTO, MembershipDTO>> monitorDeactivateMembership = responseDeactivateMembership -> monitorUpdateMembership(responseDeactivateMembership, MetricsName.TOTAL_MEMBERSHIP_DEACTIVATED_CHARGEBACK);
    private final Consumer<ApiCallWrapper<MembershipDTO, MembershipDTO>> consumeRemnantMembershipBalance = this::consumeRemnantMembershipBalance;

    @Inject
    public DeactivateMembershipByChargebackMessageConsumer(MembershipSearchModuleManager membershipSearchModuleManager,
                                                           BookingApiManager bookingApiManager,
                                                           MembershipModuleManager membershipModuleManager,
                                                           MembershipRobotsMetrics membershipRobotsMetrics) {
        this.membershipSearchModuleManager = membershipSearchModuleManager;
        this.bookingApiManager = bookingApiManager;
        this.membershipModuleManager = membershipModuleManager;
        this.membershipRobotsMetrics = membershipRobotsMetrics;
    }

    public void deactivateMembershipByChargeback(CollectionSummaryChargebackNotifications collectionSummaryChargebackNotification) {
        searchProductPrimeByBookingId(collectionSummaryChargebackNotification.getBookingId())
                .map(membershipModuleManager::deactivateMembership)
                .map(Stream::of)
                .orElseGet(Stream::empty)
                .peek(monitorDeactivateMembership.andThen(consumeRemnantMembershipBalance))
                .map(this::processResponseFromApiCallWrapper)
                .findFirst()
                .ifPresent(responseDeactivateMembership -> responseDeactivateMembership.orElseRun(retryableException -> { throw retryableException; }));
    }

    private Optional<MembershipDTO> searchProductPrimeByBookingId(Long bookingId) {
        return searchBookingById(bookingId)
                .filter(this::hasBookingProductPrimeStandalone)
                .map(bookingDTO -> membershipSearchModuleManager.getMembership(bookingDTO.getMembershipId()))
                .map(this::processResponseFromApiCallWrapper)
                .map(membership -> membership.getOrElseThrow(Function.identity()));
    }

    private Optional<BookingDTO> searchBookingById(Long bookingId) {
        BookingDTO bookingDTO = BookingDTO.builder()
                .id(bookingId)
                .build();
        final ApiCallWrapperBuilder<BookingDTO, BookingDTO> bookingDetailWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING)
                .request(bookingDTO);
        return Optional.ofNullable(bookingApiManager.getBooking(bookingDetailWrapperBuilder))
                .map(this::processResponseFromApiCallWrapper)
                .map(booking -> booking.getOrElseThrow(Function.identity()));
    }

    private <T, U> Either<RetryableException, T> processResponseFromApiCallWrapper(ApiCallWrapper<T, U> apiCallWrapper) {
        if (exceptionFromApiCall(apiCallWrapper).isPresent()) {
            return Either.left(new RetryableException(apiCallWrapper.getThrowable()));
        }
        return Either.right(apiCallWrapper.getResponse());
    }

    private Optional<ApiCall> exceptionFromApiCall(ApiCall apiCall) {
        return Optional.of(apiCall)
                .filter(api -> !api.isSuccessful())
                .filter(api -> nonNull(api.getThrowable()));
    }

    private boolean hasBookingProductPrimeStandalone(BookingDTO bookingDTO) {
        boolean result = isProductPrimeStandalone(bookingDTO.getProductTypes());
        if (result) {
            membershipRobotsMetrics.incrementCounter(MetricsName.TOTAL_PROCESSED_PRIME_STANDALONE_CHARGEBACK);
        } else {
            LOGGER.warn(MESSAGE_BOOKING_NOT_PRIME_STANDALONE, bookingDTO.getId());
        }
        return result;
    }

    private boolean isProductPrimeStandalone(List<String> productTypes) {
        return CollectionUtils.isNotEmpty(productTypes)
                && productTypes.stream().anyMatch(ProductMembershipPrimeBookingItemType::isProductMembershipPrime)
                && productTypes.size() == LENGTH_PRODUCT_STANDALONE;
    }

    private void consumeRemnantMembershipBalance(ApiCallWrapper<MembershipDTO, MembershipDTO> responseDeactivateMembership) {
        if (responseDeactivateMembership.isSuccessful()) {
            Optional.ofNullable(membershipModuleManager.consumeRemnantMembershipBalance(responseDeactivateMembership.getResponse()))
                    .ifPresent(responseConsumeRemnantBalance -> monitorUpdateMembership(responseConsumeRemnantBalance, MetricsName.TOTAL_CONSUME_REMNANT_MEMBERSHIP_BALANCE));
        }
    }

    private void monitorUpdateMembership(ApiCall responseUpdateMembership, MetricsName metricName) {
        if (responseUpdateMembership.isSuccessful()) {
            membershipRobotsMetrics.incrementCounter(metricName, MetricsName.TagName.STATUS, SUCCESS_UPDATE_MEMBERSHIP);
        } else {
            membershipRobotsMetrics.incrementCounter(metricName, MetricsName.TagName.STATUS, FAIL_UPDATE_MEMBERSHIP);
        }
    }

}
