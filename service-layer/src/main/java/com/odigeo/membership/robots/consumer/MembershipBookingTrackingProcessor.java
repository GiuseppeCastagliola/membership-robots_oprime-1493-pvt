package com.odigeo.membership.robots.consumer;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.robots.consumer.updater.Updater;
import com.odigeo.membership.robots.criteria.MembershipTrackingBiPredicateProvider;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

public class MembershipBookingTrackingProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipBookingTrackingProcessor.class);
    private final BookingDTO bookingDTO;
    private final Updater updater;
    private final MembershipTrackingBiPredicateProvider membershipTrackingBiPredicateProvider;
    private final ExternalModuleManager externalModuleManager;
    private final List<BiConditionalConsumer<BookingDTO, MembershipDTO, BookingTrackingDTO>> trackingConditionalConsumers;
    private MembershipDTO membershipDTO;

    @Inject
    protected MembershipBookingTrackingProcessor(@Assisted BookingDTO bookingDTO, @Assisted MembershipDTO membershipDTO, Updater updater,
                                                 MembershipTrackingBiPredicateProvider membershipTrackingBiPredicateProvider,
                                                 ExternalModuleManager externalModuleManager) {
        this.bookingDTO = bookingDTO;
        this.membershipDTO = membershipDTO;
        this.updater = updater;
        this.membershipTrackingBiPredicateProvider = membershipTrackingBiPredicateProvider;
        this.externalModuleManager = externalModuleManager;
        trackingConditionalConsumers = new ArrayList<>();
    }

    @Override
    public void process(long bookingId) {
        LOGGER.info("Processing tracking task for booking : {}", bookingId);
        trackingConditionalConsumers.forEach(tConsumer -> tConsumer.accept(bookingDTO, membershipDTO, retrieveBookingTrackingDTOOrTriggerRetry()));
    }

    void addTrackingCandidate() {
        trackingConditionalConsumers.add(
                BiConditionalConsumer.ofMembershipBookingTracking(
                        this::deleteBookingTrackingAndRefreshMembership,
                        membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreTrackedAndChanged(bookingDTO)
                ));
        BiConditionalConsumer<BookingDTO, MembershipDTO, BookingTrackingDTO> saveBookingTrackingOrBalanceBooking = BiConditionalConsumer.ofMembershipBookingTracking(
                updater::trackBookingAndUpdateBalance,
                membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(bookingDTO))
                .orElseThen(BiConditionalConsumer.ofMembershipBookingTracking(
                        this::balanceBooking,
                        membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreUnbalancedAndNotTracked(bookingDTO))
                );
        trackingConditionalConsumers.add(saveBookingTrackingOrBalanceBooking);
    }

    void addRebalanceNotContractBookingTrackingCandidate() {
        trackingConditionalConsumers.add(BiConditionalConsumer.ofMembershipBookingTracking(updater::deleteBookingTracking,
                membershipTrackingBiPredicateProvider.isActivatedAndBookingIsTracked()));
    }

    private void deleteBookingTrackingAndRefreshMembership(BookingDTO bookingDTO) {
        updater.deleteBookingTracking(bookingDTO);
        membershipDTO = getMembershipDTO(bookingDTO);
    }

    private MembershipDTO getMembershipDTO(BookingDTO bookingDTO) {
        return supplyDtoOrThrowException(() -> externalModuleManager.getMembership(bookingDTO.getMembershipId()));
    }

    private void balanceBooking(BookingDTO bookingDTO) {
        updater.balanceBooking(bookingDTO, membershipDTO);
    }

    private BookingTrackingDTO retrieveBookingTrackingDTOOrTriggerRetry() {
        BookingTrackingDTO bookingTrackingDTO = supplyDtoOrThrowException(() -> externalModuleManager.getBookingTracking(bookingDTO));
        if (nonNull(bookingTrackingDTO)) {
            LOGGER.info("Booking tracking [booking {},membership {}] retrieved", bookingTrackingDTO.getBookingId(), bookingTrackingDTO.getMembershipId());
        } else {
            LOGGER.info("Booking {} is not tracked", bookingDTO.getId());
        }
        return bookingTrackingDTO;
    }
}
