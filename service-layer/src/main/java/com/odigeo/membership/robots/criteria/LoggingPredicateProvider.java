package com.odigeo.membership.robots.criteria;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

final class LoggingPredicateProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingPredicateProvider.class);

    private LoggingPredicateProvider() {
    }

    static <T> Predicate<T> loggingPredicate(Function<T, String> labelFunction, Predicate<T> predicate) {
        return t -> {
            boolean result = predicate.test(t);
            String label = labelFunction.apply(t);
            LOGGER.info(" {} : {} ", label, result);
            return result;
        };
    }

    static <T, V> BiPredicate<T, V> loggingPredicate(BiFunction<T, V, String> labelFunction, BiPredicate<T, V> predicate) {
        return (t, v) -> {
            boolean result = predicate.test(t, v);
            String label = labelFunction.apply(t, v);
            LOGGER.info(" {} : {} ", label, result);
            return result;
        };
    }
}
