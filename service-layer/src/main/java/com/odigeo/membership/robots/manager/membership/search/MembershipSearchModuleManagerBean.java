package com.odigeo.membership.robots.manager.membership.search;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.robots.apicall.ApiCall.Endpoint;
import com.odigeo.membership.robots.apicall.ApiCall.Result;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.mapper.request.MembershipSearchRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.List;

@Singleton
public class MembershipSearchModuleManagerBean implements MembershipSearchModuleManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipSearchModuleManagerBean.class);
    private final MembershipSearchApi membershipSearchApi;
    private final MembershipSearchRequestMapper membershipSearchRequestMapper;
    private final MembershipResponseMapper membershipResponseMapper;

    @Inject
    public MembershipSearchModuleManagerBean(MembershipSearchApi membershipSearchApi, MembershipSearchRequestMapper membershipSearchRequestMapper, MembershipResponseMapper membershipResponseMapper) {
        this.membershipSearchApi = membershipSearchApi;
        this.membershipSearchRequestMapper = membershipSearchRequestMapper;
        this.membershipResponseMapper = membershipResponseMapper;
    }

    @Override
    public ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> searchMemberships(SearchMembershipsDTO searchMembershipsDTO) {
        final ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO> searchWrapperBuilder = new ApiCallWrapperBuilder<>(Endpoint.SEARCH_MEMBERSHIPS);
        try {
            List<MembershipResponse> membershipResponses = membershipSearchApi.searchMemberships(membershipSearchRequestMapper.searchDtoToSearchRequestBuilder(searchMembershipsDTO).build());
            searchWrapperBuilder.result(Result.SUCCESS).request(searchMembershipsDTO)
                    .message(membershipResponses.size() + " retrieved!")
                    .response(membershipResponseMapper.membershipResponsesToDtos(membershipResponses));
        } catch (MembershipServiceException | UndeclaredThrowableException e) {
            LOGGER.error(e.getMessage(), e);
            searchWrapperBuilder.result(Result.FAIL).request(searchMembershipsDTO).throwable(e).message(e.getMessage());
        }
        final ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> searchCallWrapper = searchWrapperBuilder.build();
        searchCallWrapper.logResultAndMessageWithPrefix(LOGGER, searchCallWrapper.getEndpoint() + " by " + searchCallWrapper.getRequest() + " :");
        return searchCallWrapper;
    }

    @Override
    public ApiCallWrapper<MembershipDTO, Long> getMembership(Long membershipId) {
        final ApiCallWrapperBuilder<MembershipDTO, Long> searchWrapperBuilder = new ApiCallWrapperBuilder<>(Endpoint.GET_MEMBERSHIP);
        try {
            MembershipResponse membershipResponse = membershipSearchApi.getMembership(membershipId, true);
            searchWrapperBuilder.result(Result.SUCCESS).request(membershipId)
                    .message(membershipResponse + " retrieved!")
                    .response(membershipResponseMapper.membershipResponseToDto(membershipResponse));
        } catch (MembershipServiceException | UndeclaredThrowableException e) {
            LOGGER.error(e.getMessage(), e);
            searchWrapperBuilder.result(Result.FAIL).request(membershipId).throwable(e).message(e.getMessage());
        }
        final ApiCallWrapper<MembershipDTO, Long> searchCallWrapper = searchWrapperBuilder.build();
        searchCallWrapper.logResultAndMessageWithPrefix(LOGGER, searchCallWrapper.getEndpoint() + " by " + searchCallWrapper.getRequest() + " :");
        return searchCallWrapper;
    }

}
