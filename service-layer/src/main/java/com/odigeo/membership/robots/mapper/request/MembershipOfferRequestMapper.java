package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.offer.api.request.MembershipRenewalOfferRequest;
import com.odigeo.membership.offer.api.request.TestTokenSet;
import com.odigeo.membership.robots.dto.MembershipDTO;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Mapper(imports = {StringUtils.class})
public interface MembershipOfferRequestMapper {

    @Mapping(target = "websiteCode", source = "website")
    @Mapping(target = "anInterface", constant = "OFFLINE")
    @Mapping(target = "testTokenSet", expression = "java(setTestTokenSet())")
    @Mapping(target = "membershipId", source = "id")
    MembershipRenewalOfferRequest dtoToMembershipRenewalOfferRequest(MembershipDTO membershipDTO);

    default TestTokenSet setTestTokenSet() {
        Map<String, Integer> dimensionPartitionNumberMap = new HashMap<>();
        dimensionPartitionNumberMap.put("X20-4", 1); // Six months Price AB
        return new TestTokenSet(dimensionPartitionNumberMap);
    }

    default String today() {
        return DateTimeFormatter.ISO_DATE.format(LocalDate.now());
    }

    default String tomorrow() {
        return DateTimeFormatter.ISO_DATE.format(LocalDate.now().plusDays(1L));
    }
}
