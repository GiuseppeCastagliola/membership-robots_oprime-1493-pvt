package com.odigeo.membership.robots.mapper.response;

import com.google.common.base.Preconditions;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.CollectionAttempt;
import com.odigeo.bookingapi.v12.responses.CollectionSummary;
import com.odigeo.bookingapi.v12.responses.Fee;
import com.odigeo.bookingapi.v12.responses.MembershipSubscriptionBooking;
import com.odigeo.bookingapi.v12.responses.Movement;
import com.odigeo.bookingapi.v12.responses.ProductCategoryBooking;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.CollectionAttemptDTO;
import com.odigeo.membership.robots.dto.booking.MovementDTO;
import com.odigeo.membership.robots.dto.booking.ProductCategoryBookingDTO;
import com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@SuppressWarnings("PMD.AvoidDuplicateLiterals")
@Mapper(imports = MembershipFeeSubCodes.class)
public interface BookingResponseMapper {


    @Mapping(target = "id", source = "bookingBasicInfo.id")
    @Mapping(target = "userId", source = "userId")
    @Mapping(target = "feeContainers", source = "allFeeContainers")
    @Mapping(target = "currencyCode", source = "currencyCode")
    @Mapping(target = "membershipId", source = "bookingBasicInfo.membershipId")
    @Mapping(target = "productTypes", expression = "java(obtainProductTypes(bookingDetail))")
    @Mapping(target = "testBooking", source = "testBooking")
    @Mapping(target = "bookingStatus", source = "bookingStatus")
    @Mapping(target = "bookingProducts", expression = "java(obtainBookingProducts(bookingDetail))")
    @Mapping(target = "collectionAttempts", expression = "java(collectionAttemptsWithRecurringId(bookingDetail.getCollectionSummary()))")
    @Mapping(target = "bookingDate", source = "bookingBasicInfo.creationDate")
    @Mapping(target = "totalAvoidFeesAmount", expression = "java(totalFeeAmount(bookingDetail, MembershipFeeSubCodes.AVOID_FEES))")
    @Mapping(target = "totalCostFeesAmount", expression = "java(totalFeeAmount(bookingDetail, MembershipFeeSubCodes.COST_FEE))")
    @Mapping(target = "totalPerksAmount", expression = "java(totalFeeAmount(bookingDetail, MembershipFeeSubCodes.PERKS))")
    BookingDTO bookingDetailResponseToDto(BookingDetail bookingDetail);


    @Mapping(target = "id", source = "bookingBasicInfo.id")
    @Mapping(target = "membershipId", source = "membershipId")
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "feeContainers", ignore = true)
    @Mapping(target = "currencyCode", ignore = true)
    @Mapping(target = "productTypes", ignore = true)
    @Mapping(target = "bookingProducts", ignore = true)
    @Mapping(target = "testBooking", ignore = true)
    @Mapping(target = "collectionAttempts", ignore = true)
    @Mapping(target = "bookingStatus", ignore = true)
    @Mapping(target = "bookingDate", ignore = true)
    @Mapping(target = "totalPerksAmount", ignore = true)
    @Mapping(target = "totalCostFeesAmount", ignore = true)
    @Mapping(target = "totalAvoidFeesAmount", ignore = true)
    BookingDTO bookingSummaryResponseToDto(BookingSummary bookingSummary);

    @Mapping(target = "id", expression = "java(verifyBookingId(bookingSummary, bookingDetail))")
    @Mapping(target = "membershipId", source = "bookingSummary.membershipId")
    @Mapping(target = "userId", source = "bookingDetail.userId")
    @Mapping(target = "feeContainers", source = "bookingDetail.feeContainers")
    @Mapping(target = "currencyCode", source = "bookingDetail.currencyCode")
    @Mapping(target = "testBooking", source = "bookingDetail.testBooking")
    @Mapping(target = "bookingStatus", source = "bookingDetail.bookingStatus")
    @Mapping(target = "bookingProducts", source = "bookingDetail.bookingProducts")
    @Mapping(target = "collectionAttempts", source = "bookingDetail.collectionAttempts")
    @Mapping(target = "productTypes", ignore = true)
    @Mapping(target = "bookingDate", source = "bookingDetail.bookingDate")
    @Mapping(target = "totalPerksAmount", source = "bookingDetail.totalPerksAmount")
    @Mapping(target = "totalCostFeesAmount", source = "bookingDetail.totalCostFeesAmount")
    @Mapping(target = "totalAvoidFeesAmount", source = "bookingDetail.totalAvoidFeesAmount")
    BookingDTO bookingResponsesToDto(BookingDTO bookingSummary, BookingDTO bookingDetail);

    List<BookingDTO> bookingSummariesResponseToDto(List<BookingSummary> bookingSummaries);

    default long verifyBookingId(BookingDTO bookingSummary, BookingDTO bookingDetail) {
        Preconditions.checkState(bookingSummary.getId() == bookingDetail.getId(), "bookingSummaryId: %s != bookingDetailId: %s", bookingSummary.getId(), bookingDetail.getId());
        return bookingSummary.getId();
    }

    default List<String> obtainProductTypes(BookingDetail bookingDetail) {
        return Optional.ofNullable(bookingDetail.getBookingProducts())
                .orElse(Collections.emptyList())
                .stream()
                .map(ProductCategoryBooking::getProductType)
                .collect(Collectors.toList());
    }

    default List<ProductCategoryBookingDTO> obtainBookingProducts(BookingDetail bookingDetail) {
        return Optional.ofNullable(bookingDetail.getBookingProducts())
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .map(this::buildProductCategoryBookingDTO)
                .collect(Collectors.toList());
    }

    default ProductCategoryBookingDTO buildProductCategoryBookingDTO(ProductCategoryBooking bookingProduct) {
        ProductCategoryBookingDTO.Builder builder = ProductCategoryBookingDTO.builder()
                .id(bookingProduct.getId())
                .feeContainerId(bookingProduct.getFeeContainerId())
                .productType(bookingProduct.getProductType());
        if (bookingProduct instanceof MembershipSubscriptionBooking) {
            builder.price(((MembershipSubscriptionBooking) bookingProduct).getTotalPrice());
        }
        return builder.build();
    }

    default List<CollectionAttemptDTO> collectionAttemptsWithRecurringId(CollectionSummary collectionSummary) {
        List<CollectionAttemptDTO> collectionAttemptsWithRecurringIdAndLastMovement = new ArrayList<>();
        Optional.ofNullable(collectionSummary)
                .map(CollectionSummary::getAttempts)
                .orElse(Collections.emptyList())
                .stream()
                .filter(collectionAttempt -> StringUtils.isNotEmpty(collectionAttempt.getRecurringId()))
                .forEach(collectionAttempt -> buildCollectionAttempt(collectionAttemptsWithRecurringIdAndLastMovement, collectionAttempt));
        return collectionAttemptsWithRecurringIdAndLastMovement;
    }

    default void buildCollectionAttempt(List<CollectionAttemptDTO> collectionAttemptsWithRecurringIdAndLastMovement, CollectionAttempt collectionAttempt) {
        List<Movement> validMovements = collectionAttempt.getValidMovements();
        if (CollectionUtils.isNotEmpty(validMovements)) {
            Movement lastMovement = validMovements.get(validMovements.size() - 1);
            collectionAttemptsWithRecurringIdAndLastMovement.add(CollectionAttemptDTO.builder()
                    .recurringId(collectionAttempt.getRecurringId())
                    .id(collectionAttempt.getId())
                    .lastMovement(MovementDTO.builder()
                            .action(lastMovement.getAction())
                            .status(lastMovement.getStatus())
                            .build())
                    .build());
        }
    }

    default BigDecimal totalFeeAmount(BookingDetail bookingDetail, MembershipFeeSubCodes feeSubCode) {
        return isPrimeBooking(bookingDetail)
                ? Optional.ofNullable(bookingDetail.getAllFeeContainers())
                .orElse(Collections.emptyList())
                .stream()
                .flatMap(feeContainer -> feeContainer.getFees().stream())
                .filter(matchFees(feeSubCode))
                .map(Fee::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                : BigDecimal.ZERO;
    }

    default boolean isPrimeBooking(BookingDetail bookingDetail) {
        return nonNull(bookingDetail)
                && nonNull(bookingDetail.getBookingBasicInfo())
                && !Objects.equals(0L, bookingDetail.getBookingBasicInfo().getMembershipId());
    }

    default Predicate<Fee> matchFees(MembershipFeeSubCodes membershipFee) {
        return fee -> Optional.ofNullable(fee.getSubCode())
                .map(subCode -> subCode.equals(membershipFee.getFeeCode()))
                .orElseGet(() -> membershipFee.name().equals(fee.getFeeLabel()));
    }

}
