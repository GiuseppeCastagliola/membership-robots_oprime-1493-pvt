package com.odigeo.membership.robots.fees;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.FeeContainerDTO;
import com.odigeo.membership.robots.exceptions.MembershipSubscriptionBookingException;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.manager.bookingapi.FeeRequestBuilder;
import com.odigeo.membership.robots.manager.bookingapi.UpdateBookingRequestBuilder;
import com.odigeo.membership.robots.manager.membership.MembershipFeeContainerTypes;
import com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Singleton
public class RemnantFeeServiceBean implements RemnantFeeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemnantFeeServiceBean.class);
    private static final String COLON = " : ";
    protected static final String NO_PRIME_BOOKINGS_EXCEPTION = "No prime bookings found for membershipId: %s";
    protected static final String AC12_FEE_ALREADY_PRESENT_IN_BOOKING = "AC12 fee is already present in the booking %s";
    private static final String MULTIPLE_MEMBERSHIP_PAYMENTS_EXCEPTION = "Multiple membership payments found for membershipId: %s, bookingId: %s";
    private static final String PROCESS_PRIME_SUBSCRIPTION_BOOKING_FEES_EXCEPTION = "ConsumeRemnantBalance for membership %s not called as the "
            + "processPrimeSubscriptionBookingFees call failed for subscription booking.";
    protected static final String NO_MEMBERSHIP_PAYMENTS_EXCEPTION = "No membership payments found for membershipId: %s, bookingId %s";
    protected static final String MULTIPLE_FEES_EXCEPTION = "Multiple fees found for membershipId: %s, bookingId: %s, feeContainerId: %s";
    protected static final String NO_FEES_EXCEPTION = "No fees found for membershipId: %s, bookingId: %s, feeContainerId: %s";
    private static final List<String> MEMBERSHIP_FEE_TYPES = Arrays.stream(MembershipFeeContainerTypes.values()).map(Enum::name).collect(Collectors.toList());
    private static final Predicate<FeeContainerDTO> HAS_MEMBERSHIP_FEE_CONTAINER_TYPE = feeContainer -> MEMBERSHIP_FEE_TYPES.contains(feeContainer.getFeeContainerType());
    private static final Predicate<FeeContainerDTO> HAS_FEE_WITH_SUBSCRIPTION_OR_RENEWAL_SUBCODE = feeContainer -> feeContainer.getFeeDTOs().stream()
            .anyMatch(fee -> MembershipFeeContainerTypes.getAllSubCodes().contains(fee.getSubCode()));
    private static final Predicate<FeeContainerDTO> IS_MEMBERSHIP_FEE_CONTAINER = HAS_MEMBERSHIP_FEE_CONTAINER_TYPE.or(HAS_FEE_WITH_SUBSCRIPTION_OR_RENEWAL_SUBCODE);

    private final ExternalModuleManager externalModuleManager;

    @Inject
    public RemnantFeeServiceBean(ExternalModuleManager externalModuleManager) {
        this.externalModuleManager = externalModuleManager;
    }

    public ApiCallWrapper<BookingDTO, MembershipDTO> processPrimeSubscriptionBookingFees(ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper) {
        ApiCallWrapperBuilder<BookingDTO, MembershipDTO> processPrimeSubscriptionBookingFeesWrapperBuilder;
        MembershipDTO expiredMembership = primeSubscriptionBookingWrapper.getRequest();
        Optional<BookingDTO> membershipSubscriptionBooking = Optional.ofNullable(primeSubscriptionBookingWrapper.getResponse());

        if (membershipSubscriptionBooking.isPresent()) {
            processPrimeSubscriptionBookingFeesWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, MembershipDTO>(ApiCall.Endpoint.PROCESS_REMNANT_FEE, ApiCall.Result.FAIL)
                    .request(primeSubscriptionBookingWrapper.getRequest());
            final ApiCallWrapper<BookingDTO, BookingDTO> bookingProcessFeeWrapper = processFees(membershipSubscriptionBooking.get(), expiredMembership.getBalance());

            processPrimeSubscriptionBookingFeesWrapperBuilder.result(bookingProcessFeeWrapper.getResult())
                    .response(bookingProcessFeeWrapper.getResponse())
                    .message(bookingProcessFeeWrapper.getMessage())
                    .throwable(bookingProcessFeeWrapper.getThrowable());
        } else {
            String errorMessage = String.format(NO_PRIME_BOOKINGS_EXCEPTION, primeSubscriptionBookingWrapper.getRequest().getId());
            processPrimeSubscriptionBookingFeesWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, MembershipDTO>(primeSubscriptionBookingWrapper.getEndpoint(), ApiCall.Result.FAIL)
                    .request(expiredMembership)
                    .throwable(new MembershipSubscriptionBookingException(errorMessage)).message(errorMessage);
        }

        ApiCallWrapper<BookingDTO, MembershipDTO> processPrimeSubscriptionBookingFeesWrapper = processPrimeSubscriptionBookingFeesWrapperBuilder.build();
        processPrimeSubscriptionBookingFeesWrapper.logResultAndMessageWithPrefix(LOGGER, String.join(COLON, processPrimeSubscriptionBookingFeesWrapper.getEndpoint().name(),
                String.valueOf(expiredMembership.getId()), membershipSubscriptionBooking.map(BookingDTO::getId).map(String::valueOf).orElse("bookingId=null")));
        return processPrimeSubscriptionBookingFeesWrapper;
    }

    public ApiCallWrapper<MembershipDTO, MembershipDTO> consumeMembershipRemnantBalance(ApiCallWrapper<BookingDTO, MembershipDTO> processPrimeSubscriptionBookingFeesWrapper) {
        ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> consumeRemnantBalanceWrapperBuilder;
        MembershipDTO expiredMembership = processPrimeSubscriptionBookingFeesWrapper.getRequest();

        if (processPrimeSubscriptionBookingFeesWrapper.isSuccessful()) {
            ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantBalanceResponse = externalModuleManager.consumeRemnantMembershipBalance(expiredMembership);
            consumeRemnantBalanceWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE)
                    .result(consumeRemnantBalanceResponse.getResult())
                    .request(expiredMembership)
                    .response(consumeRemnantBalanceResponse.getResponse())
                    .throwable(consumeRemnantBalanceResponse.getThrowable())
                    .message(consumeRemnantBalanceResponse.getMessage());
        } else {
            String errorMessage = String.format(PROCESS_PRIME_SUBSCRIPTION_BOOKING_FEES_EXCEPTION, expiredMembership.getId());
            consumeRemnantBalanceWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE, ApiCall.Result.FAIL)
                    .request(expiredMembership) //to prevent lambda in ReporterImpl from throwing NPE
                    .throwable(ObjectUtils.defaultIfNull(processPrimeSubscriptionBookingFeesWrapper.getThrowable(), new MembershipSubscriptionBookingException(errorMessage)))
                    .message(errorMessage);
        }
        ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantMembershipBalance = consumeRemnantBalanceWrapperBuilder.build();
        consumeRemnantMembershipBalance.logResultAndMessageWithPrefix(LOGGER, String.join(COLON, consumeRemnantMembershipBalance.getEndpoint().name(), String.valueOf(expiredMembership.getId())));
        return consumeRemnantMembershipBalance;
    }

    private ApiCallWrapper<BookingDTO, BookingDTO> processFees(BookingDTO membershipSubscriptionBooking, BigDecimal membershipBalance) {
        final ApiCallWrapperBuilder<BookingDTO, BookingDTO> bookingProcessFeeWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.BOOKING_PROCESS_REMNANT_FEE)
                .request(membershipSubscriptionBooking);
        try {
            Optional<MembershipFeeSubCodes> membershipFeeSubCode = getMembershipFeeSubCode(membershipSubscriptionBooking);
            if (membershipFeeSubCode.isPresent()) {
                UpdateBookingRequest updateBookingRequest = buildProcessFeesUpdateBookingRequest(membershipBalance, membershipSubscriptionBooking.getCurrencyCode(), membershipFeeSubCode.get());
                ApiCallWrapper<BookingDTO, UpdateBookingRequest> updatedBooking = updateBooking(membershipSubscriptionBooking.getId(), updateBookingRequest);
                bookingProcessFeeWrapperBuilder.result(updatedBooking.getResult())
                        .message(updatedBooking.getMessage())
                        .response(updatedBooking.getResponse())
                        .throwable(updatedBooking.getThrowable());
            } else {
                bookingProcessFeeWrapperBuilder.result(ApiCall.Result.SUCCESS);
                bookingProcessFeeWrapperBuilder.message(String.format(AC12_FEE_ALREADY_PRESENT_IN_BOOKING, membershipSubscriptionBooking.getId()));
            }
        } catch (MembershipSubscriptionBookingException e) {
            bookingProcessFeeWrapperBuilder.result(ApiCall.Result.FAIL)
                    .throwable(e).message(e.getMessage());
        }
        ApiCallWrapper<BookingDTO, BookingDTO> bookingProcessFeeWrapper = bookingProcessFeeWrapperBuilder.build();
        bookingProcessFeeWrapper.logResultAndMessageWithPrefix(LOGGER, String.join(COLON, bookingProcessFeeWrapper.getEndpoint().name(), String.valueOf(membershipSubscriptionBooking.getMembershipId())));
        return bookingProcessFeeWrapper;
    }

    private Optional<MembershipFeeSubCodes> getMembershipFeeSubCode(BookingDTO membershipSubscriptionBookingDetail) throws MembershipSubscriptionBookingException {
        List<FeeContainerDTO> membershipFeeContainer = membershipSubscriptionBookingDetail.getFeeContainers().stream()
                .filter(IS_MEMBERSHIP_FEE_CONTAINER)
                .collect(Collectors.toList());
        if (membershipFeeContainer.size() != 1 || membershipFeeContainer.get(0).getFeeDTOs().size() != 1) {
            final boolean isAC12FeePresent = membershipFeeContainer.stream()
                    .map(FeeContainerDTO::getFeeDTOs)
                    .flatMap(List::stream)
                    .anyMatch(feeDTO -> MembershipFeeSubCodes.AC12.getFeeCode().equals(feeDTO.getSubCode()));
            if (isAC12FeePresent) {
                return Optional.empty();
            }
            handleFeeSubCodeException(membershipSubscriptionBookingDetail, membershipFeeContainer);
        }
        FeeContainerDTO feeContainer = membershipFeeContainer.get(0);
        return StringUtils.isEmpty(feeContainer.getFeeContainerType())
                ? Optional.of(MembershipFeeSubCodes.valueOf(feeContainer.getFeeDTOs().get(0).getSubCode()))
                : Optional.of(MembershipFeeContainerTypes.valueOf(feeContainer.getFeeContainerType()).getSubCode());
    }

    private UpdateBookingRequest buildProcessFeesUpdateBookingRequest(BigDecimal balance, String currencyCode, MembershipFeeSubCodes membershipFeeSubCode) {
        FeeRequest negatedFee = new FeeRequestBuilder().withAmount(balance.negate()).withCurrency(currencyCode).withSubCode(membershipFeeSubCode).build();
        FeeRequest remnantFee = new FeeRequestBuilder().withAmount(balance).withCurrency(currencyCode).withSubCode(MembershipFeeSubCodes.AC12).build();
        return new UpdateBookingRequestBuilder().withFeeRequests(Arrays.asList(negatedFee, remnantFee)).build();
    }

    private ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBooking(long id, UpdateBookingRequest updateBookingRequest) {
        final ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> updateBookingWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(
                ApiCall.Endpoint.UPDATE_BOOKING).request(updateBookingRequest);
        return externalModuleManager.updateBooking(id, updateBookingWrapperBuilder);
    }

    private void handleFeeSubCodeException(BookingDTO membershipSubscriptionBookingDetail, List<FeeContainerDTO> membershipFeeContainer)
            throws MembershipSubscriptionBookingException {
        String errorMessage;
        Long subscriptionBookingId = membershipSubscriptionBookingDetail.getId();
        Long expiredMembershipId = membershipSubscriptionBookingDetail.getMembershipId();
        if (membershipFeeContainer.size() != 1) {
            errorMessage = membershipFeeContainer.isEmpty() ? String.format(NO_MEMBERSHIP_PAYMENTS_EXCEPTION, expiredMembershipId, subscriptionBookingId)
                    : String.format(MULTIPLE_MEMBERSHIP_PAYMENTS_EXCEPTION, expiredMembershipId, subscriptionBookingId);
        } else {
            FeeContainerDTO feeContainer = membershipFeeContainer.get(0);
            errorMessage = feeContainer.getFeeDTOs().size() <= 1 ? String.format(NO_FEES_EXCEPTION, expiredMembershipId, subscriptionBookingId, feeContainer.getId())
                    : String.format(MULTIPLE_FEES_EXCEPTION, expiredMembershipId, subscriptionBookingId, feeContainer.getId());
        }
        throw new MembershipSubscriptionBookingException(errorMessage);
    }
}
