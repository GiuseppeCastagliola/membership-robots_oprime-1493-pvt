package com.odigeo.membership.robots.consumer.updater;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.requests.CreateBookingProductCategoryRequest;
import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.bookingapi.v12.requests.PaymentType;
import com.odigeo.bookingapi.v12.requests.ProductCategoryBookingItemType;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.manager.bookingapi.FeeRequestBuilder;
import com.odigeo.membership.robots.manager.bookingapi.SearchBookingsRequestBuilder;
import com.odigeo.membership.robots.manager.bookingapi.UpdateBookingRequestBuilder;
import com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Singleton
public class BookingUpdater {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingUpdater.class);
    private final MembershipRobotsMetrics metrics;
    private final ExternalModuleManager externalModuleManager;

    @Inject
    public BookingUpdater(MembershipRobotsMetrics metrics, ExternalModuleManager externalModuleManager) {
        this.metrics = metrics;
        this.externalModuleManager = externalModuleManager;
    }

    void updateBookingWithUnbalancedFees(BookingDTO bookingDTO, MembershipDTO membershipDTO) {
        LOGGER.info("Trying to update booking unbalanced : {}", bookingDTO.getId());
        metrics.incrementCounter(MetricsName.UPDATE_BOOKING_ATTEMPTS);
        BigDecimal balancePlusAvoidFee = membershipDTO.getBalance().add(bookingDTO.getTotalAvoidFeesAmount());
        FeeRequest newAvoidFee = buildFeeRequest(bookingDTO, balancePlusAvoidFee.negate(), MembershipFeeSubCodes.AVOID_FEES);
        FeeRequest newCostFee = buildFeeRequest(bookingDTO, balancePlusAvoidFee, MembershipFeeSubCodes.COST_FEE);
        UpdateBookingRequest updateBookingRequest = buildUpdateRequest(newAvoidFee, newCostFee);
        final ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> updateBookingWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(
                ApiCall.Endpoint.UPDATE_BOOKING).request(updateBookingRequest);
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = externalModuleManager.updateBooking(bookingDTO.getId(), updateBookingWrapperBuilder);
        if (updateBookingApiCallWrapper.isFailed()) {
            metrics.incrementCounter(MetricsName.UPDATE_BOOKING_FAILED);
            throw new RetryableException(Optional.ofNullable(updateBookingApiCallWrapper.getThrowable())
                    .orElseThrow(() -> new RetryableException("Unknown fail updating unbalanced booking " + bookingDTO.getId())));
        } else {
            metrics.incrementCounter(MetricsName.UPDATE_BOOKING_SUCCESS);
        }
    }

    boolean updateBookingBasicFreeFirstBooking(BookingDTO bookingDTO, MembershipDTO membershipDTO) {
        LOGGER.info("Basic Free To Update Booking {} ", bookingDTO.getId());
        if (CollectionUtils.isEmpty(searchSubscriptionBookings(membershipDTO))) {
            metrics.incrementCounter(MetricsName.BASIC_FREE_UPDATES);
            String currency = StringUtils.defaultIfEmpty(membershipDTO.getCurrencyCode(), bookingDTO.getCurrencyCode());
            UpdateBookingRequest updateBookingRequest = buildUpdateRequest(membershipDTO.getId(), currency);
            final ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> updateBookingWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(
                    ApiCall.Endpoint.UPDATE_BOOKING).request(updateBookingRequest);
            ApiCallWrapper<BookingDTO, UpdateBookingRequest> apiCallWrapper = externalModuleManager.updateBooking(bookingDTO.getId(), updateBookingWrapperBuilder);
            if (apiCallWrapper.isFailed()) {
                metrics.incrementCounter(MetricsName.BASIC_FREE_UPDATES_FAILED);
                throw new RetryableException(Optional.ofNullable(apiCallWrapper.getThrowable())
                        .orElseThrow(() -> new RetryableException("Unknown fail updating booking, add product to basic free " + bookingDTO.getId())));
            } else {
                metrics.incrementCounter(MetricsName.BASIC_FREE_UPDATES_SUCCESS);
                LOGGER.info("Booking {} updated, subscription product added", bookingDTO.getId());
                return Boolean.TRUE;
            }
        } else {
            metrics.incrementCounter(MetricsName.BASIC_FREE_UPDATES_ALREADY_WITH_SUBSCRIPTION);
        }
        return Boolean.FALSE;
    }

    private List<BookingDTO> searchSubscriptionBookings(MembershipDTO membershipDTO) {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequestBuilder().bookingSubscriptionPrime(Boolean.TRUE).membershipId(membershipDTO.getId()).build();
        ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> searchBookingWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS)
                .request(searchBookingsRequest);
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookings = externalModuleManager.searchBookings(searchBookingWrapperBuilder);
        if (searchBookings.isFailed()) {
            throw new RetryableException(Optional.ofNullable(searchBookings.getThrowable())
                    .orElseThrow(() -> new RetryableException("Unknown fail searching prime subscription booking to add product to basic free, membership " + membershipDTO.getId())));
        }
        return searchBookings.getResponse();
    }

    private UpdateBookingRequest buildUpdateRequest(Long membershipId, String currency) {
        FeeRequest feeRequest = new FeeRequest();
        feeRequest.setCurrency(currency);
        feeRequest.setAmount(BigDecimal.ZERO);
        UpdateBookingRequestBuilder.CreateOtherProductProviderBookingBuilder createOtherProductProviderBookingBuilder = new UpdateBookingRequestBuilder.CreateOtherProductProviderBookingBuilder(Status.CONTRACT, BigDecimal.ZERO, currency, membershipId)
                .createProviderPaymentDetailRequestBuilder(new UpdateBookingRequestBuilder.CreateProviderPaymentDetailRequestBuilder(PaymentType.CASH, BigDecimal.ZERO, currency));
        CreateBookingProductCategoryRequest createProduct = new UpdateBookingRequestBuilder.CreateBookingProductCategoryRequestBuilder(ProductCategoryBookingItemType.MEMBERSHIP_SUBSCRIPTION)
                .createOtherProductProviderBookingBuilder(createOtherProductProviderBookingBuilder)
                .feeRequests(feeRequest)
                .build();
        return new UpdateBookingRequestBuilder().createBookingProductCategoryRequests(createProduct).buildAddProduct();
    }

    private FeeRequest buildFeeRequest(BookingDTO bookingDTO, BigDecimal amount, MembershipFeeSubCodes membershipFeeSubCode) {
        return new FeeRequestBuilder()
                .withAmount(amount)
                .withCurrency(bookingDTO.getCurrencyCode())
                .withSubCode(membershipFeeSubCode)
                .build();
    }

    private UpdateBookingRequest buildUpdateRequest(FeeRequest newAvoidFee, FeeRequest newCostFee) {
        return new UpdateBookingRequestBuilder()
                .withFeeRequests(Arrays.asList(newAvoidFee, newCostFee))
                .build();
    }
}
