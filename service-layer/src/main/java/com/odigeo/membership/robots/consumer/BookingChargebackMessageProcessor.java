package com.odigeo.membership.robots.consumer;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.messaging.BasicMessage;
import com.odigeo.commons.messaging.MessageProcessor;
import com.odigeo.membership.robots.mapper.response.consumer.DeactivateMembershipByChargebackMapper;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

import static java.util.Objects.nonNull;

@Singleton
public class BookingChargebackMessageProcessor implements MessageProcessor<BasicMessage> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingChargebackMessageProcessor.class);
    private static final String NOTIFICATION_CONSUMER_ROBOT = "Message to process to determine if it is associated with a prime product and then deactivate the membership: {}";
    private static final String MESSAGE_RETRY_PROCESS_NOTIFICATION_CONSUMER = "Retry #{} to process the message {} and determine if it is associated with a prime product and then deactivate the membership";
    private static final String MESSAGE_FAILED_PROCESS_NOTIFICATION_CONSUMER = "The message {} could not be processed to determine if it is associated with a prime product and then deactivate the membership. Number of attempts: {}, last error obtained: {}";
    private static final String MESSAGE_SUCCESS_PROCESS_NOTIFICATION_CONSUMER = "Message {} processed successfully";
    private static final String STATUS_CHARGEBACKED = "CHARGEBACKED";
    private static final long RETRY_DELAY = 1;
    private static final int RETRIES = 5;

    private final DeactivateMembershipByChargebackMessageConsumer deactivateMembershipByChargebackMessageConsumer;
    private final DeactivateMembershipByChargebackMapper deactivateMembershipByChargebackMapper;
    private final MembershipRobotsMetrics membershipRobotsMetrics;

    @Inject
    public BookingChargebackMessageProcessor(DeactivateMembershipByChargebackMessageConsumer deactivateMembershipByChargebackMessageConsumer,
                                             DeactivateMembershipByChargebackMapper deactivateMembershipByChargebackMapper,
                                             MembershipRobotsMetrics membershipRobotsMetrics) {
        this.deactivateMembershipByChargebackMessageConsumer = deactivateMembershipByChargebackMessageConsumer;
        this.deactivateMembershipByChargebackMapper = deactivateMembershipByChargebackMapper;
        this.membershipRobotsMetrics = membershipRobotsMetrics;
    }

    @Override
    public void onMessage(BasicMessage basicMessage) {
        if (nonNull(basicMessage) && !basicMessage.getExtraParameters().isEmpty()) {
            CollectionSummaryChargebackNotifications collectionSummaryChargebackNotification = deactivateMembershipByChargebackMapper.basicMessageToCollectionSummaryChargebackNotifications(basicMessage);
            if (STATUS_CHARGEBACKED.equals(collectionSummaryChargebackNotification.getStatus())) {
                bookingChargebackDoneProcessor(collectionSummaryChargebackNotification);
            }
        }
    }

    private void bookingChargebackDoneProcessor(CollectionSummaryChargebackNotifications collectionSummaryChargebackNotification) {
        LOGGER.warn(NOTIFICATION_CONSUMER_ROBOT, collectionSummaryChargebackNotification);
        membershipRobotsMetrics.incrementCounter(MetricsName.TOTAL_PROCESSED_CHARGEBACK);
        RetryPolicy<Object> retryPolicy  = new RetryPolicy<>()
                .handle(Exception.class)
                .withDelay(Duration.ofMinutes(RETRY_DELAY))
                .withMaxRetries(RETRIES)
                .onRetry(executionEvent -> LOGGER.warn(MESSAGE_RETRY_PROCESS_NOTIFICATION_CONSUMER, executionEvent.getAttemptCount(), collectionSummaryChargebackNotification));
        Failsafe.with(retryPolicy)
                .onFailure(failure -> LOGGER.warn(MESSAGE_FAILED_PROCESS_NOTIFICATION_CONSUMER, collectionSummaryChargebackNotification, failure.getAttemptCount(), failure.getFailure().getMessage(), failure.getFailure()))
                .onSuccess(result -> LOGGER.warn(MESSAGE_SUCCESS_PROCESS_NOTIFICATION_CONSUMER, collectionSummaryChargebackNotification))
                .runAsync(() -> deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotification));
    }
}
