package com.odigeo.membership.robots.manager;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.criteria.CollectionAttemptPredicateProvider;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipDTOBuilder;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.CollectionAttemptDTO;
import com.odigeo.membership.robots.manager.bookingapi.BookingApiManager;
import com.odigeo.membership.robots.manager.membership.MembershipModuleManager;
import com.odigeo.membership.robots.manager.membership.MembershipModuleManagerTracking;
import com.odigeo.membership.robots.manager.membership.search.MembershipSearchModuleManager;
import com.odigeo.membership.robots.manager.offer.MembershipOfferModuleManager;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class ExternalModuleManagerBean implements ExternalModuleManager {

    private final MembershipOfferModuleManager membershipOfferModuleManager;
    private final MembershipModuleManager membershipModuleManager;
    private final MembershipSearchModuleManager membershipSearchModuleManager;
    private final MembershipModuleManagerTracking membershipModuleManagerTracking;
    private final BookingApiManager bookingApiManager;
    private final CollectionAttemptPredicateProvider collectionAttemptPredicateProvider;

    @Inject
    public ExternalModuleManagerBean(MembershipOfferModuleManager membershipOfferModuleManager, MembershipModuleManager membershipModuleManager,
                                     MembershipSearchModuleManager membershipSearchModuleManager, BookingApiManager bookingApiManager, MembershipModuleManagerTracking membershipModuleManagerTracking,
                                     CollectionAttemptPredicateProvider collectionAttemptPredicateProvider) {
        this.membershipOfferModuleManager = membershipOfferModuleManager;
        this.membershipModuleManager = membershipModuleManager;
        this.membershipSearchModuleManager = membershipSearchModuleManager;
        this.bookingApiManager = bookingApiManager;
        this.membershipModuleManagerTracking = membershipModuleManagerTracking;
        this.collectionAttemptPredicateProvider = collectionAttemptPredicateProvider;
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> createPendingToCollectFromPreviousMembership(ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper) {
        return membershipModuleManager.createPendingToCollect(subscriptionOfferWrapper);
    }

    @Override
    public ApiCallWrapper<MembershipOfferDTO, MembershipDTO> getMembershipSubscriptionOffer(MembershipDTO previousMembership) {
        return membershipOfferModuleManager.getOffer(previousMembership);
    }

    @Override
    public ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> searchMemberships(SearchMembershipsDTO searchMembershipsDTO) {
        return membershipSearchModuleManager.searchMemberships(searchMembershipsDTO);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembership(MembershipDTO membershipDTO) {
        return membershipModuleManager.expireMembership(membershipDTO);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> discardMembership(MembershipDTO membershipDTO) {
        return membershipModuleManager.discardMembership(membershipDTO);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> enableMembershipAutoRenewal(MembershipDTO membershipDTO) {
        return membershipModuleManager.enableAutoRenewal(membershipDTO);
    }

    @Override
    public ApiCallWrapper<List<MembershipDTO>, List<Long>> retrieveMembershipsByIds(List<Long> membershipIds) {
        final List<MembershipDTO> totalResult = membershipIds.stream()
                .map(membershipSearchModuleManager::getMembership)
                .map(ApiCallWrapper::getResponse)
                .collect(Collectors.toList());
        return new ApiCallWrapperBuilder<List<MembershipDTO>, List<Long>>(ApiCall.Endpoint.SEARCH_MEMBERSHIPS)
                .request(membershipIds).response(totalResult).build();
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantMembershipBalance(MembershipDTO expiredMembership) {
        return membershipModuleManager.consumeRemnantMembershipBalance(expiredMembership);
    }

    @Override
    public ApiCallWrapper<BookingDTO, MembershipDTO> getPrimeSubscriptionBookingForMembership(MembershipDTO membership) {
        return bookingApiManager.getPrimeSubscriptionBookingForMembership(membership);
    }

    @Override
    public ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookings(ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> bookingSummariesWrapperBuilder) {
        return bookingApiManager.searchBookings(bookingSummariesWrapperBuilder);
    }

    @Override
    public ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBooking(long id, ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> updateBookingWrapperBuilder) {
        return bookingApiManager.updateBooking(id, updateBookingWrapperBuilder);
    }

    @Override
    public ApiCallWrapper<BookingTrackingDTO, BookingDTO> getBookingTracking(BookingDTO bookingDTO) {
        return membershipModuleManagerTracking.getBookingTracking(bookingDTO);
    }

    @Override
    public ApiCallWrapper<BookingTrackingDTO, BookingTrackingDTO> trackBookingAndUpdateBalance(BookingTrackingDTO bookingTrackingDTO) {
        return membershipModuleManagerTracking.trackBookingAndUpdateBalance(bookingTrackingDTO);
    }

    @Override
    public ApiCallWrapper<Void, BookingDTO> deleteBookingTrackingAndUpdateBalance(BookingDTO bookingDTO) {
        return membershipModuleManagerTracking.deleteBookingTrackingAndUpdateBalance(bookingDTO);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, Long> getMembership(Long membershipId) {
        return membershipSearchModuleManager.getMembership(membershipId);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> saveRecurringId(MembershipDTO membershipDTO, BookingDTO bookingDTO) {
        MembershipDTO membershipWithRecurring = MembershipDTOBuilder.builderFromDto(membershipDTO)
                .recurringId(getRecurringIdFromBooking(bookingDTO))
                .build();
        return membershipModuleManager.saveRecurringId(membershipWithRecurring);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> activateMembership(MembershipDTO membershipDTO, BookingDTO bookingDTO) {
        return membershipModuleManager.activateMembership(membershipDTO, bookingDTO);
    }

    private String getRecurringIdFromBooking(BookingDTO bookingDTO) {
        return bookingDTO.getCollectionAttempts().stream()
                .filter(collectionAttemptPredicateProvider.hasRecurringId()
                        .and(collectionAttemptPredicateProvider.isLastMovementStatusAndActionValid()))
                .findFirst()
                .map(CollectionAttemptDTO::getRecurringId)
                .orElse(StringUtils.EMPTY);
    }
}
