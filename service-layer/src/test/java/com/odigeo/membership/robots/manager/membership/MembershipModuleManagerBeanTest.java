package com.odigeo.membership.robots.manager.membership;

import com.odigeo.membership.MembershipService;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.request.product.ActivationRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.ExpirationConfiguration;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipDTOBuilder;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.mapper.request.MembershipRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import org.mapstruct.factory.Mappers;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipModuleManagerBeanTest {
    private static final long TEST_MEMBERSHIP_ID = 1L;
    private static final long TEST_MEMBERSHIP_PENDING_TO_COLLECT_ID = 2L;
    private static final Integer TWELVE = 12;
    private static final Integer THREE = 3;
    private static final String EXPIRED = "EXPIRED";
    private static final String ACTIVATED = "ACTIVATED";
    private static final String DEACTIVATED = "DEACTIVATED";
    private static final String DISCARDED = "DISCARDED";
    private static final String TEST_WEBSITE = "ES";
    private static final String NON_RECURRING = "NON_RECURRING";
    private static final String BUSINESS = "BUSINESS";
    private static final String POST_BOOKING = "POST_BOOKING";
    private static final long TEST_MEMBERACCOUNT_ID = 3L;
    private static final String EUR = "EUR";
    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final LocalDateTime EXPECTED_EXPIRATION_DATE = NOW.plusMonths(TWELVE);
    private static final LocalDateTime EXPECTED_EXPIRATION_DATE_MONTHLY_SUBSCRIPTIONS = NOW.plusMonths(THREE);
    private static final BigDecimal TEST_SUBSCRIPTION_PRICE = BigDecimal.TEN;
    private static final MembershipDTO MEMBERSHIP_DTO = MembershipDTO.builder().status(ACTIVATED).id(TEST_MEMBERSHIP_ID).balance(BigDecimal.TEN).build();
    private static final MembershipOfferDTO.Builder COMMON_MEMBERSHIP_OFFER_BUILDER = MembershipOfferDTO.builder().website(TEST_WEBSITE).price(TEST_SUBSCRIPTION_PRICE).currencyCode(EUR).duration(TWELVE);
    private static final MembershipOfferDTO MEMBERSHIP_OFFER_DTO = COMMON_MEMBERSHIP_OFFER_BUILDER.build();
    private static final MembershipInternalServerErrorException MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION = new MembershipInternalServerErrorException("Exception");
    private static final MembershipDTOBuilder COMMON_MEMBERSHIP_BUILDER = MembershipDTO.builder()
            .id(TEST_MEMBERSHIP_ID)
            .website(TEST_WEBSITE)
            .recurringId(NON_RECURRING)
            .membershipType(BUSINESS)
            .sourceType(POST_BOOKING)
            .memberAccountId(TEST_MEMBERACCOUNT_ID)
            .currencyCode(EUR);
    private static final MembershipDTO PREVIOUS_MEMBERSHIP_DTO = COMMON_MEMBERSHIP_BUILDER
            .expirationDate(NOW)
            .status(EXPIRED)
            .build();
    private static final MembershipDTO PREVIOUS_MEMBERSHIP_DTO_NULL_EXPIRATION = COMMON_MEMBERSHIP_BUILDER
            .expirationDate(null)
            .build();
    private static final String ENABLED = "ENABLED";
    private static final String DISABLED = "DISABLED";

    @Mock
    private MembershipService membershipService;
    @Mock
    private ExpirationConfiguration expirationConfiguration;
    private MembershipModuleManagerBean membershipModuleManagerBean;
    @Captor
    private ArgumentCaptor<CreatePendingToCollectRequest> createPendingToCollectRequestArgumentCaptor;

    @BeforeMethod
    public void setup() {
        openMocks(this);
        given(expirationConfiguration.isMonthlyWebsite(eq(TEST_WEBSITE))).willReturn(Boolean.FALSE);
        membershipModuleManagerBean = new MembershipModuleManagerBean(membershipService, Mappers.getMapper(MembershipRequestMapper.class),
                Mappers.getMapper(MembershipResponseMapper.class), expirationConfiguration);
    }

    @Test
    public void testExpireMembership() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willReturn(true);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembershipWrapper = membershipModuleManagerBean.expireMembership(MEMBERSHIP_DTO);
        assertTrue(expireMembershipWrapper.isSuccessful());
        assertEquals(expireMembershipWrapper.getResponse().getStatus(), EXPIRED);
    }

    @Test
    public void testExpireMembershipFailed() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willReturn(false);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembershipWrapper = membershipModuleManagerBean.expireMembership(MEMBERSHIP_DTO);
        assertFalse(expireMembershipWrapper.isSuccessful());
        assertEquals(expireMembershipWrapper.getResponse().getStatus(), ACTIVATED);
    }

    @Test
    public void testExpireMembershipException() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willThrow(MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembershipWrapper = membershipModuleManagerBean.expireMembership(MEMBERSHIP_DTO);
        assertFalse(expireMembershipWrapper.isSuccessful());
        assertEquals(expireMembershipWrapper.getResponse().getStatus(), ACTIVATED);
        assertEquals(expireMembershipWrapper.getThrowable(), MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        assertEquals(expireMembershipWrapper.getMessage(), "Update membership failed when EXPIRE_MEMBERSHIP was requested for membershipId " + MEMBERSHIP_DTO.getId());
    }

    @Test
    public void testExpireMembershipAlreadyExpired() {
        MembershipDTO expiredDto = COMMON_MEMBERSHIP_BUILDER.status(EXPIRED).expirationDate(NOW).build();
        final ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembershipWrapper = membershipModuleManagerBean.expireMembership(expiredDto);
        then(membershipService).should(never()).updateMembership(any());
        assertTrue(expireMembershipWrapper.isSuccessful());
        assertEquals(expireMembershipWrapper.getResponse().getStatus(), EXPIRED);
        assertNull(expireMembershipWrapper.getThrowable());
    }

    @Test
    public void testCreatePendingToCollect() {
        ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper = buildSubscriptionOfferWrapper(PREVIOUS_MEMBERSHIP_DTO, ApiCall.Result.SUCCESS).build();
        given(membershipService.createMembership(any(CreatePendingToCollectRequest.class))).willReturn(TEST_MEMBERSHIP_PENDING_TO_COLLECT_ID);

        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingToCollectWrapper = membershipModuleManagerBean.createPendingToCollect(subscriptionOfferWrapper);

        then(membershipService).should().createMembership(any(CreatePendingToCollectRequest.class));
        assertTrue(pendingToCollectWrapper.isSuccessful());
        assertEquals(pendingToCollectWrapper.getEndpoint(), ApiCall.Endpoint.CREATE_PENDING_TO_COLLECT);
        assertEquals(pendingToCollectWrapper.getResponse().getId(), TEST_MEMBERSHIP_PENDING_TO_COLLECT_ID);
        assertEquals(pendingToCollectWrapper.getResponse().getCurrencyCode(), MEMBERSHIP_OFFER_DTO.getCurrencyCode());
        assertEquals(pendingToCollectWrapper.getResponse().getTotalPrice(), MEMBERSHIP_OFFER_DTO.getPrice());
        assertEquals(pendingToCollectWrapper.getResponse().getExpirationDate(), EXPECTED_EXPIRATION_DATE);
        assertEquals(pendingToCollectWrapper.getResponse().getMonthsDuration().intValue(), 12);
    }

    @Test
    public void testCreatePendingToCollectRequest() {
        ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper = buildSubscriptionOfferWrapper(PREVIOUS_MEMBERSHIP_DTO, ApiCall.Result.SUCCESS).build();
        given(membershipService.createMembership(any(CreatePendingToCollectRequest.class))).willReturn(TEST_MEMBERSHIP_PENDING_TO_COLLECT_ID);

        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingToCollectWrapper = membershipModuleManagerBean.createPendingToCollect(subscriptionOfferWrapper);

        then(membershipService).should().createMembership(createPendingToCollectRequestArgumentCaptor.capture());
        CreatePendingToCollectRequest request = createPendingToCollectRequestArgumentCaptor.getValue();

        assertTrue(pendingToCollectWrapper.isSuccessful());
        assertEquals(request.getWebsite(), TEST_WEBSITE);
        assertEquals(request.getSubscriptionPrice(), TEST_SUBSCRIPTION_PRICE);
        assertEquals(request.getRecurringId(), NON_RECURRING);
        assertEquals(request.getSourceType(), POST_BOOKING);
        assertEquals(request.getMembershipType(), BUSINESS);
        assertEquals(request.getExpirationDate(), EXPECTED_EXPIRATION_DATE.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        assertEquals(request.getMonthsToRenewal(), TWELVE.intValue());
        assertEquals(request.getMemberAccountId(), TEST_MEMBERACCOUNT_ID);
        assertEquals(request.getCurrencyCode(), EUR);
    }

    @Test
    public void testCreatePendingToCollectMonthlySubscriptionRequest() {
        ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper = buildSubscriptionOfferWrapper(PREVIOUS_MEMBERSHIP_DTO, ApiCall.Result.SUCCESS).build();
        given(membershipService.createMembership(any(CreatePendingToCollectRequest.class))).willReturn(TEST_MEMBERSHIP_PENDING_TO_COLLECT_ID);
        given(expirationConfiguration.isMonthlyWebsite(eq(TEST_WEBSITE))).willReturn(Boolean.TRUE);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingToCollectWrapper = membershipModuleManagerBean.createPendingToCollect(subscriptionOfferWrapper);

        then(membershipService).should().createMembership(createPendingToCollectRequestArgumentCaptor.capture());
        CreatePendingToCollectRequest request = createPendingToCollectRequestArgumentCaptor.getValue();

        assertTrue(pendingToCollectWrapper.isSuccessful());
        assertEquals(request.getWebsite(), TEST_WEBSITE);
        assertEquals(request.getSubscriptionPrice(), TEST_SUBSCRIPTION_PRICE);
        assertEquals(request.getRecurringId(), NON_RECURRING);
        assertEquals(request.getSourceType(), POST_BOOKING);
        assertEquals(request.getMembershipType(), BUSINESS);
        assertEquals(request.getExpirationDate(), EXPECTED_EXPIRATION_DATE_MONTHLY_SUBSCRIPTIONS.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        assertEquals(request.getMonthsToRenewal(), TWELVE.intValue());
        assertEquals(request.getMemberAccountId(), TEST_MEMBERACCOUNT_ID);
        assertEquals(request.getCurrencyCode(), EUR);
    }

    @Test
    public void testCreatePendingToCollectNullExpiration() {
        ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper = buildSubscriptionOfferWrapper(PREVIOUS_MEMBERSHIP_DTO_NULL_EXPIRATION, ApiCall.Result.SUCCESS).build();

        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingToCollectWrapper = membershipModuleManagerBean.createPendingToCollect(subscriptionOfferWrapper);

        then(membershipService).should(never()).createMembership(any(CreatePendingToCollectRequest.class));
        assertFalse(pendingToCollectWrapper.isSuccessful());
        assertTrue(pendingToCollectWrapper.getMessage().contains(
                String.format("Expiration date should not be null. previousMembership id:%s, previousMembership expirationDate:%s, membershipOffer duration:%s", TEST_MEMBERSHIP_ID, null, TWELVE))
        );
    }

    @Test
    public void testCreatePendingToCollectException() {
        ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper = buildSubscriptionOfferWrapper(PREVIOUS_MEMBERSHIP_DTO, ApiCall.Result.SUCCESS).build();
        given(membershipService.createMembership(any(CreatePendingToCollectRequest.class))).willThrow(MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);

        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingToCollectWrapper = membershipModuleManagerBean.createPendingToCollect(subscriptionOfferWrapper);

        then(membershipService).should().createMembership(any(CreatePendingToCollectRequest.class));
        assertFalse(pendingToCollectWrapper.isSuccessful());
        assertEquals(pendingToCollectWrapper.getResponse(), PREVIOUS_MEMBERSHIP_DTO);
        assertEquals(pendingToCollectWrapper.getThrowable(), MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        assertEquals(pendingToCollectWrapper.getMessage(), MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION.getMessage());
    }

    @Test
    public void testCreatePendingToCollectSubscriptionOfferFail() {
        ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferFailedWrapper = buildSubscriptionOfferWrapper(PREVIOUS_MEMBERSHIP_DTO, ApiCall.Result.FAIL)
                .message("Subscription Offer Message").throwable(new Exception("Subscription Offer Exception")).build();

        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingToCollectWrapper = membershipModuleManagerBean.createPendingToCollect(subscriptionOfferFailedWrapper);

        then(membershipService).should(never()).createMembership(any(CreatePendingToCollectRequest.class));
        assertEquals(pendingToCollectWrapper.getEndpoint(), ApiCall.Endpoint.GET_OFFER);
        assertFalse(pendingToCollectWrapper.isSuccessful());
        assertEquals(pendingToCollectWrapper.getRequest(), subscriptionOfferFailedWrapper.getRequest());
        assertEquals(pendingToCollectWrapper.getThrowable(), subscriptionOfferFailedWrapper.getThrowable());
        assertEquals(pendingToCollectWrapper.getMessage(), subscriptionOfferFailedWrapper.getMessage());
    }

    @Test
    public void testConsumeRemnantMembershipBalance() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willReturn(true);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantBalanceWrapper = membershipModuleManagerBean.consumeRemnantMembershipBalance(MEMBERSHIP_DTO);
        assertTrue(consumeRemnantBalanceWrapper.isSuccessful());
        assertEquals(consumeRemnantBalanceWrapper.getResponse().getBalance(), BigDecimal.ZERO);
    }

    @Test
    public void testDiscardMembership() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willReturn(true);
        MembershipDTO request = new MembershipDTOBuilder().status(ACTIVATED).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> wrapper = membershipModuleManagerBean.discardMembership(request);
        Assert.assertEquals(wrapper.getRequest(), request);
        MembershipDTO response = wrapper.getResponse();
        Assert.assertEquals(response.getId(), request.getId());
        Assert.assertEquals(response.getStatus(), DISCARDED);
    }

    @Test
    public void testAlreadyDiscardedMembership() {
        MembershipDTO request = new MembershipDTOBuilder().status(DISCARDED).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> wrapper = membershipModuleManagerBean.discardMembership(request);
        Assert.assertEquals(wrapper.getRequest(), request);
        MembershipDTO response = wrapper.getResponse();
        Assert.assertEquals(response.getId(), request.getId());
        Assert.assertEquals(response.getStatus(), DISCARDED);
    }

    @Test
    public void testDeactivateMembership() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willReturn(true);
        MembershipDTO request = new MembershipDTOBuilder().status(ACTIVATED).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> wrapper = membershipModuleManagerBean.deactivateMembership(request);
        Assert.assertEquals(wrapper.getRequest(), request);
        MembershipDTO response = wrapper.getResponse();
        Assert.assertEquals(response.getId(), request.getId());
        Assert.assertEquals(response.getStatus(), DEACTIVATED);
    }

    @Test
    public void testDeactivateMembershipAlreadyDeactivate() {
        MembershipDTO request = new MembershipDTOBuilder().status(DEACTIVATED).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> wrapper = membershipModuleManagerBean.deactivateMembership(request);
        Assert.assertEquals(wrapper.getRequest(), request);
        MembershipDTO response = wrapper.getResponse();
        Assert.assertEquals(response.getId(), request.getId());
        Assert.assertEquals(response.getStatus(), DEACTIVATED);
    }

    @Test
    public void testEnableMembershipAutoRenewal() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willReturn(true);
        MembershipDTO request = new MembershipDTOBuilder().autoRenewal(DISABLED).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> wrapper = membershipModuleManagerBean.enableAutoRenewal(request);
        Assert.assertEquals(wrapper.getRequest(), request);
        MembershipDTO response = wrapper.getResponse();
        Assert.assertEquals(response.getId(), request.getId());
        Assert.assertEquals(response.getAutoRenewal(), ENABLED);
    }

    @Test
    public void testEnableMembershipAutoRenewalAlreadyEnabled() {
        MembershipDTO request = new MembershipDTOBuilder().autoRenewal(ENABLED).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> wrapper = membershipModuleManagerBean.enableAutoRenewal(request);
        Assert.assertEquals(wrapper.getRequest(), request);
        MembershipDTO response = wrapper.getResponse();
        Assert.assertEquals(response.getId(), request.getId());
        Assert.assertEquals(response.getAutoRenewal(), ENABLED);
    }

    @Test
    public void testInsertRecurringId() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willReturn(true);
        MembershipDTO request = MembershipDTO.builder().id(TEST_MEMBERSHIP_ID).recurringId(NON_RECURRING).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> wrapper = membershipModuleManagerBean.saveRecurringId(request);
        Assert.assertEquals(wrapper.getRequest(), request);
        MembershipDTO response = wrapper.getResponse();
        Assert.assertEquals(response.getId(), request.getId());
        Assert.assertEquals(response.getRecurringId(), NON_RECURRING);
    }

    @Test
    public void testActivateMembership() {
        MembershipResponse activatedResponse = new MembershipResponse();
        activatedResponse.setId(TEST_MEMBERSHIP_ID);
        activatedResponse.setStatus(ACTIVATED);
        activatedResponse.setBalance(BigDecimal.ONE);
        BookingDTO bookingDTO = BookingDTO.builder().id(1L).build();
        given(membershipService.activateMembership(eq(TEST_MEMBERSHIP_ID), any(ActivationRequest.class))).willReturn(activatedResponse);
        MembershipDTO request = MembershipDTO.builder().id(TEST_MEMBERSHIP_ID).balance(BigDecimal.ONE).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> wrapper = membershipModuleManagerBean.activateMembership(request, bookingDTO);
        assertTrue(wrapper.isSuccessful());
        Assert.assertEquals(wrapper.getResponse().getId(), request.getId());
        assertNull(wrapper.getThrowable());
    }

    @Test
    public void testActivateMembershipFails() {
        MembershipDTO request = MembershipDTO.builder().id(TEST_MEMBERSHIP_ID).balance(BigDecimal.ONE).build();
        BookingDTO bookingDTO = BookingDTO.builder().id(1L).build();
        given(membershipService.activateMembership(eq(TEST_MEMBERSHIP_ID), any(ActivationRequest.class))).willThrow(MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        ApiCallWrapper<MembershipDTO, MembershipDTO> wrapper = membershipModuleManagerBean.activateMembership(request, bookingDTO);
        assertTrue(wrapper.isFailed());
        Assert.assertEquals(wrapper.getResponse().getId(), request.getId());
        assertEquals(MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION, wrapper.getThrowable());
    }

    private ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO> buildSubscriptionOfferWrapper(MembershipDTO request, ApiCall.Result result) {
        return new ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO>(ApiCall.Endpoint.GET_OFFER)
                .result(result)
                .response(MEMBERSHIP_OFFER_DTO)
                .request(request);
    }
}

