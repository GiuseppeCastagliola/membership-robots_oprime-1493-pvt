package com.odigeo.membership.robots.report;

import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.StringJoiner;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class ReporterImplTest {
    private static final String FAIL_MSG = "failed";
    private static final StringJoiner STRING_JOINER = new StringJoiner(StringUtils.EMPTY);
    private static final String SUCCESS_MSG = "success";
    private static final long REQUEST_MEMBERSHIP_ID = 111L;
    private static final long RESPONSE_MEMBERSHIP_ID = 222L;
    @Spy
    private ReporterImpl reportService;
    @Mock
    private Report report;
    @Mock
    private MembershipDTO requestMembershipDTO;
    @Mock
    private MembershipDTO responseMembershipDTO;

    private ApiCallWrapper<MembershipDTO, MembershipDTO> failMembershipWrapper;
    private ApiCallWrapper<MembershipDTO, MembershipDTO> successMembershipWrapper;
    private ApiCallWrapper<MembershipDTO, MembershipDTO> failProcessRemnantFeeWrapper;
    private ApiCallWrapper<MembershipDTO, MembershipDTO> successProcessRemnantFeeWrapper;
    private ApiCallWrapper<MembershipDTO, MembershipDTO> failNullResponseWrapper;
    private ApiCallWrapper<MembershipDTO, MembershipDTO> successNullResponseWrapper;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        when(report.getErrorJoiner()).thenReturn(STRING_JOINER);
        when(report.getSuccessJoiner()).thenReturn(STRING_JOINER);
        when(requestMembershipDTO.getId()).thenReturn(REQUEST_MEMBERSHIP_ID);
        when(responseMembershipDTO.getId()).thenReturn(RESPONSE_MEMBERSHIP_ID);
        failMembershipWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP, ApiCall.Result.FAIL)
                .request(requestMembershipDTO)
                .response(responseMembershipDTO)
                .message(FAIL_MSG)
                .build();
        successMembershipWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP, ApiCall.Result.SUCCESS)
                .request(requestMembershipDTO)
                .response(responseMembershipDTO)
                .message(SUCCESS_MSG)
                .build();
        failProcessRemnantFeeWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.PROCESS_REMNANT_FEE, ApiCall.Result.FAIL)
                .request(requestMembershipDTO)
                .response(responseMembershipDTO)
                .message(FAIL_MSG)
                .build();
        successProcessRemnantFeeWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.PROCESS_REMNANT_FEE, ApiCall.Result.SUCCESS)
                .request(requestMembershipDTO)
                .response(responseMembershipDTO)
                .message(SUCCESS_MSG)
                .build();
        failNullResponseWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE, ApiCall.Result.FAIL)
                .request(requestMembershipDTO)
                .message(FAIL_MSG)
                .build();
        successNullResponseWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE, ApiCall.Result.SUCCESS)
                .request(requestMembershipDTO)
                .message(SUCCESS_MSG)
                .build();
    }

    @Test
    public void testFeedReport() {
        reportService.feedReport(failMembershipWrapper, report);
        reportService.feedReport(failMembershipWrapper, report);
        reportService.feedReport(successMembershipWrapper, report);
        reportService.feedReport(failProcessRemnantFeeWrapper, report);
        reportService.feedReport(successProcessRemnantFeeWrapper, report);
        then(report).should(times(3)).getErrorJoiner();
        then(report).should(times(2)).getSuccessJoiner();
    }

    @Test
    public void testGenerateReport() {
        reportService.feedReport(failMembershipWrapper, report);
        reportService.feedReport(failMembershipWrapper, report);
        reportService.feedReport(successMembershipWrapper, report);
        reportService.feedReport(failProcessRemnantFeeWrapper, report);
        reportService.feedReport(successProcessRemnantFeeWrapper, report);
        reportService.generateReportLog(report);
        then(report).should(times(4)).getErrorJoiner();
        then(report).should(times(3)).getSuccessJoiner();
    }

    @Test
    public void testGenerateReportOnlySuccesses() {
        reportService.feedReport(successMembershipWrapper, report);
        reportService.feedReport(successMembershipWrapper, report);
        reportService.feedReport(successProcessRemnantFeeWrapper, report);
        reportService.generateReportLog(report);
        then(report).should().getErrorJoiner();
        then(report).should(times(4)).getSuccessJoiner();
    }

    @Test
    public void testGenerateReportOnlyFails() {
        reportService.feedReport(failMembershipWrapper, report);
        reportService.feedReport(failProcessRemnantFeeWrapper, report);
        reportService.generateReportLog(report);
        then(report).should(times(3)).getErrorJoiner();
        then(report).should().getSuccessJoiner();
    }

    @Test
    public void testNoNullPointerExceptionInLambdaWhenNullResponseMembershipDTO() {
        reportService.feedReport(failNullResponseWrapper, report);
        reportService.feedReport(successNullResponseWrapper, report);
        reportService.generateReportLog(report);
        then(report).should(times(2)).getErrorJoiner();
        then(report).should(times(2)).getSuccessJoiner();
    }
}
