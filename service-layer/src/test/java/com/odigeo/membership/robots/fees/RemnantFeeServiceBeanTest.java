package com.odigeo.membership.robots.fees;

import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipDTOBuilder;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.FeeContainerDTO;
import com.odigeo.membership.robots.dto.booking.FeeDTO;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.manager.bookingapi.FeeRequestBuilder;
import com.odigeo.membership.robots.manager.bookingapi.UpdateBookingRequestBuilder;
import com.odigeo.membership.robots.manager.membership.MembershipFeeContainerTypes;
import com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.odigeo.membership.robots.apicall.ApiCall.Result.FAIL;
import static com.odigeo.membership.robots.apicall.ApiCall.Result.SUCCESS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class RemnantFeeServiceBeanTest {

    private static final long MEMBERSHIP_ID1 = 789L;
    private static final String EXPIRED = "EXPIRED";
    private static final String FEE_CONTAINER_TYPE_BOOKING = "BOOKING";
    private static final String ADJUSTMENT = "ADJUSTMENT";
    private static final String CURRENCY = "EUR";
    private static final BigDecimal BALANCE = BigDecimal.TEN;
    private static final BigDecimal MEMBERSHIP_SUBSCRIPTION_AMOUNT = new BigDecimal("44.99");

    private static MembershipDTO MEMBERSHIP_DTO_ID1 = MembershipDTO.builder().id(MEMBERSHIP_ID1).status(EXPIRED).balance(BALANCE).build();

    private static final Map<Long, ApiCallWrapper<BookingDTO, MembershipDTO>> GET_SUBSCRIPTION_BOOKING_ANSWERS = new HashMap<>();
    private static final Map<Long, ApiCallWrapper<BookingDTO, UpdateBookingRequest>> PROCESS_PRIME_SUBSCRIPTION_BOOKING_FEES_ANSWERS = new HashMap<>();
    private static final Map<MembershipDTO, ApiCallWrapper<MembershipDTO, MembershipDTO>> CONSUME_MEMBERSHIP_REMNANT_BALANCE_ANSWERS = new HashMap<>();

    @Mock
    private ExternalModuleManager externalModuleManager;

    private RemnantFeeService remnantFeeService;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        remnantFeeService = new RemnantFeeServiceBean(externalModuleManager);
    }

    @AfterMethod
    public void tearDown() {
        GET_SUBSCRIPTION_BOOKING_ANSWERS.clear();
        PROCESS_PRIME_SUBSCRIPTION_BOOKING_FEES_ANSWERS.clear();
        CONSUME_MEMBERSHIP_REMNANT_BALANCE_ANSWERS.clear();
    }

    private ApiCallWrapper<BookingDTO, MembershipDTO> buildGetPrimeSubscriptionBookingResponse(MembershipDTO requestMembership, boolean successfulAnswers) {
        return buildGetPrimeSubscriptionBookingResponse(requestMembership, successfulAnswers, false);
    }

    private ApiCallWrapper<BookingDTO, MembershipDTO> buildGetPrimeSubscriptionBookingResponse(MembershipDTO requestMembership, boolean successfulAnswers, boolean withAC12Fee) {
        return buildGetBookingApiCallWrapper(successfulAnswers ? SUCCESS : FAIL,
                requestMembership,
                withAC12Fee ? buildBookingForMembershipWithAC12(requestMembership) : buildBookingForMembership(requestMembership));
    }

    private ApiCallWrapper<BookingDTO, MembershipDTO> buildGetBookingApiCallWrapper(ApiCall.Result callResult, MembershipDTO membershipDTO, BookingDTO bookingDTO) {
        return new ApiCallWrapperBuilder<BookingDTO, MembershipDTO>(ApiCall.Endpoint.GET_BOOKING)
                .result(callResult)
                .request(membershipDTO)
                .response(bookingDTO)
                .build();
    }

    private BookingDTO buildBookingForMembership(MembershipDTO membership) {
        MembershipFeeContainerTypes membershipFeeType = MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION;
        List<FeeDTO> fees = Collections.singletonList(FeeDTO.builder().subCode(membershipFeeType.getSubCode().name())
                .amount(MEMBERSHIP_SUBSCRIPTION_AMOUNT).build());
        return BookingDTO.builder()
                .id(membership.getId() * 10)
                .currencyCode(CURRENCY)
                .membershipId(membership.getId())
                .feeContainers(Collections.singletonList(buildFeeContainer(membershipFeeType.name(), fees)))
                .build();
    }

    private BookingDTO buildBookingForMembershipWithAC12(MembershipDTO membership) {
        final String subscriptionFeeSubCode = MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION.getSubCode().name();
        final List<FeeDTO> feeSubscription = buildSubscriptionFeeDtoList(subscriptionFeeSubCode, 1);
        final List<FeeDTO> membershipAdjustmentFees = buildAdjustmentFeeContainerDto(subscriptionFeeSubCode, BigDecimal.TEN);
        final List<FeeContainerDTO> feeContainers = Arrays.asList(
                buildFeeContainer(MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION.name(), feeSubscription),
                buildFeeContainer(FEE_CONTAINER_TYPE_BOOKING, membershipAdjustmentFees)
        );
        return buildBookingDto(membership, feeContainers);
    }

    private BookingDTO buildBookingDto(MembershipDTO membershipDTO, List<FeeContainerDTO> feeContainers) {
        return BookingDTO.builder()
                .id(membershipDTO.getId() * 10)
                .currencyCode(CURRENCY)
                .membershipId(membershipDTO.getId())
                .feeContainers(feeContainers)
                .build();
    }

    private List<FeeDTO> buildSubscriptionFeeDtoList(String subscriptionFeeSubCode, int numOfSubscriptionFees) {
        final FeeDTO feeDTO = FeeDTO.builder().subCode(subscriptionFeeSubCode).amount(MEMBERSHIP_SUBSCRIPTION_AMOUNT)
                .feeLabel(ADJUSTMENT).currency(CURRENCY).build();
        return numOfSubscriptionFees == 1 ? Collections.singletonList(feeDTO) : Arrays.asList(feeDTO, feeDTO);
    }

    private List<FeeDTO> buildAdjustmentFeeContainerDto(String subscriptionFeeSubCode, BigDecimal balance) {
        return Arrays.asList(
                FeeDTO.builder().subCode(MembershipFeeSubCodes.AC12.getFeeCode()).amount(balance).feeLabel(ADJUSTMENT).currency(CURRENCY).build(),
                FeeDTO.builder().subCode(subscriptionFeeSubCode).amount(balance.negate()).feeLabel(ADJUSTMENT).currency(CURRENCY).build()
        );
    }

    private FeeContainerDTO buildFeeContainer(String feeContainerType, List<FeeDTO> fees) {
        return FeeContainerDTO.builder().feeContainerType(feeContainerType)
                .fees(fees)
                .build();
    }

    private Answer<ApiCallWrapper<BookingDTO, UpdateBookingRequest>> answerPrimeSubscriptionBookingProcessFeesResponses(
            Collection<ApiCallWrapper<BookingDTO, MembershipDTO>> primeSubscriptionBookingWrappers, boolean successfulAnswers) {
        final ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> defaultFailResponse = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.UPDATE_BOOKING, FAIL);

        primeSubscriptionBookingWrappers.forEach(primeSubscriptionBookingWrapper -> {
            PROCESS_PRIME_SUBSCRIPTION_BOOKING_FEES_ANSWERS.put(primeSubscriptionBookingWrapper.getResponse().getId(),
                    buildPrimeSubscriptionBookingProcessFeesResponse(primeSubscriptionBookingWrapper, successfulAnswers));
        });

        return (InvocationOnMock invocation) -> {
            final long bookingId = (Long) invocation.getArguments()[0];
            return PROCESS_PRIME_SUBSCRIPTION_BOOKING_FEES_ANSWERS.getOrDefault(bookingId, defaultFailResponse.build());
        };
    }

    private ApiCallWrapper<BookingDTO, UpdateBookingRequest> buildPrimeSubscriptionBookingProcessFeesResponse(
            ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper, boolean successfulAnswers) {
        final MembershipDTO expiredMembership = primeSubscriptionBookingWrapper.getRequest();
        final BookingDTO primeSubscriptionBooking = primeSubscriptionBookingWrapper.getResponse();
        final BigDecimal balance = expiredMembership.getBalance();
        final String currencyCode = primeSubscriptionBooking.getCurrencyCode();
        final MembershipFeeSubCodes feeSubCode = MembershipFeeSubCodes.valueOf(primeSubscriptionBooking.getFeeContainers().get(0).getFeeDTOs().get(0).getSubCode());

        FeeRequest negatedFee = new FeeRequestBuilder().withAmount(balance.negate()).withCurrency(currencyCode).withSubCode(feeSubCode).build();
        FeeRequest remnantFee = new FeeRequestBuilder().withAmount(balance).withCurrency(currencyCode).withSubCode(MembershipFeeSubCodes.AC12).build();
        UpdateBookingRequest updateBookingRequest = new UpdateBookingRequestBuilder().withFeeRequests(Arrays.asList(negatedFee, remnantFee)).build();

        FeeContainerDTO updatedBookingFeeContainer = buildFeeContainer(FEE_CONTAINER_TYPE_BOOKING, buildAdjustmentFeeContainerDto(feeSubCode.name(), balance));

        List<FeeContainerDTO> updatedFeeContainers = new ArrayList<>(primeSubscriptionBooking.getFeeContainers());
        updatedFeeContainers.add(updatedBookingFeeContainer);

        BookingDTO updatedBooking = BookingDTO.builder()
                .id(primeSubscriptionBooking.getId())
                .membershipId(primeSubscriptionBooking.getMembershipId())
                .currencyCode(primeSubscriptionBooking.getCurrencyCode())
                .feeContainers(updatedFeeContainers)
                .build();

        return new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(ApiCall.Endpoint.UPDATE_BOOKING)
                .result(successfulAnswers ? SUCCESS : FAIL)
                .request(updateBookingRequest)
                .response(updatedBooking)
                .build();
    }

    private void fillSubscriptionBookingAnswersMap(List<MembershipDTO> membershipsToProcess) {
        membershipsToProcess.forEach(membershipToExpire -> GET_SUBSCRIPTION_BOOKING_ANSWERS.put(membershipToExpire.getId(),
                buildGetPrimeSubscriptionBookingResponse(membershipToExpire, true)));
    }

    @Test
    public void testProcessPrimeSubscriptionBookingFees_HappyPath() {
        //GIVEN
        ApiCallWrapper<BookingDTO, MembershipDTO> subscriptionBookingWrapper = buildGetPrimeSubscriptionBookingResponse(MEMBERSHIP_DTO_ID1, true);

        List<MembershipDTO> membershipsToProcess = Collections.singletonList(MEMBERSHIP_DTO_ID1);
        fillSubscriptionBookingAnswersMap(membershipsToProcess);

        given(externalModuleManager.updateBooking(anyLong(), any())).will(answerPrimeSubscriptionBookingProcessFeesResponses(GET_SUBSCRIPTION_BOOKING_ANSWERS.values(), true));

        //WHEN
        final ApiCallWrapper<BookingDTO, MembershipDTO> processBookingFeesApiCallWrapper = remnantFeeService.processPrimeSubscriptionBookingFees(subscriptionBookingWrapper);

        //THEN
        assertTrue(processBookingFeesApiCallWrapper.isSuccessful());
        assertNotNull(processBookingFeesApiCallWrapper.getResponse());
        verify(externalModuleManager).updateBooking(anyLong(), any());
    }

    @Test
    public void testProcessPrimeSubscriptionBookingFees_SubscriptionBookingNotPresent() {
        //GIVEN
        ApiCallWrapper<BookingDTO, MembershipDTO> noSubscriptionBookingWrapper = new ApiCallWrapperBuilder<BookingDTO, MembershipDTO>(ApiCall.Endpoint.GET_BOOKING)
                .request(MEMBERSHIP_DTO_ID1)
                .build();

        //WHEN
        final ApiCallWrapper<BookingDTO, MembershipDTO> processBookingFeesApiCallWrapper = remnantFeeService.processPrimeSubscriptionBookingFees(noSubscriptionBookingWrapper);

        //THEN
        assertFalse(processBookingFeesApiCallWrapper.isSuccessful());
        final String expectedErrorMessage = String.format(RemnantFeeServiceBean.NO_PRIME_BOOKINGS_EXCEPTION, MEMBERSHIP_DTO_ID1.getId());
        assertEquals(processBookingFeesApiCallWrapper.getMessage(), expectedErrorMessage);
        verify(externalModuleManager, never()).updateBooking(anyLong(), any());
    }

    @Test
    public void testProcessPrimeSubscriptionBookingFees_AC12PresentResultSuccess() {
        //GIVEN
        ApiCallWrapper<BookingDTO, MembershipDTO> subscriptionBookingWithAC12Wrapper = buildGetPrimeSubscriptionBookingResponse(MEMBERSHIP_DTO_ID1, true, true);

        //WHEN
        final ApiCallWrapper<BookingDTO, MembershipDTO> processBookingFeesApiCallWrapper = remnantFeeService.processPrimeSubscriptionBookingFees(subscriptionBookingWithAC12Wrapper);

        //THEN
        assertTrue(processBookingFeesApiCallWrapper.isSuccessful());
        assertNull(processBookingFeesApiCallWrapper.getResponse());
        final String expectedErrorMessage = String.format(RemnantFeeServiceBean.AC12_FEE_ALREADY_PRESENT_IN_BOOKING, subscriptionBookingWithAC12Wrapper.getResponse().getId());
        assertEquals(processBookingFeesApiCallWrapper.getMessage(), expectedErrorMessage);
        verify(externalModuleManager, never()).updateBooking(anyLong(), any());
    }

    @Test
    public void testProcessPrimeSubscriptionBookingFees_MultipleSubscriptionFeeResultFail() {
        //GIVEN
        final String subscriptionFeeSubCode = MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION.getSubCode().name();
        final List<FeeDTO> subscriptionFeeDtoList = buildSubscriptionFeeDtoList(subscriptionFeeSubCode, 2);
        final List<FeeContainerDTO> feeContainers = Collections.singletonList(
                buildFeeContainer(MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION.name(), subscriptionFeeDtoList)
        );
        final BookingDTO bookingDTO = buildBookingDto(MEMBERSHIP_DTO_ID1, feeContainers);

        ApiCallWrapper<BookingDTO, MembershipDTO> multipleFeesBookingWrapper = buildGetBookingApiCallWrapper(SUCCESS, MEMBERSHIP_DTO_ID1, bookingDTO);

        //WHEN
        final ApiCallWrapper<BookingDTO, MembershipDTO> responseBookingMultipleFeesApiCallWrapper = remnantFeeService.processPrimeSubscriptionBookingFees(multipleFeesBookingWrapper);

        //THEN
        assertFalse(responseBookingMultipleFeesApiCallWrapper.isSuccessful());
        final String expectedErrorMessage = String.format(RemnantFeeServiceBean.MULTIPLE_FEES_EXCEPTION, MEMBERSHIP_DTO_ID1.getId(), bookingDTO.getId(), feeContainers.get(0).getId());
        assertEquals(responseBookingMultipleFeesApiCallWrapper.getMessage(), expectedErrorMessage);
        verify(externalModuleManager, never()).updateBooking(anyLong(), any());
    }

    @Test
    public void testProcessPrimeSubscriptionBookingFees_NoContainerExceptionsFails() {
        //GIVEN
        final BookingDTO bookingDTO = buildBookingDto(MEMBERSHIP_DTO_ID1, Collections.emptyList());
        ApiCallWrapper<BookingDTO, MembershipDTO> noFeesBookingWrapper = buildGetBookingApiCallWrapper(SUCCESS, MEMBERSHIP_DTO_ID1, bookingDTO);

        //WHEN
        final ApiCallWrapper<BookingDTO, MembershipDTO> responseBookingMultipleFeesApiCallWrapper = remnantFeeService.processPrimeSubscriptionBookingFees(noFeesBookingWrapper);

        //THEN
        assertFalse(responseBookingMultipleFeesApiCallWrapper.isSuccessful());
        final String expectedErrorMessage = String.format(RemnantFeeServiceBean.NO_MEMBERSHIP_PAYMENTS_EXCEPTION, MEMBERSHIP_DTO_ID1.getId(), bookingDTO.getId());
        assertEquals(responseBookingMultipleFeesApiCallWrapper.getMessage(), expectedErrorMessage);
        verify(externalModuleManager, never()).updateBooking(anyLong(), any());
    }

    @Test
    public void testProcessPrimeSubscriptionBookingFees_NoFeesExceptionsFails() {
        //GIVEN
        final List<FeeDTO> feeSubscription = Collections.emptyList();
        final List<FeeContainerDTO> feeContainers = Collections.singletonList(
                buildFeeContainer(MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION.name(), feeSubscription)
        );
        final BookingDTO bookingDTO = buildBookingDto(MEMBERSHIP_DTO_ID1, feeContainers);

        ApiCallWrapper<BookingDTO, MembershipDTO> noFeeContainerBookingWrapper = buildGetBookingApiCallWrapper(SUCCESS, MEMBERSHIP_DTO_ID1, bookingDTO);

        //WHEN
        final ApiCallWrapper<BookingDTO, MembershipDTO> responseBookingNoFeesApiCallWrapper = remnantFeeService.processPrimeSubscriptionBookingFees(noFeeContainerBookingWrapper);

        //THEN
        assertFalse(responseBookingNoFeesApiCallWrapper.isSuccessful());
        final String expectedErrorMessage = String.format(RemnantFeeServiceBean.NO_FEES_EXCEPTION, MEMBERSHIP_DTO_ID1.getId(), bookingDTO.getId(), feeContainers.get(0).getId());
        assertEquals(responseBookingNoFeesApiCallWrapper.getMessage(), expectedErrorMessage);
        verify(externalModuleManager, never()).updateBooking(anyLong(), any());
    }

    @Test
    public void testProcessPrimeSubscriptionBookingFees_UpdateBookingFails() {
        //GIVEN
        ApiCallWrapper<BookingDTO, MembershipDTO> subscriptionBookingWrapper = buildGetPrimeSubscriptionBookingResponse(MEMBERSHIP_DTO_ID1, true);

        List<MembershipDTO> membershipsToProcess = Collections.singletonList(MEMBERSHIP_DTO_ID1);
        fillSubscriptionBookingAnswersMap(membershipsToProcess);

        given(externalModuleManager.updateBooking(anyLong(), any())).will(answerPrimeSubscriptionBookingProcessFeesResponses(GET_SUBSCRIPTION_BOOKING_ANSWERS.values(), false));

        //WHEN
        final ApiCallWrapper<BookingDTO, MembershipDTO> processBookingFeesApiCallWrapperFail = remnantFeeService.processPrimeSubscriptionBookingFees(subscriptionBookingWrapper);

        //THEN
        assertFalse(processBookingFeesApiCallWrapperFail.isSuccessful());
        verify(externalModuleManager).updateBooking(anyLong(), any());
    }

    @Test
    public void testProcessPrimeSubscriptionBookingFees_NoFeeContainerType() {
        //GIVEN
        final List<FeeDTO> feeSubscription = Collections.emptyList();
        final List<FeeContainerDTO> feeContainers = Collections.singletonList(
                buildFeeContainer(StringUtils.EMPTY, feeSubscription)
        );
        final BookingDTO bookingDTO = buildBookingDto(MEMBERSHIP_DTO_ID1, feeContainers);

        ApiCallWrapper<BookingDTO, MembershipDTO> noFeeContainerTypeBookingWrapper = buildGetBookingApiCallWrapper(SUCCESS, MEMBERSHIP_DTO_ID1, bookingDTO);

        //WHEN
        final ApiCallWrapper<BookingDTO, MembershipDTO> processBookingFeesApiCallWrapperFail = remnantFeeService.processPrimeSubscriptionBookingFees(noFeeContainerTypeBookingWrapper);

        //THEN
        assertFalse(processBookingFeesApiCallWrapperFail.isSuccessful());
        final String expectedErrorMessage = String.format(RemnantFeeServiceBean.NO_MEMBERSHIP_PAYMENTS_EXCEPTION, MEMBERSHIP_DTO_ID1.getId(), bookingDTO.getId());
        assertEquals(processBookingFeesApiCallWrapperFail.getMessage(), expectedErrorMessage);
        verify(externalModuleManager, never()).updateBooking(anyLong(), any());
    }

    @Test
    public void testProcessPrimeSubscriptionBookingFees_MultipleContainerExceptionsFails() {
        //GIVEN
        final String subscriptionFeeSubCode = MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION.getSubCode().name();
        final List<FeeDTO> subscriptionFeeDtoList = buildSubscriptionFeeDtoList(subscriptionFeeSubCode, 1);
        final List<FeeContainerDTO> feeContainers = Arrays.asList(
                buildFeeContainer(MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION.name(), subscriptionFeeDtoList),
                buildFeeContainer(MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION.name(), Collections.emptyList())
        );

        final BookingDTO bookingDTO = buildBookingDto(MEMBERSHIP_DTO_ID1, feeContainers);

        ApiCallWrapper<BookingDTO, MembershipDTO> multipleContainerBookingWrapper = buildGetBookingApiCallWrapper(SUCCESS, MEMBERSHIP_DTO_ID1, bookingDTO);

        //WHEN
        final ApiCallWrapper<BookingDTO, MembershipDTO> processBookingFeesApiCallWrapperFail = remnantFeeService.processPrimeSubscriptionBookingFees(multipleContainerBookingWrapper);

        //THEN
        assertFalse(processBookingFeesApiCallWrapperFail.isSuccessful());
        verify(externalModuleManager, never()).updateBooking(anyLong(), any());
    }

    @Test
    public void testConsumeMembershipRemnantBalance_HappyPath() {
        //GIVEN
        ApiCallWrapper<BookingDTO, MembershipDTO> subscriptionBookingWrapper = buildGetPrimeSubscriptionBookingResponse(MEMBERSHIP_DTO_ID1, true);
        given(externalModuleManager.consumeRemnantMembershipBalance(any(MembershipDTO.class)))
                .will(answerConsumeRemnantMembershipBalance(Collections.singletonList(MEMBERSHIP_DTO_ID1), true));

        //WHEN
        final ApiCallWrapper<MembershipDTO, MembershipDTO> consumeBalanceApiCallWrapper = remnantFeeService.consumeMembershipRemnantBalance(subscriptionBookingWrapper);

        //THEN
        assertTrue(consumeBalanceApiCallWrapper.isSuccessful());
        verify(externalModuleManager).consumeRemnantMembershipBalance(MEMBERSHIP_DTO_ID1);
    }

    @Test
    public void testConsumeMembershipRemnantBalance_PreviousCallFailed() {
        //GIVEN
        ApiCallWrapper<BookingDTO, MembershipDTO> subscriptionBookingWrapper = buildGetPrimeSubscriptionBookingResponse(MEMBERSHIP_DTO_ID1, false);

        //WHEN
        final ApiCallWrapper<MembershipDTO, MembershipDTO> consumeBalanceApiCallWrapper = remnantFeeService.consumeMembershipRemnantBalance(subscriptionBookingWrapper);

        //THEN
        assertFalse(consumeBalanceApiCallWrapper.isSuccessful());
        verify(externalModuleManager, never()).consumeRemnantMembershipBalance(MEMBERSHIP_DTO_ID1);
    }

    @Test
    public void testConsumeMembershipRemnantBalance_PreviousCallSuccessNullBookingResponse() {
        //GIVEN
        ApiCallWrapper<BookingDTO, MembershipDTO> noBookingResponseWrapper = new ApiCallWrapperBuilder<BookingDTO, MembershipDTO>(ApiCall.Endpoint.BOOKING_PROCESS_REMNANT_FEE)
                .result(SUCCESS)
                .request(MEMBERSHIP_DTO_ID1)
                .build();
        given(externalModuleManager.consumeRemnantMembershipBalance(any(MembershipDTO.class)))
                .will(answerConsumeRemnantMembershipBalance(Collections.singletonList(MEMBERSHIP_DTO_ID1), true));

        //WHEN
        final ApiCallWrapper<MembershipDTO, MembershipDTO> consumeBalanceApiCallWrapper = remnantFeeService.consumeMembershipRemnantBalance(noBookingResponseWrapper);

        //THEN
        assertTrue(consumeBalanceApiCallWrapper.isSuccessful());
        verify(externalModuleManager).consumeRemnantMembershipBalance(MEMBERSHIP_DTO_ID1);
    }

    private Answer<ApiCallWrapper<MembershipDTO, MembershipDTO>> answerConsumeRemnantMembershipBalance(List<MembershipDTO> membershipBalancesToConsume, boolean successfulAnswers) {
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> defaultFailedResponse = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE, FAIL);

        membershipBalancesToConsume.forEach(
                membershipBalanceToConsume -> CONSUME_MEMBERSHIP_REMNANT_BALANCE_ANSWERS.put(membershipBalanceToConsume,
                        buildConsumeMembershipBalanceResponseWrapper(membershipBalanceToConsume, successfulAnswers)));

        return (InvocationOnMock invocation) -> {
            final MembershipDTO requestDto = (MembershipDTO) invocation.getArguments()[0];
            return CONSUME_MEMBERSHIP_REMNANT_BALANCE_ANSWERS.getOrDefault(requestDto, defaultFailedResponse.request(requestDto).build());
        };
    }

    private ApiCallWrapper<MembershipDTO, MembershipDTO> buildConsumeMembershipBalanceResponseWrapper(MembershipDTO expiredMembership, boolean successfulAnswers) {
        return new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE)
                .result(successfulAnswers ? SUCCESS : FAIL)
                .request(expiredMembership)
                .response(MembershipDTOBuilder.builderFromDto(expiredMembership)
                        .balance(BigDecimal.ZERO)
                        .build())
                .build();
    }
}