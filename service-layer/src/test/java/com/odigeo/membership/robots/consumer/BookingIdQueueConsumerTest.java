package com.odigeo.membership.robots.consumer;

import com.odigeo.membership.robots.configuration.ConsumerRetriesConfig;
import com.odigeo.membership.robots.exceptions.consumer.BookingNotFoundException;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import net.jodah.failsafe.RetryPolicy;
import org.mockito.Mock;
import org.mockito.internal.stubbing.answers.AnswersWithDelay;
import org.mockito.internal.stubbing.answers.Returns;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingIdQueueConsumerTest {

    private static final Long BOOKING_ID = 1L;
    private static final Long SECOND_BOOKING_ID = 2L;
    private static final Long THIRD_BOOKING_ID = 3L;

    @Mock
    Processor bookingProcessor;
    @Mock
    private BlockingQueue<Long> bookingIdQueue;
    @Mock
    private MembershipRobotsMetrics membershipRobotsMetrics;

    private ConsumerRetriesConfig retriesConfig;

    private BookingIdQueueConsumer bookingIdQueueConsumer;


    @BeforeMethod
    public void setUp() {
        openMocks(this);
        retriesConfig = new ConsumerRetriesConfig().defaultValues();
        bookingIdQueueConsumer = new BookingIdQueueConsumer(bookingIdQueue, bookingProcessor, membershipRobotsMetrics, retriesConfig);
    }

    @Test
    public void testRun() throws InterruptedException {
        given(bookingIdQueue.poll(anyLong(), any(TimeUnit.class))).willReturn(BOOKING_ID).willReturn(null).willThrow(new InterruptedException());
        bookingIdQueueConsumer.run();
        then(bookingIdQueue).should(times(3)).poll(anyLong(), any(TimeUnit.class));
        assertFalse(bookingIdQueueConsumer.interrupt.get());
    }

    @Test
    public void testStop() throws InterruptedException {
        given(bookingIdQueue.poll(anyLong(), any(TimeUnit.class))).willReturn(BOOKING_ID).willReturn(null).willThrow(new InterruptedException());
        bookingIdQueueConsumer.stop();
        assertTrue(bookingIdQueueConsumer.interrupt.get());
        bookingIdQueueConsumer.run();
        then(bookingIdQueue).should(never()).poll(anyLong(), any(TimeUnit.class));
        assertFalse(bookingIdQueueConsumer.interrupt.get());
    }

    @Test
    public void testRetriesOnRetryableExceptions() throws InterruptedException {
        setupRetryPolicies();
        given(bookingIdQueue.poll(anyLong(), any(TimeUnit.class)))
                .willReturn(BOOKING_ID)
                .willAnswer(new AnswersWithDelay(200, new Returns(SECOND_BOOKING_ID)))
                .willAnswer(new AnswersWithDelay(200, new Returns(THIRD_BOOKING_ID)))
                .willThrow(new InterruptedException());
        doThrow(new RetryableException(new Exception())).when(bookingProcessor).process(eq(BOOKING_ID));
        doThrow(new BookingNotFoundException(new Exception())).when(bookingProcessor).process(eq(SECOND_BOOKING_ID));
        bookingIdQueueConsumer.run();
        then(bookingIdQueue).should(times(4)).poll(anyLong(), any(TimeUnit.class));
        then(membershipRobotsMetrics).should(times(2)).incrementCounter(eq(MetricsName.BOOKING_PROCESSING_FAILED), any(), any());
        then(membershipRobotsMetrics).should(times(2)).incrementCounter(eq(MetricsName.BOOKING_NOT_FOUND_FAIL), any(), any());
        assertFalse(bookingIdQueueConsumer.interrupt.get());
    }

    private void setupRetryPolicies() {
        bookingIdQueueConsumer.setRetryPolicy(new RetryPolicy<>()
                .handle(RetryableException.class)
                .withDelay(Duration.ofMillis(100))
                .withMaxRetries(1));
        bookingIdQueueConsumer.setBookingNotFoundRetryPolicy(new RetryPolicy<>()
                .handle(BookingNotFoundException.class)
                .withDelay(Duration.ofMillis(120))
                .withMaxRetries(1));
    }

    @AfterMethod
    private void clean() {
        bookingIdQueueConsumer.stop();
    }
}

