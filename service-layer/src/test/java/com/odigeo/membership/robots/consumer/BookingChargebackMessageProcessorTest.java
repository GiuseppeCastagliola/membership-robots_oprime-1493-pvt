package com.odigeo.membership.robots.consumer;

import com.odigeo.commons.messaging.BasicMessage;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.mapper.response.consumer.DeactivateMembershipByChargebackMapper;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.BDDMockito.then;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.MockitoAnnotations.openMocks;

public class BookingChargebackMessageProcessorTest {

    @Mock
    DeactivateMembershipByChargebackMessageConsumer deactivateMembershipByChargebackMessageConsumer;
    @Mock
    MembershipRobotsMetrics membershipRobotsMetrics;
    @Spy
    DeactivateMembershipByChargebackMapper deactivateMembershipByChargebackMapper = Mappers.getMapper(DeactivateMembershipByChargebackMapper.class);
    BookingChargebackMessageProcessor bookingChargebackMessageProcessor;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        bookingChargebackMessageProcessor = new BookingChargebackMessageProcessor(deactivateMembershipByChargebackMessageConsumer, deactivateMembershipByChargebackMapper, membershipRobotsMetrics);
    }

    @Test
    public void messageIsNull() throws RetryableException {
        bookingChargebackMessageProcessor.onMessage(null);
        then(deactivateMembershipByChargebackMessageConsumer).should(never()).deactivateMembershipByChargeback(any(CollectionSummaryChargebackNotifications.class));
        then(membershipRobotsMetrics).should(never()).incrementCounter(any());
    }

    @Test
    public void messageIsEmpty() throws RetryableException {
        bookingChargebackMessageProcessor.onMessage(new BasicMessage());
        then(deactivateMembershipByChargebackMessageConsumer).should(never()).deactivateMembershipByChargeback(any(CollectionSummaryChargebackNotifications.class));
        then(membershipRobotsMetrics).should(never()).incrementCounter(any());
    }

    @Test
    public void messageIsValid() {
        BasicMessage basicMessage = new BasicMessage();
        basicMessage.addExtraParameter("bookingId", "123456");
        basicMessage.addExtraParameter("action", "CHARGEBACK");
        basicMessage.addExtraParameter("status", "CHARGEBACKED");
        doNothing().when(deactivateMembershipByChargebackMessageConsumer).deactivateMembershipByChargeback(any(CollectionSummaryChargebackNotifications.class));
        bookingChargebackMessageProcessor.onMessage(basicMessage);
        then(deactivateMembershipByChargebackMapper).should().basicMessageToCollectionSummaryChargebackNotifications(eq(basicMessage));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_PROCESSED_CHARGEBACK));
    }

    @Test
    public void messageWithStatusNotChargebacked() {
        BasicMessage basicMessage = new BasicMessage();
        basicMessage.addExtraParameter("bookingId", "123456");
        basicMessage.addExtraParameter("action", "CHARGEBACK");
        basicMessage.addExtraParameter("status", "CHARGEBACK_CANCELLED");
        doNothing().when(deactivateMembershipByChargebackMessageConsumer).deactivateMembershipByChargeback(any(CollectionSummaryChargebackNotifications.class));
        bookingChargebackMessageProcessor.onMessage(basicMessage);
        then(deactivateMembershipByChargebackMapper).should().basicMessageToCollectionSummaryChargebackNotifications(eq(basicMessage));
        then(deactivateMembershipByChargebackMessageConsumer).should(never()).deactivateMembershipByChargeback(any(CollectionSummaryChargebackNotifications.class));
        then(membershipRobotsMetrics).should(never()).incrementCounter(any());
    }
}
