package com.odigeo.membership.robots.criteria;

import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.BookingStatus;
import com.odigeo.membership.robots.dto.booking.CollectionAttemptDTO;
import com.odigeo.membership.robots.dto.booking.FeeContainerDTO;
import com.odigeo.membership.robots.dto.booking.ProductCategoryBookingDTO;
import com.odigeo.membership.robots.manager.bookingapi.product.ProductMembershipPrimeBookingItemType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingPredicateProviderTest extends PredicateHelper<BookingDTO> {
    private static final String SUBSCRIPTION = ProductMembershipPrimeBookingItemType.MEMBERSHIP_SUBSCRIPTION.name();
    private static final String RENEWAL = ProductMembershipPrimeBookingItemType.MEMBERSHIP_RENEWAL.name();
    private static final String CONTRACT = BookingStatus.CONTRACT.name();
    private static final String REQUEST = BookingStatus.REQUEST.name();
    private static final String INIT = BookingStatus.INIT.name();
    private static final String MEMBERSHIP_PERKS_CONTAINER_TYPE = "MEMBERSHIP_PERKS";
    private static final long MEMBERSHIP_ID = 1L;

    private List<ProductCategoryBookingDTO> bookingProducts;
    private List<CollectionAttemptDTO> collectionAttempts;
    private List<FeeContainerDTO> feeContainers;
    @Mock
    private BookingDTO bookingDTO;
    @Mock
    private ProductCategoryBookingDTO productCategoryBookingDTO;
    @Mock
    private FeeContainerDTO feeContainerDTO;
    @Mock
    private CollectionAttemptPredicateProvider collectionAttemptPredicateProvider;
    @Mock
    private BookingPredicateProvider bookingPredicateProviderMock;
    @InjectMocks
    private BookingPredicateProvider bookingPredicateProvider;


    @BeforeMethod
    public void setUp() {
        openMocks(this);
        bookingProducts = Collections.singletonList(productCategoryBookingDTO);
        collectionAttempts = Collections.singletonList(CollectionAttemptDTO.builder().build());
        feeContainers = Collections.singletonList(feeContainerDTO);
        given(bookingPredicateProviderMock.eligibleForRecurring()).willCallRealMethod();
        given(bookingPredicateProviderMock.eligibleForTracking()).willCallRealMethod();
        given(bookingPredicateProviderMock.eligibleForTrackingUpdate()).willCallRealMethod();
        given(bookingPredicateProviderMock.eligibleForActivation()).willCallRealMethod();
        given(bookingPredicateProviderMock.eligibleForBasicFreeUpdate()).willCallRealMethod();
    }

    @Test
    public void testIsContract() {
        given(bookingDTO.getBookingStatus()).willReturn(CONTRACT, null, INIT);
        predicateMultipleTest(bookingPredicateProvider.isContract(), bookingDTO, true, false, false);
    }

    @Test
    public void testIsRequest() {
        given(bookingDTO.getBookingStatus()).willReturn(REQUEST, null, INIT, StringUtils.EMPTY);
        predicateMultipleTest(bookingPredicateProvider.isRequest(), bookingDTO, true, false, false, false);
    }

    @Test
    public void testHaveValidStatusForRecurring() {
        given(bookingDTO.getBookingStatus()).willReturn(REQUEST, REQUEST, CONTRACT, null, null, StringUtils.EMPTY, StringUtils.EMPTY);
        predicateMultipleTest(bookingPredicateProvider.hasValidStatusForRecurring(), bookingDTO, true, true, false, false);
    }

    @Test
    public void testHaveValidMovementForRecurring() {
        PredicateHelper<CollectionAttemptDTO> attemptDTOPredicateTest = new PredicateHelper<>();
        Predicate<CollectionAttemptDTO> truePredicate = attemptDTOPredicateTest.truePredicate();
        Predicate<CollectionAttemptDTO> falsePredicate = attemptDTOPredicateTest.falsePredicate();
        given(bookingDTO.getCollectionAttempts()).willReturn(collectionAttempts, collectionAttempts, collectionAttempts, Collections.emptyList());
        given(collectionAttemptPredicateProvider.hasRecurringId()).willReturn(truePredicate, falsePredicate, truePredicate);
        given(collectionAttemptPredicateProvider.isLastMovementStatusAndActionValid()).willReturn(truePredicate, falsePredicate);
        predicateMultipleTest(bookingPredicateProvider.hasValidMovementForRecurring(), bookingDTO, true, false, false, false);
    }

    @Test
    public void testIsBookingWithSubscription() {
        given(productCategoryBookingDTO.getProductType()).willReturn(SUBSCRIPTION, "ITINERARY");
        given(bookingDTO.getBookingProducts()).willReturn(bookingProducts, bookingProducts, Collections.emptyList());
        predicateMultipleTest(bookingPredicateProvider.isBookingWithSubscription(), bookingDTO, true, false, false);
    }

    @Test
    public void testIsBookingWithRenewalOrSubscription() {
        given(productCategoryBookingDTO.getProductType()).willReturn(SUBSCRIPTION, RENEWAL, "ITINERARY");
        given(bookingDTO.getBookingProducts()).willReturn(bookingProducts, bookingProducts, bookingProducts, Collections.emptyList());
        predicateMultipleTest(bookingPredicateProvider.isBookingWithRenewalOrSubscription(), bookingDTO, true, true, false, false);
    }

    @Test
    public void testArePerksPresent() {
        given(feeContainerDTO.getFeeContainerType()).willReturn(MEMBERSHIP_PERKS_CONTAINER_TYPE, "OTHER_CONTAINER");
        given(bookingDTO.getFeeContainers()).willReturn(feeContainers, feeContainers, Collections.emptyList());
        predicateMultipleTest(bookingPredicateProvider.arePerksPresent(), bookingDTO, true, false, false);
    }

    @Test
    public void testIsPrimeBooking() {
        given(bookingDTO.getMembershipId()).willReturn(MEMBERSHIP_ID, NumberUtils.LONG_ZERO, null);
        predicateMultipleTest(bookingPredicateProvider.isPrimeBooking(), bookingDTO, true, false, false);
    }

    @Test
    public void testIsTestBooking() {
        given(bookingDTO.isTestBooking()).willReturn(false, true);
        predicateMultipleTest(bookingPredicateProvider.isTestBooking(), bookingDTO, false, true);
    }

    @Test
    public void testIsNotTestAndIsPrime() {
        given(bookingDTO.isTestBooking()).willReturn(true, false, false, false);
        given(bookingDTO.getMembershipId()).willReturn(MEMBERSHIP_ID, NumberUtils.LONG_ZERO, null);
        predicateMultipleTest(bookingPredicateProvider.isNotTestAndIsPrime(), bookingDTO, false, true, false, false);
    }

    @Test
    public void testEligibleForTracking() {
        configureEligibleTrackingMockPredicates(true, true);
        assertTrue(bookingPredicateProviderMock.eligibleForTracking().test(bookingDTO));
    }

    @Test
    public void testEligibleForTrackingNotContract() {
        configureEligibleTrackingMockPredicates(false, true);
        assertFalse(bookingPredicateProviderMock.eligibleForTracking().test(bookingDTO));
    }

    @Test
    public void testEligibleForTrackingWithoutPerks() {
        configureEligibleTrackingMockPredicates(true, false);
        assertFalse(bookingPredicateProviderMock.eligibleForTracking().test(bookingDTO));
    }

    @Test
    public void testEligibleForTrackingUpdate() {
        configureEligibleTrackingMockPredicates(false, true);
        assertTrue(bookingPredicateProviderMock.eligibleForTrackingUpdate().test(bookingDTO));
    }

    @Test
    public void testEligibleForTrackingUpdateInContract() {
        configureEligibleTrackingMockPredicates(true, true);
        assertFalse(bookingPredicateProviderMock.eligibleForTrackingUpdate().test(bookingDTO));
    }

    @Test
    public void testEligibleForTrackingUpdateWithoutPerks() {
        configureEligibleTrackingMockPredicates(false, false);
        assertFalse(bookingPredicateProviderMock.eligibleForTrackingUpdate().test(bookingDTO));
    }

    @Test
    public void testEligibleForActivation() {
        configureEligibleActivationMockPredicates(true, true);
        assertTrue(bookingPredicateProviderMock.eligibleForActivation().test(bookingDTO));
    }

    @Test
    public void testEligibleForActivationNotInContract() {
        configureEligibleActivationMockPredicates(false, true);
        assertFalse(bookingPredicateProviderMock.eligibleForActivation().test(bookingDTO));
    }

    @Test
    public void testEligibleForActivationWithoutSubscription() {
        configureEligibleActivationMockPredicates(true, false);
        assertFalse(bookingPredicateProviderMock.eligibleForActivation().test(bookingDTO));
    }

    @Test
    public void testEligibleForRecurring() {
        configureEligibleRecurringMockPredicates(true, true, true);
        assertTrue(bookingPredicateProviderMock.eligibleForRecurring().test(bookingDTO));
    }

    @Test
    public void testEligibleForRecurringNotValidMovement() {
        configureEligibleRecurringMockPredicates(true, false, true);
        assertFalse(bookingPredicateProviderMock.eligibleForRecurring().test(bookingDTO));
    }

    @Test
    public void testEligibleForRecurringNotValidStatus() {
        configureEligibleRecurringMockPredicates(false, true, true);
        assertFalse(bookingPredicateProviderMock.eligibleForRecurring().test(bookingDTO));
    }

    @Test
    public void testEligibleForRecurringWithoutPrimeProducts() {
        configureEligibleRecurringMockPredicates(true, true, false);
        assertFalse(bookingPredicateProviderMock.eligibleForRecurring().test(bookingDTO));
    }

    @Test
    public void testEligibleForBasicFreeUpdateSubscriptionBooking() {
        configureEligibleBasicFreeMocks(true, true);
        assertFalse(bookingPredicateProviderMock.eligibleForBasicFreeUpdate().test(bookingDTO));
    }

    @Test
    public void testEligibleForBasicFreeUpdate() {
        configureEligibleBasicFreeMocks(false, true);
        assertTrue(bookingPredicateProviderMock.eligibleForBasicFreeUpdate().test(bookingDTO));
    }

    @Test
    public void testEligibleForBasicFreeUpdateNotInContract() {
        configureEligibleBasicFreeMocks(true, false);
        assertFalse(bookingPredicateProviderMock.eligibleForBasicFreeUpdate().test(bookingDTO));
    }

    private void configureEligibleBasicFreeMocks(boolean isSubscription, boolean eligibleForTracking) {
        given(bookingPredicateProviderMock.isBookingWithSubscription()).willReturn(isSubscription ? truePredicate() : falsePredicate());
        configureEligibleTrackingMockPredicates(eligibleForTracking, eligibleForTracking);
    }

    private void configureEligibleRecurringMockPredicates(boolean validStatus, boolean validMovement, boolean productPresent) {
        given(bookingPredicateProviderMock.hasValidStatusForRecurring()).willReturn(validStatus ? truePredicate() : falsePredicate());
        given(bookingPredicateProviderMock.hasValidMovementForRecurring()).willReturn(validMovement ? truePredicate() : falsePredicate());
        given(bookingPredicateProviderMock.isBookingWithRenewalOrSubscription()).willReturn(productPresent ? truePredicate() : falsePredicate());
    }

    private void configureEligibleTrackingMockPredicates(boolean contract, boolean perksPresent) {
        given(bookingPredicateProviderMock.isContract()).willReturn(contract ? truePredicate() : falsePredicate());
        given(bookingPredicateProviderMock.arePerksPresent()).willReturn(perksPresent ? truePredicate() : falsePredicate());
    }

    private void configureEligibleActivationMockPredicates(boolean contract, boolean subscriptionPresent) {
        given(bookingPredicateProviderMock.isContract()).willReturn(contract ? truePredicate() : falsePredicate());
        given(bookingPredicateProviderMock.isBookingWithSubscription()).willReturn(subscriptionPresent ? truePredicate() : falsePredicate());
    }
}