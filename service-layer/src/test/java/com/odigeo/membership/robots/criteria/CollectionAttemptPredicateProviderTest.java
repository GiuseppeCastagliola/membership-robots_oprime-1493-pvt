package com.odigeo.membership.robots.criteria;

import com.odigeo.membership.robots.dto.booking.CollectionAttemptDTO;
import com.odigeo.membership.robots.dto.booking.MovementDTO;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class CollectionAttemptPredicateProviderTest extends PredicateHelper<CollectionAttemptDTO> {
    private static final String RECURRING_ID = "ABC";
    private static final String PAID = CollectionAttemptPredicateProvider.MovementsStatus.PAID.name();
    private static final String REFUNDED = CollectionAttemptPredicateProvider.MovementsStatus.REFUNDED.name();
    private static final String CHARGEBACK = CollectionAttemptPredicateProvider.MovementsStatus.CHARGEBACKED.name();
    private static final String CONFIRM_ACTION = CollectionAttemptPredicateProvider.MovementsAction.CONFIRM.name();
    private static final String DIRECTY_PAY_ACTION = CollectionAttemptPredicateProvider.MovementsAction.DIRECTY_PAYMENT.name();
    private static final String CHARGEBACK_ACTION = CollectionAttemptPredicateProvider.MovementsAction.CHARGEBACK.name();
    private static final String MANUAL_REFUND_ACTION = CollectionAttemptPredicateProvider.MovementsAction.MANUAL_REFUND.name();

    @Mock
    private CollectionAttemptDTO collectionAttempt;
    @Mock
    private MovementDTO lastMovement;
    @Mock
    private CollectionAttemptPredicateProvider collectionAttemptPredicateProviderMock;
    private CollectionAttemptPredicateProvider collectionAttemptPredicateProvider;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        collectionAttemptPredicateProvider = new CollectionAttemptPredicateProvider();
        given(collectionAttemptPredicateProviderMock.isLastMovementStatusAndActionValid()).willCallRealMethod();
    }

    @Test
    public void testHaveRecurringId() {
        given(collectionAttempt.getRecurringId()).willReturn(RECURRING_ID, StringUtils.EMPTY, null);
        predicateMultipleTest(collectionAttemptPredicateProvider.hasRecurringId(), collectionAttempt, true, false, false);
    }

    @Test
    public void testIsLastMovementPaidAndConfirmedOrDirectyPayment() {
        given(lastMovement.getStatus()).willReturn(PAID, PAID, PAID, REFUNDED);
        given(lastMovement.getAction()).willReturn(CONFIRM_ACTION, DIRECTY_PAY_ACTION, StringUtils.EMPTY);
        given(collectionAttempt.getLastMovement()).willReturn(lastMovement);
        predicateMultipleTest(collectionAttemptPredicateProvider.isLastMovementPaidAndConfirmedOrDirectyPayment(), collectionAttempt, true, true, false, false);
    }

    @Test
    public void testIsLastMovementRefunded() {
        given(lastMovement.getStatus()).willReturn(REFUNDED, REFUNDED, REFUNDED, PAID);
        given(lastMovement.getAction()).willReturn(MANUAL_REFUND_ACTION, DIRECTY_PAY_ACTION, StringUtils.EMPTY);
        given(collectionAttempt.getLastMovement()).willReturn(lastMovement);
        predicateMultipleTest(collectionAttemptPredicateProvider.isLastMovementRefunded(), collectionAttempt, true, true, false, false);
    }

    @Test
    public void testIsLastMovementChargedBack() {
        given(lastMovement.getStatus()).willReturn(CHARGEBACK, CHARGEBACK, REFUNDED);
        given(lastMovement.getAction()).willReturn(CHARGEBACK_ACTION, StringUtils.EMPTY);
        given(collectionAttempt.getLastMovement()).willReturn(lastMovement);
        predicateMultipleTest(collectionAttemptPredicateProvider.isLastMovementChargedBack(), collectionAttempt, true, false, false);
    }

    @Test
    public void testIsLastMovementStatusAndActionValidFalse() {
        configurePredicateProviderMock(false, false, false);
        assertFalse(collectionAttemptPredicateProviderMock.isLastMovementStatusAndActionValid().test(collectionAttempt));
    }

    @Test
    public void testIsLastMovementStatusAndActionValidChargedBack() {
        configurePredicateProviderMock(true, false, false);
        assertTrue(collectionAttemptPredicateProviderMock.isLastMovementStatusAndActionValid().test(collectionAttempt));
    }

    @Test
    public void testIsLastMovementStatusAndActionValidRefunded() {
        configurePredicateProviderMock(false, true, false);
        assertTrue(collectionAttemptPredicateProviderMock.isLastMovementStatusAndActionValid().test(collectionAttempt));
    }

    @Test
    public void testIsLastMovementStatusAndActionValidPaid() {
        configurePredicateProviderMock(false, false, true);
        assertTrue(collectionAttemptPredicateProviderMock.isLastMovementStatusAndActionValid().test(collectionAttempt));
    }

    private void configurePredicateProviderMock(boolean chargedBack, boolean refunded, boolean paid) {
        given(collectionAttemptPredicateProviderMock.isLastMovementChargedBack()).willReturn(chargedBack ? truePredicate() : falsePredicate());
        given(collectionAttemptPredicateProviderMock.isLastMovementRefunded()).willReturn(refunded ? truePredicate() : falsePredicate());
        given(collectionAttemptPredicateProviderMock.isLastMovementPaidAndConfirmedOrDirectyPayment()).willReturn(paid ? truePredicate() : falsePredicate());
    }
}