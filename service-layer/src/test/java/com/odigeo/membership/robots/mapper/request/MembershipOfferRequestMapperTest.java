package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.offer.api.request.MembershipRenewalOfferRequest;
import com.odigeo.membership.robots.dto.MembershipDTO;
import org.mapstruct.factory.Mappers;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class MembershipOfferRequestMapperTest {

    private static final String WEBSITE = "ES";
    private static final long TEST_ID = 1L;
    private static final String SIX_M_AB = "X20-4";
    private static final MembershipOfferRequestMapper MAPPER = Mappers.getMapper(MembershipOfferRequestMapper.class);

    @Test
    public void testDtoToSearchCriteriaRequest() {
        final MembershipRenewalOfferRequest membershipRenewalOfferRequest = MAPPER.dtoToMembershipRenewalOfferRequest(MembershipDTO.builder().id(TEST_ID).website(WEBSITE).build());
        assertNotNull(membershipRenewalOfferRequest);
        assertEquals(membershipRenewalOfferRequest.getWebsiteCode(), WEBSITE);
        assertEquals(membershipRenewalOfferRequest.getMembershipId(), String.valueOf(TEST_ID));
        assertEquals(membershipRenewalOfferRequest.getTestTokenSet().getDimensionPartitionNumberMap().get(SIX_M_AB).intValue(), 1);
    }

    @Test
    public void testDtoNullToSearchCriteriaRequest() {
        assertNull(MAPPER.dtoToMembershipRenewalOfferRequest(null));
    }

}