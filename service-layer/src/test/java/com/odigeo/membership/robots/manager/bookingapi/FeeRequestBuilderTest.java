package com.odigeo.membership.robots.manager.bookingapi;

import bean.test.BeanTest;
import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;

public class FeeRequestBuilderTest extends BeanTest<FeeRequestBuilder> {

    private static final String CURRENCY = "EUR";

    @Override
    protected FeeRequestBuilder getBean() {
        return new FeeRequestBuilder()
                .withAmount(BigDecimal.TEN)
                .withCurrency(CURRENCY)
                .withSubCode(MembershipFeeSubCodes.AC12);
    }

    @Test
    public void testBuild() {
        FeeRequest feeRequest = getBean().build();
        assertEquals(feeRequest.getAmount(), BigDecimal.TEN);
        assertEquals(feeRequest.getCurrency(), CURRENCY);
        assertEquals(feeRequest.getSubCode(), MembershipFeeSubCodes.AC12.getFeeCode());
    }
}