package com.odigeo.membership.robots.consumer.updater;

import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import org.apache.commons.lang3.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.MockitoAnnotations.openMocks;

public class UpdaterBeanTest {
    private static final RetryableException RETRYABLE_EXCEPTION = new RetryableException(StringUtils.EMPTY);
    @Mock
    private MembershipDTO membershipDTO;
    @Mock
    private BookingDTO bookingDTO;
    @Mock
    private BookingUpdater bookingUpdater;
    @Mock
    private MembershipUpdater membershipUpdater;
    @InjectMocks
    private UpdaterBean updater;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
    }

    @AfterMethod
    public void cleanUp() {
        updater = null;
    }

    @Test
    public void testSaveRecurringId() {
        updater.saveRecurringId(membershipDTO, bookingDTO);
        then(membershipUpdater).should().saveRecurringId(eq(membershipDTO), eq(bookingDTO));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testSaveRecurringIdThrowsException() {
        given(membershipUpdater.saveRecurringId(eq(membershipDTO), eq(bookingDTO))).willThrow(RETRYABLE_EXCEPTION);
        updater.saveRecurringId(membershipDTO, bookingDTO);
    }

    @Test
    public void testActivateMembership() {
        updater.activateMembership(membershipDTO, bookingDTO);
        then(membershipUpdater).should().activateMembership(eq(membershipDTO), eq(bookingDTO));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testActivateMembershipThrowsException() {
        given(membershipUpdater.activateMembership(eq(membershipDTO), eq(bookingDTO))).willThrow(RETRYABLE_EXCEPTION);
        updater.activateMembership(membershipDTO, bookingDTO);
    }

    @Test
    public void testTrackBookingAndUpdateBalance() {
        updater.trackBookingAndUpdateBalance(bookingDTO);
        then(membershipUpdater).should().trackBookingAndUpdateBalance(eq(bookingDTO));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testTrackBookingAndUpdateBalanceThrowsException() {
        given(membershipUpdater.trackBookingAndUpdateBalance(eq(bookingDTO))).willThrow(RETRYABLE_EXCEPTION);
        updater.trackBookingAndUpdateBalance(bookingDTO);
    }

    @Test
    public void testDeleteBookingTracking() {
        updater.deleteBookingTracking(bookingDTO);
        then(membershipUpdater).should().deleteBookingTracking(eq(bookingDTO));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testDeleteBookingTrackingThrowsException() {
        doThrow(RETRYABLE_EXCEPTION).when(membershipUpdater).deleteBookingTracking(eq(bookingDTO));
        updater.deleteBookingTracking(bookingDTO);
    }

    @Test
    public void testBalanceBooking() {
        updater.balanceBooking(bookingDTO, membershipDTO);
        then(bookingUpdater).should().updateBookingWithUnbalancedFees(eq(bookingDTO), eq(membershipDTO));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testBalanceBookingThrowException() {
        doThrow(RETRYABLE_EXCEPTION).when(bookingUpdater).updateBookingWithUnbalancedFees(eq(bookingDTO), eq(membershipDTO));
        updater.balanceBooking(bookingDTO, membershipDTO);
    }

    @Test
    public void testUpdateBookingBasicFreeFirstBookingFails() {
        given(bookingUpdater.updateBookingBasicFreeFirstBooking(eq(bookingDTO), eq(membershipDTO))).willReturn(Boolean.TRUE);
        updater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO);
        then(bookingUpdater).should().updateBookingBasicFreeFirstBooking(eq(bookingDTO), eq(membershipDTO));
        then(membershipUpdater).should().enableAutoRenewal(eq(membershipDTO));
    }

    @Test
    public void testUpdateBookingBasicFreeFirstBooking() {
        given(bookingUpdater.updateBookingBasicFreeFirstBooking(eq(bookingDTO), eq(membershipDTO))).willReturn(Boolean.FALSE);
        updater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO);
        then(bookingUpdater).should().updateBookingBasicFreeFirstBooking(eq(bookingDTO), eq(membershipDTO));
        then(membershipUpdater).should(never()).enableAutoRenewal(eq(membershipDTO));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testUpdateBookingBasicFreeFirstBookingThrowException() {
        doThrow(RETRYABLE_EXCEPTION).when(bookingUpdater).updateBookingBasicFreeFirstBooking(eq(bookingDTO), eq(membershipDTO));
        updater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO);
    }
}