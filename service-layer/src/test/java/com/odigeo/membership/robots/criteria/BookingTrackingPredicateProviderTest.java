package com.odigeo.membership.robots.criteria;

import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingTrackingPredicateProviderTest {

    private static final BigDecimal MINUS_TEN = BigDecimal.TEN.negate();
    private static final BigDecimal TWENTY = BigDecimal.valueOf(20);
    private static final BigDecimal MINUS_TWENTY = TWENTY.negate();

    private MembershipDTO membershipDTO;
    @Mock
    private BookingDTO bookingDTO;
    @InjectMocks
    private BookingTrackingPredicateProvider bookingTrackingPredicateProvider;


    @BeforeMethod
    public void setUp() {
        openMocks(this);
    }


    @Test
    public void testIsAlreadyTrackedAndFeesAreChanged() {
        given(bookingDTO.getTotalAvoidFeesAmount()).willReturn(MINUS_TWENTY);
        given(bookingDTO.getTotalCostFeesAmount()).willReturn(BigDecimal.ZERO);
        given(bookingDTO.getTotalPerksAmount()).willReturn(BigDecimal.ZERO);
        BookingTrackingDTO bookingTrackingDTO = BookingTrackingDTO.builder().perksAmount(BigDecimal.ZERO).costFeeAmount(BigDecimal.ZERO).avoidFeeAmount(TWENTY).build();
        assertTrue(bookingTrackingPredicateProvider.isAlreadyTrackedAndFeesAreChanged(bookingDTO).test(bookingTrackingDTO));
        bookingTrackingDTO = BookingTrackingDTO.builder().perksAmount(BigDecimal.ONE).costFeeAmount(BigDecimal.ZERO).avoidFeeAmount(MINUS_TWENTY).build();
        assertTrue(bookingTrackingPredicateProvider.isAlreadyTrackedAndFeesAreChanged(bookingDTO).test(bookingTrackingDTO));
        bookingTrackingDTO = BookingTrackingDTO.builder().perksAmount(BigDecimal.ZERO).costFeeAmount(BigDecimal.ONE).avoidFeeAmount(MINUS_TWENTY).build();
        assertTrue(bookingTrackingPredicateProvider.isAlreadyTrackedAndFeesAreChanged(bookingDTO).test(bookingTrackingDTO));
    }

    @Test
    public void testIsAlreadyTrackedAndFeesAreNotChanged() {
        given(bookingDTO.getTotalAvoidFeesAmount()).willReturn(TWENTY);
        given(bookingDTO.getTotalCostFeesAmount()).willReturn(BigDecimal.ZERO);
        given(bookingDTO.getTotalPerksAmount()).willReturn(BigDecimal.ZERO);
        BookingTrackingDTO bookingTrackingDTO = BookingTrackingDTO.builder().perksAmount(BigDecimal.ZERO).costFeeAmount(BigDecimal.ZERO).avoidFeeAmount(TWENTY).build();
        assertFalse(bookingTrackingPredicateProvider.isAlreadyTrackedAndFeesAreChanged(bookingDTO).test(bookingTrackingDTO));
    }

    @Test
    public void testIsAlreadyTrackedAndFeesAreChangedNotTrackedBooking() {
        assertFalse(bookingTrackingPredicateProvider.isAlreadyTrackedAndFeesAreChanged(bookingDTO).test(null));
    }
}