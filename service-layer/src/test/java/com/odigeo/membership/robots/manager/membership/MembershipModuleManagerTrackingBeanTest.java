package com.odigeo.membership.robots.manager.membership;

import com.odigeo.membership.MembershipService;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.request.tracking.MembershipBookingTrackingRequest;
import com.odigeo.membership.response.tracking.MembershipBookingTracking;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.mapper.request.MembershipRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipModuleManagerTrackingBeanTest {
    private static final MembershipInternalServerErrorException MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION = new MembershipInternalServerErrorException("Exception");
    private BookingDTO bookingDTO;
    @Mock
    private MembershipService membershipService;
    private MembershipModuleManagerTrackingBean membershipModuleManagerTracking;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        bookingDTO = BookingDTO.builder().id(1L).membershipId(2L).build();
        membershipModuleManagerTracking = new MembershipModuleManagerTrackingBean(membershipService, Mappers.getMapper(MembershipRequestMapper.class), Mappers.getMapper(MembershipResponseMapper.class));
    }

    @Test
    public void testGetTracking() {
        BookingDTO bookingDTO = BookingDTO.builder().id(1L).build();
        MembershipBookingTracking membershipBookingTracking = new MembershipBookingTracking();
        membershipBookingTracking.setBookingId(bookingDTO.getId());
        given(membershipService.getBookingTracking(eq(bookingDTO.getId()))).willReturn(membershipBookingTracking, null);
        ApiCallWrapper<BookingTrackingDTO, BookingDTO> wrapper = membershipModuleManagerTracking.getBookingTracking(bookingDTO);
        assertTrue(wrapper.isSuccessful());
        assertEquals(bookingDTO.getId(), wrapper.getResponse().getBookingId());
        ApiCallWrapper<BookingTrackingDTO, BookingDTO> wrapperNotFound = membershipModuleManagerTracking.getBookingTracking(bookingDTO);
        assertTrue(wrapperNotFound.isSuccessful());
        assertNull(wrapperNotFound.getResponse());
    }

    @Test
    public void testGetTrackingFail() {
        MembershipBookingTracking membershipBookingTracking = new MembershipBookingTracking();
        membershipBookingTracking.setBookingId(bookingDTO.getId());
        given(membershipService.getBookingTracking(eq(bookingDTO.getId()))).willThrow(MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        ApiCallWrapper<BookingTrackingDTO, BookingDTO> wrapper = membershipModuleManagerTracking.getBookingTracking(bookingDTO);
        assertTrue(wrapper.isFailed());
        assertNull(wrapper.getResponse());
        assertEquals(wrapper.getThrowable(), MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
    }

    @Test
    public void testTrackBookingAndUpdateBalance() {
        MembershipBookingTracking membershipBookingTracking = new MembershipBookingTracking();
        membershipBookingTracking.setBookingId(bookingDTO.getId());
        membershipBookingTracking.setMembershipId(bookingDTO.getMembershipId());
        given(membershipService.addBookingTrackingUpdateBalance(any(MembershipBookingTrackingRequest.class))).willReturn(membershipBookingTracking);
        ApiCallWrapper<BookingTrackingDTO, BookingTrackingDTO> wrapper = membershipModuleManagerTracking.trackBookingAndUpdateBalance(BookingTrackingDTO.builder()
                .bookingId(bookingDTO.getId())
                .membershipId(bookingDTO.getMembershipId())
                .bookingDate(LocalDateTime.MIN)
                .build());
        assertTrue(wrapper.isSuccessful());
        assertEquals(bookingDTO.getId(), wrapper.getResponse().getBookingId());
        assertEquals(bookingDTO.getMembershipId().longValue(), wrapper.getResponse().getMembershipId());
    }

    @Test
    public void testTrackBookingAndUpdateBalanceFail() {
        given(membershipService.addBookingTrackingUpdateBalance(any(MembershipBookingTrackingRequest.class))).willThrow(MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        ApiCallWrapper<BookingTrackingDTO, BookingTrackingDTO> wrapper = membershipModuleManagerTracking.trackBookingAndUpdateBalance(BookingTrackingDTO.builder()
                .bookingId(bookingDTO.getId())
                .membershipId(bookingDTO.getMembershipId())
                .bookingDate(LocalDateTime.MIN)
                .build());
        assertTrue(wrapper.isFailed());
        assertEquals(wrapper.getThrowable(), MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        assertNull(wrapper.getResponse());
    }

    @Test
    public void testDeleteBookingTrackingAndUpdateBalance() {
        ApiCallWrapper<Void, BookingDTO> wrapper = membershipModuleManagerTracking.deleteBookingTrackingAndUpdateBalance(bookingDTO);
        then(membershipService).should().deleteBookingTrackingUpdateBalance(eq(bookingDTO.getId()));
        assertTrue(wrapper.isSuccessful());
    }

    @Test
    public void testDeleteBookingTrackingAndUpdateBalanceFails() {
        doThrow(MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION).when(membershipService).deleteBookingTrackingUpdateBalance(eq(bookingDTO.getId()));
        ApiCallWrapper<Void, BookingDTO> wrapper = membershipModuleManagerTracking.deleteBookingTrackingAndUpdateBalance(bookingDTO);
        then(membershipService).should().deleteBookingTrackingUpdateBalance(eq(bookingDTO.getId()));
        assertTrue(wrapper.isFailed());
        assertEquals(wrapper.getThrowable(), MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
    }
}
