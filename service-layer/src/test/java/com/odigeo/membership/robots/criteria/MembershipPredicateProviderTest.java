package com.odigeo.membership.robots.criteria;

import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.expiration.MembershipExpirationServiceBean;
import org.apache.commons.lang3.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipPredicateProviderTest extends PredicateHelper<MembershipDTO> {
    private static final String PENDING_TO_ACTIVATE = MembershipExpirationServiceBean.MembershipStatus.PENDING_TO_ACTIVATE.name();
    private static final String PENDING_TO_COLLECT = MembershipExpirationServiceBean.MembershipStatus.PENDING_TO_COLLECT.name();
    private static final String ACTIVATED = MembershipExpirationServiceBean.MembershipStatus.ACTIVATED.name();
    private static final String RECURRING_ID = "A12B";
    private static final BigDecimal MINUS_TEN = BigDecimal.TEN.negate();
    private static final BigDecimal TWENTY = BigDecimal.valueOf(20);
    private static final BigDecimal MINUS_TWENTY = TWENTY.negate();
    @Mock
    private MembershipDTO membershipDTO;
    @Mock
    private MembershipPredicateProvider membershipPredicateProviderMock;
    @Mock
    private BookingDTO bookingDTO;

    @InjectMocks
    private MembershipPredicateProvider membershipPredicateProvider;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        given(membershipPredicateProviderMock.isBasicFreeToProcess()).willCallRealMethod();
    }

    @Test
    public void testIsPendingToActivate() {
        given(membershipDTO.getStatus()).willReturn(PENDING_TO_ACTIVATE, PENDING_TO_COLLECT, ACTIVATED);
        predicateMultipleTest(membershipPredicateProvider.isPendingToActivate(), membershipDTO, true, false, false);
    }

    @Test
    public void testIsActivated() {
        given(membershipDTO.getStatus()).willReturn(PENDING_TO_ACTIVATE, PENDING_TO_COLLECT, ACTIVATED);
        predicateMultipleTest(membershipPredicateProvider.isActivated(), membershipDTO, false, false, true);
    }

    @Test
    public void testHaveRecurringId() {
        given(membershipDTO.getRecurringId()).willReturn(RECURRING_ID, StringUtils.EMPTY, null);
        predicateMultipleTest(membershipPredicateProvider.hasRecurringId(), membershipDTO, true, false, false);
    }

    @Test
    public void testIsBasicFree() {
        given(membershipDTO.getMembershipType()).willReturn("BASIC_FREE", "BASIC", StringUtils.EMPTY, null);
        predicateMultipleTest(membershipPredicateProvider.isBasicFree(), membershipDTO, true, false, false, false);
    }

    @Test
    public void testIsAutoRenewalDisabled() {
        given(membershipDTO.getAutoRenewal()).willReturn("DISABLED", "ENABLED", StringUtils.EMPTY, null);
        predicateMultipleTest(membershipPredicateProvider.isAutoRenewalDisabled(), membershipDTO, true, false, false, false);
    }

    @Test
    public void testIsBasicFreeToProcess() {
        configureBasicFreeProcessMockPredicates(true, true, true);
        assertTrue(membershipPredicateProviderMock.isBasicFreeToProcess().test(membershipDTO));
    }

    @Test
    public void testIsBasicFreeToProcessNotActivated() {
        configureBasicFreeProcessMockPredicates(false, true, true);
        assertFalse(membershipPredicateProviderMock.isBasicFreeToProcess().test(membershipDTO));
    }

    @Test
    public void testIsBasicFreeToProcessNotBasicFree() {
        configureBasicFreeProcessMockPredicates(true, false, true);
        assertFalse(membershipPredicateProviderMock.isBasicFreeToProcess().test(membershipDTO));
    }

    @Test
    public void testIsBasicFreeToProcessAutorenewalEnabled() {
        configureBasicFreeProcessMockPredicates(true, true, false);
        assertFalse(membershipPredicateProviderMock.isBasicFreeToProcess().test(membershipDTO));
    }

    @Test
    public void testAreFeesUnbalancedWhenAvoidFeeBiggerThanBalance() {
        MembershipDTO membershipDTO = MembershipDTO.builder().balance(BigDecimal.TEN).build();
        setupFees(MINUS_TWENTY, BigDecimal.ZERO);
        assertTrue(membershipPredicateProvider.areFeesUnbalanced(bookingDTO).test(membershipDTO));
    }

    @Test
    public void testAreFeesUnbalancedWhenAvoidFeeConsumeBalanceAndCostExists() {
        MembershipDTO membershipDTO = MembershipDTO.builder().balance(TWENTY).build();
        setupFees(MINUS_TWENTY, MINUS_TEN);
        assertFalse(membershipPredicateProvider.areFeesUnbalanced(bookingDTO).test(membershipDTO));
    }

    @Test
    public void testAreFeesUnbalancedWhenAvoidFeeSmallerThanBalanceAndCostExists() {
        MembershipDTO membershipDTO = MembershipDTO.builder().balance(TWENTY).build();
        setupFees(MINUS_TEN, MINUS_TEN);
        assertTrue(membershipPredicateProvider.areFeesUnbalanced(bookingDTO).test(membershipDTO));
    }

    @Test
    public void testAreFeesUnbalancedWhenAvoidFeeSmallerThanBalanceAndCostNotExists() {
        MembershipDTO membershipDTO = MembershipDTO.builder().balance(TWENTY).build();
        setupFees(MINUS_TEN, BigDecimal.ZERO);
        assertFalse(membershipPredicateProvider.areFeesUnbalanced(bookingDTO).test(membershipDTO));
    }


    private void setupFees(BigDecimal avoidFees, BigDecimal costFees) {
        given(bookingDTO.getTotalAvoidFeesAmount()).willReturn(avoidFees);
        given(bookingDTO.getTotalCostFeesAmount()).willReturn(costFees);
    }

    private void configureBasicFreeProcessMockPredicates(boolean activated, boolean basicFree, boolean autoRenewalDisabled) {
        given(membershipPredicateProviderMock.isActivated()).willReturn(activated ? truePredicate() : falsePredicate());
        given(membershipPredicateProviderMock.isBasicFree()).willReturn(basicFree ? truePredicate() : falsePredicate());
        given(membershipPredicateProviderMock.isAutoRenewalDisabled()).willReturn(autoRenewalDisabled ? truePredicate() : falsePredicate());
    }
}