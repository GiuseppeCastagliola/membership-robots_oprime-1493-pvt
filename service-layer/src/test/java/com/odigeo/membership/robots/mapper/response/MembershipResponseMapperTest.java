package com.odigeo.membership.robots.mapper.response;

import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.robots.dto.MemberAccountDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import org.mapstruct.factory.Mappers;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class MembershipResponseMapperTest {

    private static final String ACTIVATED = "ACTIVATED";
    private static final String EXPIRED = "EXPIRED";
    private static final String DISABLED = "DISABLED";
    private static final String ENABLED = "ENABLED";
    private static final long TEST_ID = 1L;
    private static final String WEBSITE = "IT";
    private static final String BUSINESS = "BUSINESS";
    private static final String POST_BOOKING = "POST_BOOKING";
    private static final String INIT = "INIT";
    private static final String LAST_NAMES = "Sanchez";
    private static final String NAME = "Pedro";
    private static final long USER_ID = 111L;
    private static final long MEMBER_ACCOUNT_ID = 123L;
    public static final String EUR = "EUR";
    private static final String TEST_DATE = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
    private static final MembershipResponseMapper MEMBERSHIP_RESPONSE_MAPPER = Mappers.getMapper(MembershipResponseMapper.class);

    @Test
    public void expiredDTOTest() {
        MembershipDTO membershipDTO = MembershipDTO.builder().id(TEST_ID).status(ACTIVATED).build();
        final MembershipDTO expiredDto = MEMBERSHIP_RESPONSE_MAPPER.dtoToExpiredDto(membershipDTO);
        assertNotEquals(membershipDTO, expiredDto);
        assertEquals(expiredDto.getId(), expiredDto.getId());
        assertEquals(expiredDto.getStatus(), EXPIRED);
    }

    @Test
    public void enabledAutoRenewalDTOTest() {
        MembershipDTO membershipDTO = MembershipDTO.builder().id(TEST_ID).status(ACTIVATED).autoRenewal(DISABLED).build();
        final MembershipDTO enabledAutoRenewalDto = MEMBERSHIP_RESPONSE_MAPPER.dtoToEnabledAutoRenewalDto(membershipDTO);
        assertNotEquals(membershipDTO, enabledAutoRenewalDto);
        assertEquals(enabledAutoRenewalDto.getId(), enabledAutoRenewalDto.getId());
        assertEquals(enabledAutoRenewalDto.getAutoRenewal(), ENABLED);
    }


    @Test
    public void testDtoToConsumedBalanceDTO() {
        MembershipDTO membershipDTO = MembershipDTO.builder().id(TEST_ID).balance(BigDecimal.TEN).build();
        MembershipDTO consumedBalanceDto = MEMBERSHIP_RESPONSE_MAPPER.dtoToConsumedBalanceDto(membershipDTO);
        assertNotEquals(membershipDTO, consumedBalanceDto);
        assertEquals(membershipDTO.getId(), consumedBalanceDto.getId());
        assertEquals(consumedBalanceDto.getBalance(), BigDecimal.ZERO);
    }

    @Test
    public void membershipResponseWithoutAccountToDtoTest() {
        final MembershipResponse membershipResponse = createResponse(false);
        final MembershipDTO membershipDTO = MEMBERSHIP_RESPONSE_MAPPER.membershipResponseToDto(membershipResponse);
        checkEqualMembershipFields(membershipResponse, membershipDTO);
        assertNull(membershipDTO.getName());
        assertNull(membershipDTO.getLastName());
        assertNull(membershipDTO.getUserId());
    }

    @Test
    public void membershipResponseWithAccountToDtoTest() {
        final MembershipResponse membershipResponse = createResponse(true);
        final MembershipDTO membershipDTO = MEMBERSHIP_RESPONSE_MAPPER.membershipResponseToDto(membershipResponse);
        checkEqualMembershipFields(membershipResponse, membershipDTO);
        assertEquals(membershipDTO.getTimestamp().format(DateTimeFormatter.ISO_DATE_TIME), membershipResponse.getTimestamp());
        assertEquals(membershipDTO.getName(), membershipResponse.getMemberAccount().getName());
        assertEquals(membershipDTO.getLastName(), membershipResponse.getMemberAccount().getLastNames());
        assertEquals(membershipDTO.getUserId().longValue(), membershipResponse.getMemberAccount().getUserId());
    }

    @Test
    public void membershipResponseWithNullDatesToDtoTest() {
        final MembershipResponse membershipResponse = createResponse(true);
        membershipResponse.setTimestamp(null);
        membershipResponse.setActivationDate(null);
        membershipResponse.setExpirationDate(null);
        final MembershipDTO membershipDTO = MEMBERSHIP_RESPONSE_MAPPER.membershipResponseToDto(membershipResponse);
        checkEqualMembershipFields(membershipResponse, membershipDTO);
        assertEquals(membershipDTO.getName(), membershipResponse.getMemberAccount().getName());
        assertEquals(membershipDTO.getLastName(), membershipResponse.getMemberAccount().getLastNames());
        assertEquals(membershipDTO.getUserId().longValue(), membershipResponse.getMemberAccount().getUserId());
    }

    @Test
    public void testMemberAccountResponseToMemberAccountDto() {
        List<MemberAccountDTO> memberAccountDTOS = MEMBERSHIP_RESPONSE_MAPPER.memberAccountResponsesToDtos(Collections.singletonList(getMemberAccount()));
        assertNotNull(memberAccountDTOS);
        MemberAccountDTO memberAccountDTO = memberAccountDTOS.get(0);
        assertEquals(NAME, memberAccountDTO.getName());
        assertEquals(LAST_NAMES, memberAccountDTO.getLastNames());
        assertEquals(USER_ID, memberAccountDTO.getUserId());
        assertEquals(MEMBER_ACCOUNT_ID, memberAccountDTO.getId());
    }

    private void checkEqualMembershipFields(MembershipResponse membershipResponse, MembershipDTO membershipDTO) {
        assertEquals(membershipDTO.getId(), membershipResponse.getId());
        assertEquals(membershipDTO.getWebsite(), membershipResponse.getWebsite());
        assertEquals(membershipDTO.getUserCreditCardId(), membershipResponse.getUserCreditCardId());
        assertEquals(membershipDTO.getTotalPrice(), membershipResponse.getTotalPrice());
        assertEquals(membershipDTO.getMemberAccountId(), membershipResponse.getMemberAccountId());
        assertEquals(membershipDTO.getStatus(), membershipResponse.getStatus());
    }

    @Test
    public void membershipResponseListToDtoListTest() {
        final List<MembershipResponse> membershipResponses = Arrays.asList(createResponse(true), createResponse(false));
        final List<MembershipDTO> membershipDTOS = MEMBERSHIP_RESPONSE_MAPPER.membershipResponsesToDtos(membershipResponses);
        assertEquals(membershipResponses.size(), membershipDTOS.size());
        checkEqualMembershipFields(membershipResponses.get(0), membershipDTOS.get(0));
        checkEqualMembershipFields(membershipResponses.get(1), membershipDTOS.get(1));
    }


    @Test
    public void mapperNullDtoTest() {
        assertNull(MEMBERSHIP_RESPONSE_MAPPER.dtoToEnabledAutoRenewalDto(null));
        assertNull(MEMBERSHIP_RESPONSE_MAPPER.dtoToExpiredDto(null));
        assertNull(MEMBERSHIP_RESPONSE_MAPPER.membershipResponsesToDtos(null));
        assertNull(MEMBERSHIP_RESPONSE_MAPPER.membershipResponseToDto(null));
        assertNull(MEMBERSHIP_RESPONSE_MAPPER.dtoToConsumedBalanceDto(null));
        assertNull(MEMBERSHIP_RESPONSE_MAPPER.memberAccountResponsesToDtos(null));
        assertNull(MEMBERSHIP_RESPONSE_MAPPER.dtoToDeactivatedDto(null));
        assertNull(MEMBERSHIP_RESPONSE_MAPPER.dtoToDiscardedDto(null));
    }

    private MembershipResponse createResponse(boolean withAccount) {
        final MembershipResponse membershipResponse = new MembershipResponse();
        membershipResponse.setTimestamp(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        membershipResponse.setWebsite(WEBSITE);
        membershipResponse.setUserCreditCardId(BigDecimal.ONE);
        membershipResponse.setTotalPrice(BigDecimal.TEN);
        membershipResponse.setStatus(EXPIRED);
        membershipResponse.setMembershipType(BUSINESS);
        membershipResponse.setMonthsDuration(12);
        membershipResponse.setSourceType(POST_BOOKING);
        membershipResponse.setProductStatus(INIT);
        membershipResponse.setMemberStatusActions(Collections.emptyList());
        if (withAccount) {
            membershipResponse.setMemberAccount(getMemberAccount());
        }
        membershipResponse.setMemberAccountId(MEMBER_ACCOUNT_ID);
        membershipResponse.setId(TEST_ID);
        membershipResponse.setCurrencyCode(EUR);
        membershipResponse.setExpirationDate(TEST_DATE);
        membershipResponse.setActivationDate(TEST_DATE);
        membershipResponse.setBalance(BigDecimal.TEN);
        return membershipResponse;
    }

    private MemberAccountResponse getMemberAccount() {
        final MemberAccountResponse memberAccountResponse = new MemberAccountResponse();
        memberAccountResponse.setLastNames(LAST_NAMES);
        memberAccountResponse.setName(NAME);
        memberAccountResponse.setUserId(USER_ID);
        memberAccountResponse.setId(MEMBER_ACCOUNT_ID);
        memberAccountResponse.setMemberships(Collections.emptyList());
        return memberAccountResponse;
    }
}