package com.odigeo.membership.robots.consumer.updater;

import com.odigeo.membership.exception.MembershipForbiddenException;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.CollectionAttemptDTO;
import com.odigeo.membership.robots.dto.booking.ProductCategoryBookingDTO;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.mapper.request.BookingRequestMapper;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import org.apache.commons.lang3.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertNotNull;

public class MembershipUpdaterTest {
    private static final long MEMBERSHIP_ID = 1L;
    private static final String RECURRING_ID = "id123";
    private static final long BOOKING_ID = 1L;
    private static final MembershipInternalServerErrorException MEMBERSHIP_INTERNAL_ERROR = new MembershipInternalServerErrorException(StringUtils.EMPTY);
    private MembershipDTO membershipDTO;
    private BookingDTO bookingDTO;
    private BookingTrackingDTO bookingTrackingDTO;
    @Mock
    private MembershipRobotsMetrics membershipRobotsMetrics;
    @Mock
    private ExternalModuleManager externalModuleManager;
    @Mock
    private ApiCallWrapper<MembershipDTO, MembershipDTO> insertRecurringWrapper;
    @Mock
    private ApiCallWrapper<MembershipDTO, MembershipDTO> activateWrapper;
    @Mock
    private BookingRequestMapper bookingRequestMapper;
    @InjectMocks
    private MembershipUpdater membershipUpdater;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        buildDtos();
        given(bookingRequestMapper.trackingDtoFromBookingDTO(eq(bookingDTO)))
                .willReturn(bookingTrackingDTO);
    }

    @AfterMethod
    public void tearDown() {
        membershipUpdater = null;
    }

    private void buildDtos() {
        membershipDTO = MembershipDTO.builder()
                .id(MEMBERSHIP_ID)
                .balance(BigDecimal.ONE)
                .build();
        bookingDTO = BookingDTO.builder()
                .id(BOOKING_ID)
                .collectionAttempts(Collections.singletonList(CollectionAttemptDTO.builder()
                        .recurringId(RECURRING_ID)
                        .build()))
                .bookingProducts(Collections.singletonList(
                        ProductCategoryBookingDTO.builder()
                                .productType("MEMBERSHIP_SUBSCRIPTION")
                                .price(BigDecimal.TEN)
                                .build()
                ))
                .totalPerksAmount(BigDecimal.ONE)
                .totalCostFeesAmount(BigDecimal.ONE)
                .totalAvoidFeesAmount(BigDecimal.ONE)
                .build();
        bookingTrackingDTO = BookingTrackingDTO.builder().build();
    }

    @Test
    public void testSaveRecurringId() {
        given(insertRecurringWrapper.isSuccessful()).willReturn(true);
        given(externalModuleManager.saveRecurringId(any(MembershipDTO.class), any(BookingDTO.class))).willReturn(insertRecurringWrapper);
        membershipUpdater.saveRecurringId(membershipDTO, bookingDTO);
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.RECURRING_ATTEMPTS));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.INSERT_RECURRING_SUCCESS));
        then(externalModuleManager).should().saveRecurringId(any(MembershipDTO.class), any(BookingDTO.class));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testFailedSaveRecurringIdWithoutException() {
        given(insertRecurringWrapper.isSuccessful()).willReturn(false);
        given(externalModuleManager.saveRecurringId(any(MembershipDTO.class), any(BookingDTO.class))).willReturn(insertRecurringWrapper);
        membershipUpdater.saveRecurringId(membershipDTO, bookingDTO);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testFailedSaveRecurringIdWithException() {
        given(insertRecurringWrapper.isSuccessful()).willReturn(false);
        given(insertRecurringWrapper.getThrowable()).willReturn(MEMBERSHIP_INTERNAL_ERROR);
        given(externalModuleManager.saveRecurringId(any(MembershipDTO.class), any(BookingDTO.class))).willReturn(insertRecurringWrapper);
        membershipUpdater.saveRecurringId(membershipDTO, bookingDTO);
    }

    @Test
    public void testFailedSaveRecurringIdBecauseAlreadySet() {
        given(insertRecurringWrapper.isSuccessful()).willReturn(false);
        given(insertRecurringWrapper.getThrowable()).willReturn(new MembershipForbiddenException(StringUtils.EMPTY));
        given(externalModuleManager.saveRecurringId(any(MembershipDTO.class), any(BookingDTO.class))).willReturn(insertRecurringWrapper);
        membershipUpdater.saveRecurringId(membershipDTO, bookingDTO);
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.RECURRING_ATTEMPTS));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.INSERT_RECURRING_FAILED));
        then(externalModuleManager).should().saveRecurringId(any(MembershipDTO.class), any(BookingDTO.class));
    }

    @Test
    public void testActivation() {
        given(activateWrapper.isSuccessful()).willReturn(true);
        given(activateWrapper.getResponse()).willReturn(MembershipDTO.builder().status("ACTIVATED").build());
        given(externalModuleManager.searchMemberships(any(SearchMembershipsDTO.class)))
                .willReturn(new ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO>(ApiCall.Endpoint.SEARCH_MEMBERSHIPS, ApiCall.Result.SUCCESS)
                        .response(Collections.emptyList()).build());
        given(externalModuleManager.activateMembership(any(MembershipDTO.class), any(BookingDTO.class))).willReturn(activateWrapper);
        MembershipDTO membershipDtoResponse = membershipUpdater.activateMembership(membershipDTO, bookingDTO);
        assertNotNull(membershipDtoResponse);
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.ACTIVATE_ATTEMPTS));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.ACTIVATION_SUCCESS));
        then(externalModuleManager).should().activateMembership(any(MembershipDTO.class), any(BookingDTO.class));
    }

    @Test
    public void testActivationUserAlreadyPrime() {
        given(externalModuleManager.searchMemberships(any(SearchMembershipsDTO.class)))
                .willReturn(new ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO>(ApiCall.Endpoint.SEARCH_MEMBERSHIPS, ApiCall.Result.SUCCESS)
                        .response(Collections.singletonList(MembershipDTO.builder().id(1L).build())).build());
        MembershipDTO membershipDtoResponse = membershipUpdater.activateMembership(membershipDTO, bookingDTO);
        assertNotNull(membershipDtoResponse);
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.ACTIVATE_ATTEMPTS));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.ALREADY_SUBSCRIBED));
        then(externalModuleManager).should(never()).activateMembership(any(MembershipDTO.class), any(BookingDTO.class));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testActivationWithException() {
        given(activateWrapper.isSuccessful()).willReturn(false);
        given(activateWrapper.getThrowable()).willReturn(MEMBERSHIP_INTERNAL_ERROR);
        given(externalModuleManager.activateMembership(any(MembershipDTO.class), any(BookingDTO.class))).willReturn(activateWrapper);
        given(externalModuleManager.searchMemberships(any(SearchMembershipsDTO.class)))
                .willReturn(new ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO>(ApiCall.Endpoint.SEARCH_MEMBERSHIPS, ApiCall.Result.SUCCESS)
                        .response(Collections.emptyList()).build());
        membershipUpdater.activateMembership(membershipDTO, bookingDTO);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testActivationWithSearchException() {
        given(externalModuleManager.searchMemberships(any(SearchMembershipsDTO.class)))
                .willReturn(new ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO>(ApiCall.Endpoint.SEARCH_MEMBERSHIPS, ApiCall.Result.FAIL)
                        .throwable(MEMBERSHIP_INTERNAL_ERROR).build());
        membershipUpdater.activateMembership(membershipDTO, bookingDTO);
    }

    @Test
    public void testDeleteBookingTracking() {
        given(externalModuleManager.deleteBookingTrackingAndUpdateBalance(eq(bookingDTO)))
                .willReturn(new ApiCallWrapperBuilder<Void, BookingDTO>(ApiCall.Endpoint.DELETE_BOOKING_TRACKING, ApiCall.Result.SUCCESS)
                        .build());
        membershipUpdater.deleteBookingTracking(bookingDTO);
        then(membershipRobotsMetrics).should().incrementCounter(MetricsName.DELETE_TRACKING_ATTEMPTS);
        then(membershipRobotsMetrics).should().incrementCounter(MetricsName.DELETE_TRACKING_SUCCESS);
        then(externalModuleManager).should().deleteBookingTrackingAndUpdateBalance(eq(bookingDTO));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testDeleteBookingTrackingFail() {
        given(externalModuleManager.deleteBookingTrackingAndUpdateBalance(eq(bookingDTO)))
                .willReturn(new ApiCallWrapperBuilder<Void, BookingDTO>(ApiCall.Endpoint.DELETE_BOOKING_TRACKING, ApiCall.Result.FAIL)
                        .throwable(MEMBERSHIP_INTERNAL_ERROR)
                        .build());
        membershipUpdater.deleteBookingTracking(bookingDTO);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testDeleteBookingTrackingFailUnknown() {
        given(externalModuleManager.deleteBookingTrackingAndUpdateBalance(eq(bookingDTO)))
                .willReturn(new ApiCallWrapperBuilder<Void, BookingDTO>(ApiCall.Endpoint.DELETE_BOOKING_TRACKING, ApiCall.Result.FAIL)
                        .build());
        membershipUpdater.deleteBookingTracking(bookingDTO);
    }

    @Test
    public void testTrackBooking() {
        given(externalModuleManager.trackBookingAndUpdateBalance(eq(bookingTrackingDTO)))
                .willReturn(new ApiCallWrapperBuilder<BookingTrackingDTO, BookingTrackingDTO>(ApiCall.Endpoint.TRACK_BOOKING, ApiCall.Result.SUCCESS)
                        .response(bookingTrackingDTO)
                        .build());
        membershipUpdater.trackBookingAndUpdateBalance(bookingDTO);
        then(membershipRobotsMetrics).should().incrementCounter(MetricsName.TRACKING_ATTEMPTS);
        then(membershipRobotsMetrics).should().incrementCounter(MetricsName.INSERT_TRACKING_SUCCESS);
        then(externalModuleManager).should().trackBookingAndUpdateBalance(eq(bookingTrackingDTO));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testTrackBookingFail() {
        given(externalModuleManager.trackBookingAndUpdateBalance(eq(bookingTrackingDTO)))
                .willReturn(new ApiCallWrapperBuilder<BookingTrackingDTO, BookingTrackingDTO>(ApiCall.Endpoint.TRACK_BOOKING, ApiCall.Result.FAIL)
                        .throwable(MEMBERSHIP_INTERNAL_ERROR)
                        .build());
        membershipUpdater.trackBookingAndUpdateBalance(bookingDTO);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testTrackBookingFailUnknown() {
        given(externalModuleManager.trackBookingAndUpdateBalance(eq(bookingTrackingDTO)))
                .willReturn(new ApiCallWrapperBuilder<BookingTrackingDTO, BookingTrackingDTO>(ApiCall.Endpoint.TRACK_BOOKING, ApiCall.Result.FAIL)
                        .build());
        membershipUpdater.trackBookingAndUpdateBalance(bookingDTO);
    }

    @Test
    public void testEnableAutoRenewal() {
        given(externalModuleManager.enableMembershipAutoRenewal(eq(membershipDTO)))
                .willReturn(new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.ENABLE_AUTO_RENEWAL, ApiCall.Result.SUCCESS).build());
        membershipUpdater.enableAutoRenewal(this.membershipDTO);
        then(externalModuleManager).should().enableMembershipAutoRenewal(eq(membershipDTO));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testEnableAutoRenewalFail() {
        given(externalModuleManager.enableMembershipAutoRenewal(eq(membershipDTO)))
                .willReturn(new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.ENABLE_AUTO_RENEWAL, ApiCall.Result.FAIL)
                        .throwable(MEMBERSHIP_INTERNAL_ERROR)
                        .build());
        membershipUpdater.enableAutoRenewal(this.membershipDTO);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testEnableAutoRenewalFailUnknown() {
        given(externalModuleManager.enableMembershipAutoRenewal(eq(membershipDTO)))
                .willReturn(new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.ENABLE_AUTO_RENEWAL, ApiCall.Result.FAIL)
                        .build());
        membershipUpdater.enableAutoRenewal(this.membershipDTO);
    }
}