package com.odigeo.membership.robots.metrics;

import com.odigeo.membership.robots.metrics.MetricsName.TagName;
import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.testng.Assert.fail;

public class MembershipRobotsMetricsTest {

    @Test
    public void testIncrementCounterWithMetricsNameAreAllCorrect() {
        MembershipRobotsMetrics membershipRobotsMetrics = new MembershipRobotsMetrics();
        try {
            Arrays.stream(MetricsName.values()).forEach(membershipRobotsMetrics::incrementCounter);
            Arrays.stream(TagName.values()).forEach(tagName -> membershipRobotsMetrics.incrementCounter(MetricsName.values()[0], tagName, StringUtils.SPACE));
        } catch (IllegalArgumentException e) {
            fail(e.getMessage());
        }
    }
}