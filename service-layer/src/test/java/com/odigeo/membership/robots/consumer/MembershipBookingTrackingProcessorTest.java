package com.odigeo.membership.robots.consumer;

import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.consumer.updater.Updater;
import com.odigeo.membership.robots.criteria.MembershipTrackingBiPredicateProvider;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.function.BiPredicate;
import java.util.function.Consumer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.openMocks;

public class MembershipBookingTrackingProcessorTest {
    private static final Long MEMBERSHIP_ID = 1L;
    private static final BiPredicate<MembershipDTO, BookingTrackingDTO> FALSE_BI_PREDICATE = (x, y) -> false;
    private static final BiPredicate<MembershipDTO, BookingTrackingDTO> TRUE_BI_PREDICATE = (x, y) -> true;
    private static final Long BOOKING_ID = 1L;
    @Mock
    private BookingDTO bookingDTO;
    @Mock
    private MembershipDTO membershipDTO;
    @Mock
    private ExternalModuleManager externalModuleManager;
    @Mock
    private MembershipTrackingBiPredicateProvider membershipTrackingBiPredicateProvider;
    @Mock
    private Updater updater;
    @InjectMocks
    private MembershipBookingTrackingProcessor membershipBookingTrackingProcessor;


    @BeforeMethod
    public void setUp() {
        openMocks(this);
        given(bookingDTO.getMembershipId()).willReturn(MEMBERSHIP_ID);
        given(bookingDTO.getId()).willReturn(BOOKING_ID);
        givenSuccessfulGetMembership();
        givenSuccessfulGetBookingTracking();
    }

    @AfterMethod
    public void cleanUp() {
        membershipBookingTrackingProcessor = null;
    }


    @Test
    public void testProcessEmptyFilteredExecutions() {
        membershipBookingTrackingProcessor.process(BOOKING_ID);
        then(externalModuleManager).shouldHaveNoInteractions();
        then(membershipTrackingBiPredicateProvider).shouldHaveNoInteractions();
    }


    @Test
    public void testProcessTrackingSaveCandidate() {
        givenSuccessfulGetBookingTrackingNotTracked();
        given(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreUnbalancedAndNotTracked(eq(bookingDTO))).willReturn(FALSE_BI_PREDICATE);
        given(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreTrackedAndChanged(eq(bookingDTO))).willReturn(FALSE_BI_PREDICATE);
        setUpConsumerAndCondition(MembershipBookingTrackingProcessor::addTrackingCandidate, membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(eq(bookingDTO)), true);
        membershipBookingTrackingProcessor.process(MEMBERSHIP_ID);
        then(externalModuleManager).should(never()).getMembership(eq(MEMBERSHIP_ID));
        then(membershipTrackingBiPredicateProvider).should().isActivatedAndBookingFeesAreBalancedAndNotTracked(eq(bookingDTO));
        then(updater).should().trackBookingAndUpdateBalance(eq(bookingDTO));
        then(externalModuleManager).should(never()).updateBooking(eq(bookingDTO.getId()), any());
    }

    @Test
    public void testProcessTrackingUnbalancedBooking() {
        givenSuccessfulGetBookingTrackingNotTracked();
        given(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreUnbalancedAndNotTracked(eq(bookingDTO))).willReturn(TRUE_BI_PREDICATE);
        given(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreTrackedAndChanged(eq(bookingDTO))).willReturn(FALSE_BI_PREDICATE);
        setUpConsumerAndCondition(MembershipBookingTrackingProcessor::addTrackingCandidate, membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(eq(bookingDTO)), false);
        membershipBookingTrackingProcessor.process(MEMBERSHIP_ID);
        then(externalModuleManager).should(never()).getMembership(eq(MEMBERSHIP_ID));
        then(membershipTrackingBiPredicateProvider).should().isActivatedAndBookingFeesAreBalancedAndNotTracked(eq(bookingDTO));
        then(updater).should().balanceBooking(eq(bookingDTO), eq(membershipDTO));
        then(updater).should(never()).trackBookingAndUpdateBalance(any());
        then(updater).should(never()).deleteBookingTracking(any());
    }

    @Test
    public void testProcessFeesChangedDeleteAndTrackingSaveCandidate() {
        given(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreUnbalancedAndNotTracked(eq(bookingDTO))).willReturn(FALSE_BI_PREDICATE);
        given(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreTrackedAndChanged(eq(bookingDTO))).willReturn(TRUE_BI_PREDICATE);
        setUpConsumerAndCondition(MembershipBookingTrackingProcessor::addTrackingCandidate, membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(eq(bookingDTO)), true);
        membershipBookingTrackingProcessor.process(MEMBERSHIP_ID);
        then(membershipTrackingBiPredicateProvider).should().isActivatedAndBookingFeesAreBalancedAndNotTracked(eq(bookingDTO));
        then(externalModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(externalModuleManager).should(times(2)).getBookingTracking(eq(bookingDTO));
        then(updater).should().deleteBookingTracking(eq(bookingDTO));
        then(updater).should().trackBookingAndUpdateBalance(eq(bookingDTO));
        then(externalModuleManager).shouldHaveNoMoreInteractions();
    }

    @Test
    public void testProcessFeesChangedDeleteAndBookingUnbalanced() {
        given(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreUnbalancedAndNotTracked(eq(bookingDTO))).willReturn(TRUE_BI_PREDICATE);
        given(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreTrackedAndChanged(eq(bookingDTO))).willReturn(TRUE_BI_PREDICATE);
        setUpConsumerAndCondition(MembershipBookingTrackingProcessor::addTrackingCandidate, membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(eq(bookingDTO)), false);
        membershipBookingTrackingProcessor.process(BOOKING_ID);
        then(membershipTrackingBiPredicateProvider).should().isActivatedAndBookingFeesAreBalancedAndNotTracked(eq(bookingDTO));
        then(externalModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(externalModuleManager).should(times(2)).getBookingTracking(eq(bookingDTO));
        then(updater).should().deleteBookingTracking(eq(bookingDTO));
        then(updater).should().balanceBooking(eq(bookingDTO), any(MembershipDTO.class));
        then(updater).shouldHaveNoMoreInteractions();
        then(externalModuleManager).shouldHaveNoMoreInteractions();
    }

    @Test
    public void testProcessRebalanceNotContractBookingTrackingCandidate() {
        setUpConsumerAndCondition(MembershipBookingTrackingProcessor::addRebalanceNotContractBookingTrackingCandidate, membershipTrackingBiPredicateProvider.isActivatedAndBookingIsTracked(), true);
        membershipBookingTrackingProcessor.process(BOOKING_ID);
        then(externalModuleManager).should(never()).getMembership(eq(MEMBERSHIP_ID));
        then(membershipTrackingBiPredicateProvider).should().isActivatedAndBookingIsTracked();
        then(externalModuleManager).should().getBookingTracking(eq(bookingDTO));
        then(updater).should().deleteBookingTracking(eq(bookingDTO));
        then(externalModuleManager).shouldHaveNoMoreInteractions();
    }

    @Test
    public void testMembershipNotCompliantToAnyProcessing() {
        setUpConsumerAndCondition(MembershipBookingTrackingProcessor::addRebalanceNotContractBookingTrackingCandidate, membershipTrackingBiPredicateProvider.isActivatedAndBookingIsTracked(), false);
        given(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(any())).willReturn(FALSE_BI_PREDICATE);
        given(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreUnbalancedAndNotTracked(any())).willReturn(FALSE_BI_PREDICATE);
        setUpConsumerAndCondition(MembershipBookingTrackingProcessor::addTrackingCandidate, membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreTrackedAndChanged(any()), false);
        membershipBookingTrackingProcessor.process(MEMBERSHIP_ID);
        then(externalModuleManager).should(times(3)).getBookingTracking(eq(bookingDTO));
        then(externalModuleManager).shouldHaveNoMoreInteractions();
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testFailedGetMembership() {
        givenSuccessfulGetBookingTracking();
        setUpConsumerAndCondition(MembershipBookingTrackingProcessor::addTrackingCandidate, membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreTrackedAndChanged(eq(bookingDTO)), true);
        given(externalModuleManager.getMembership(eq(MEMBERSHIP_ID)))
                .willReturn(new ApiCallWrapperBuilder<MembershipDTO, Long>(ApiCall.Endpoint.GET_MEMBERSHIP, ApiCall.Result.FAIL).throwable(new Exception()).build());
        membershipBookingTrackingProcessor.process(MEMBERSHIP_ID);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testFailedGetTrackingBooking() {
        setUpConsumerAndCondition(MembershipBookingTrackingProcessor::addTrackingCandidate, membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(eq(bookingDTO)), true);
        given(externalModuleManager.getBookingTracking(eq(bookingDTO)))
                .willReturn(new ApiCallWrapperBuilder<BookingTrackingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING_TRACKING, ApiCall.Result.FAIL)
                        .throwable(new Exception()).build());
        membershipBookingTrackingProcessor.process(MEMBERSHIP_ID);
    }

    private void givenSuccessfulGetMembership() {
        MembershipDTO membershipDTO = MembershipDTO.builder().id(MEMBERSHIP_ID).build();
        ApiCallWrapper<MembershipDTO, Long> membershipApiCallWrapper = new ApiCallWrapperBuilder<MembershipDTO, Long>(ApiCall.Endpoint.GET_MEMBERSHIP, ApiCall.Result.SUCCESS).response(membershipDTO).build();
        given(externalModuleManager.getMembership(MEMBERSHIP_ID)).willReturn(membershipApiCallWrapper);
    }

    private void givenSuccessfulGetBookingTracking() {
        ApiCallWrapper<BookingTrackingDTO, BookingDTO> getTrackingApiCall = new ApiCallWrapperBuilder<BookingTrackingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING_TRACKING, ApiCall.Result.SUCCESS)
                .response(BookingTrackingDTO.builder().build())
                .build();
        given(externalModuleManager.getBookingTracking(eq(bookingDTO))).willReturn(getTrackingApiCall);
    }

    private void givenSuccessfulGetBookingTrackingNotTracked() {
        ApiCallWrapper<BookingTrackingDTO, BookingDTO> getTrackingApiCall = new ApiCallWrapperBuilder<BookingTrackingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING_TRACKING, ApiCall.Result.SUCCESS)
                .build();
        given(externalModuleManager.getBookingTracking(eq(bookingDTO))).willReturn(getTrackingApiCall);
    }

    private void setUpConsumerAndCondition(Consumer<MembershipBookingTrackingProcessor> membershipProcessorMethod, BiPredicate<MembershipDTO, BookingTrackingDTO> condition, boolean conditionResult) {
        given(condition).willReturn((membership, tracking) -> conditionResult);
        membershipProcessorMethod.accept(membershipBookingTrackingProcessor);
    }
}