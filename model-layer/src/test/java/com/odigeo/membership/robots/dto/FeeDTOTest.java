package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import com.odigeo.membership.robots.dto.booking.FeeDTO;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.testng.Assert.assertNull;

public class FeeDTOTest extends BeanTest<FeeDTO> {

    private static final String CURRENCY = "EUR";
    private static final String FEE_LABEL = "Label";
    private static final String SUB_CODE = "AE_11";
    private static final long FEE_ID = 1L;

    @Override
    protected FeeDTO getBean() {
        return FeeDTO.builder()
                .id(FEE_ID)
                .amount(BigDecimal.TEN)
                .creationDate(new Date())
                .currency(CURRENCY)
                .feeLabel(FEE_LABEL)
                .subCode(SUB_CODE)
                .build();
    }

    @Test
    public void testDateNull() {
        FeeDTO feeDto = FeeDTO.builder()
                .creationDate(null)
                .build();
        assertNull(feeDto.getCreationDate());
    }

    @Test
    public void feeDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(FeeDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
