package com.odigeo.membership.robots.consumer;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class CollectionSummaryChargebackNotificationsTest extends BeanTest<CollectionSummaryChargebackNotifications> {

    private static final Long BOOKING_ID = 12345l;
    private static final String ACTION = "CHARGEBACK";
    private static final String STATUS = "CHARGEBACKED";
    private static final Long EXECUTION_TIME_MILLIS = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    private static final Long STORAGE_TIME_MILLIS = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

    @Override
    protected CollectionSummaryChargebackNotifications getBean() {
        return CollectionSummaryChargebackNotifications.builder()
                .bookingId(BOOKING_ID)
                .action(ACTION)
                .status(STATUS)
                .executionTimeInMillis(EXECUTION_TIME_MILLIS)
                .storageTimeInMillis(STORAGE_TIME_MILLIS)
                .build();
    }

    @Test
    public void collectionSummaryChargebackNotificationsEqualsVerifierTest() {
        EqualsVerifier.forClass(CollectionSummaryChargebackNotifications.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
