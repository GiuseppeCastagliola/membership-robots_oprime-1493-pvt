package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;


public class BookingTrackingDTOTest extends BeanTest<BookingTrackingDTO> {

    @Override
    protected BookingTrackingDTO getBean() {
        return BookingTrackingDTO.builder()
                .membershipId(1L)
                .bookingId(1L)
                .perksAmount(BigDecimal.ZERO)
                .costFeeAmount(BigDecimal.ONE)
                .avoidFeeAmount(BigDecimal.ONE)
                .bookingDate(LocalDateTime.MIN)
                .build();
    }

    @Test
    public void bookingTrackingDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(BookingTrackingDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}