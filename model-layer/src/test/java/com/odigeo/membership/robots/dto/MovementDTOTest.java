package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import com.odigeo.membership.robots.dto.booking.MovementDTO;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class MovementDTOTest extends BeanTest<MovementDTO> {

    private static final Integer ID = 1;
    private static final String MOVEMENT_ACTION = "act";
    private static final String MOVEMENT_STATUS = "stat";

    @Override
    protected MovementDTO getBean() {
        return MovementDTO.builder()
                .action(MOVEMENT_ACTION)
                .status(MOVEMENT_STATUS)
                .build();
    }

    @Test
    public void movementDTOEqualsVerifierTest() {
        EqualsVerifier.forClass(MovementDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

}
