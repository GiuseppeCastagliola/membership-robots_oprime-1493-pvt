package com.odigeo.membership.robots.apicall;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.odigeo.membership.robots.apicall.ApiCall.Endpoint.EXPIRE_MEMBERSHIP;
import static com.odigeo.membership.robots.apicall.ApiCall.Endpoint.SEARCH_MEMBERSHIPS;
import static com.odigeo.membership.robots.apicall.ApiCall.Result.FAIL;
import static com.odigeo.membership.robots.apicall.ApiCall.Result.SUCCESS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ApiCallWrapperTest extends BeanTest<ApiCallWrapper<String, String>> {

    private static final String REQUEST = "request";
    private static final String RESPONSE = "response";
    private static final String MSG = "message";
    private static final String PREFIX = "prefix";
    public static final Exception EXCEPTION = new Exception();
    @Mock
    private Logger logger;

    @BeforeMethod
    public void setup() {
        openMocks(this);
    }

    @Override
    protected ApiCallWrapper<String, String> getBean() {
        return new ApiCallWrapperBuilder<String, String>(SEARCH_MEMBERSHIPS, FAIL)
                .throwable(EXCEPTION)
                .message(MSG)
                .request(REQUEST)
                .response(RESPONSE)
                .build();
    }

    @Test
    public void testErrorLog() {
        getBean().logResultAndMessageWithPrefix(logger, PREFIX);
        Mockito.verify(logger).error(anyString(), eq(PREFIX), eq(FAIL), eq(MSG), any(Exception.class));
    }

    @Test
    public void testSuccessfulLog() {
        final ApiCallWrapper<String, String> expireCallWrapper = new ApiCallWrapperBuilder<String, String>(EXPIRE_MEMBERSHIP, SUCCESS)
                .message(MSG)
                .response(RESPONSE)
                .request(REQUEST)
                .build();
        expireCallWrapper.logResultAndMessageWithPrefix(logger, PREFIX);
        Mockito.verify(logger).info(anyString(), eq(PREFIX), eq(SUCCESS), eq(MSG));
    }

    @Test
    public void testToString() {
        final String apiCallToString = getBean().toString();
        assertTrue(apiCallToString.contains(REQUEST) && apiCallToString.contains(RESPONSE)
                && apiCallToString.contains(FAIL.toString()) && apiCallToString.contains(MSG));

    }

    @Test
    public void testConstructors() {
        final ApiCallWrapper<String, String> apiCallWrapper = new ApiCallWrapperBuilder<String, String>(SEARCH_MEMBERSHIPS)
                .result(FAIL)
                .response(RESPONSE)
                .request(REQUEST)
                .message(MSG)
                .throwable(EXCEPTION)
                .build();
        assertEquals(apiCallWrapper, getBean());
    }

    @Test
    public void apiCallWrapperEqualsVerifierTest() {
        EqualsVerifier.forClass(ApiCallWrapper.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}