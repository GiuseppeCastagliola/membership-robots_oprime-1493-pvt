package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.testng.Assert.assertEquals;

public class MembershipDTOTest extends BeanTest<MembershipDTO> {

    public static final String INIT = "INIT";
    private static final LocalDateTime TEST_DATE = LocalDateTime.now();
    private static final String ENABLED_AUTO_RENEWAL = "ENABLED";
    private static final String EURO = "EUR";
    private static final long TEST_ID = 1L;
    private static final String MEMBERSHIP_TYPE = "BUSINESS";
    private static final String LAST_NAME = "Sanchez";
    public static final int MONTHS_DURATION = 3;
    public static final String NAME = "Pedro";
    public static final String RECURRING_ID = "NON_RECURRING";
    private static final String POST_BOOKING = "POST_BOOKING";
    private static final String PENDING_TO_COLLECT = "PENDING_TO_COLLECT";
    private static final String WEBSITE = "ES";


    @Override
    protected MembershipDTO getBean() {
        return MembershipDTO.builder()
                .activationDate(TEST_DATE)
                .autoRenewal(ENABLED_AUTO_RENEWAL)
                .balance(BigDecimal.TEN)
                .currencyCode(EURO)
                .expirationDate(TEST_DATE)
                .id(TEST_ID)
                .memberAccountId(TEST_ID)
                .membershipType(MEMBERSHIP_TYPE)
                .lastName(LAST_NAME)
                .monthsDuration(MONTHS_DURATION)
                .name(NAME)
                .productStatus(INIT)
                .recurringId(RECURRING_ID)
                .sourceType(POST_BOOKING)
                .timestamp(TEST_DATE)
                .status(PENDING_TO_COLLECT)
                .totalPrice(BigDecimal.ONE)
                .userCreditCardId(BigDecimal.ZERO)
                .userId(TEST_ID)
                .website(WEBSITE)
                .build();
    }

    @Test
    public void testCopyBuilder() {
        assertEquals(MembershipDTOBuilder.builderFromDto(getBean()).build(), getBean());
    }

    @Test
    public void membershipDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(MembershipDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}