package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.CollectionAttemptDTO;
import com.odigeo.membership.robots.dto.booking.FeeContainerDTO;
import com.odigeo.membership.robots.dto.booking.ProductCategoryBookingDTO;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

import static com.odigeo.membership.robots.dto.booking.BookingStatus.CONTRACT;
import static org.testng.Assert.assertFalse;

public class BookingDTOTest extends BeanTest<BookingDTO> {

    private static final String CURRENCY_CODE = "EUR";
    private static final long BOOKING_ID = 123L;
    private static final long MEMBERSHIP_ID = 321L;
    private static final String PRODUCT_TYPE_NAME = "MEMBERSHIP_RENEWAL";
    private static final long USER_ID = 543L;

    @Override
    protected BookingDTO getBean() {
        return BookingDTO.builder()
                .currencyCode(CURRENCY_CODE)
                .feeContainers(Collections.singletonList(FeeContainerDTO.builder().build()))
                .id(BOOKING_ID)
                .membershipId(MEMBERSHIP_ID)
                .productTypes(Collections.singletonList(PRODUCT_TYPE_NAME))
                .userId(USER_ID)
                .bookingStatus(CONTRACT.name())
                .testBooking(false)
                .bookingProducts(Collections.singletonList(ProductCategoryBookingDTO.builder().id(12L).productType("PRIME").feeContainerId(876L).build()))
                .collectionAttempts(Collections.singletonList(CollectionAttemptDTO.builder().build()))
                .bookingDate(LocalDateTime.MIN)
                .totalAvoidFeesAmount(BigDecimal.TEN)
                .totalCostFeesAmount(BigDecimal.ONE)
                .totalPerksAmount(BigDecimal.ZERO)
                .build();
    }

    @Test
    public void testIsTestBooking() {
        assertFalse(getBean().isTestBooking());
    }

    @Test
    public void bookingDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(BookingDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
