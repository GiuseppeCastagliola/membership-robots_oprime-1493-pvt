package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import com.odigeo.membership.robots.dto.booking.ProductCategoryBookingDTO;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class ProductCategoryBookingDTOTest extends BeanTest<ProductCategoryBookingDTO> {

    private static final long FEE_CONTAINER_ID = 123L;
    private static final long ID = 321L;
    private static final String PRODUCT_TYPE_NAME = "MEMBERSHIP_RENEWAL";


    @Override
    protected ProductCategoryBookingDTO getBean() {
        return ProductCategoryBookingDTO.builder()
                .feeContainerId(FEE_CONTAINER_ID)
                .id(ID)
                .productType(PRODUCT_TYPE_NAME)
                .price(BigDecimal.TEN)
                .build();
    }

    @Test
    public void productCategoryBookingDTOEqualsVerifierTest() {
        EqualsVerifier.forClass(ProductCategoryBookingDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
