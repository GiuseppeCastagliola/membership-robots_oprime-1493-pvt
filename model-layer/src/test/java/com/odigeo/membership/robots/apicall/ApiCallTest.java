package com.odigeo.membership.robots.apicall;

import bean.test.BeanTest;
import org.testng.annotations.Test;

import static com.odigeo.membership.robots.apicall.ApiCall.Endpoint.CREATE_PENDING_TO_COLLECT;
import static com.odigeo.membership.robots.apicall.ApiCall.Endpoint.GET_OFFER;
import static com.odigeo.membership.robots.apicall.ApiCall.Result.FAIL;
import static com.odigeo.membership.robots.apicall.ApiCall.Result.SUCCESS;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ApiCallTest extends BeanTest<ApiCall> {

    private static final String MSG = "message";

    @Override
    protected ApiCall getBean() {
        return new ApiCallWrapperBuilder<>(CREATE_PENDING_TO_COLLECT, SUCCESS)
                .throwable(new Exception())
                .message(MSG)
                .build();
    }

    @Test
    public void testIsSuccessful() {
        assertTrue(getBean().isSuccessful());
        assertFalse(new ApiCallWrapperBuilder<>(GET_OFFER, FAIL)
                .build().isSuccessful());
    }

    @Test
    public void testIsFailed() {
        assertFalse(getBean().isFailed());
        assertTrue(new ApiCallWrapperBuilder<>(GET_OFFER, FAIL)
                .build().isFailed());
    }
}