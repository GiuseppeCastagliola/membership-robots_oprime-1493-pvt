package com.odigeo.membership.robots.dto;

import org.apache.commons.lang.builder.EqualsBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

public class MembershipDTO {
    private final long id;
    private final String website;
    private final String status;
    private final String autoRenewal;
    private final long memberAccountId;
    private final LocalDateTime expirationDate;
    private final LocalDateTime activationDate;
    private final LocalDateTime timestamp;
    private final BigDecimal userCreditCardId;
    private final String membershipType;
    private final BigDecimal balance;
    private final Integer monthsDuration;
    private final String productStatus;
    private final BigDecimal totalPrice;
    private final String currencyCode;
    private final String sourceType;
    private final String recurringId;
    private final String name;
    private final String lastName;
    private final Long userId;

    protected MembershipDTO(MembershipDTOBuilder builder) {
        id = builder.id;
        userCreditCardId = builder.userCreditCardId;
        status = builder.status;
        totalPrice = builder.totalPrice;
        productStatus = builder.productStatus;
        recurringId = builder.recurringId;
        activationDate = builder.activationDate;
        expirationDate = builder.expirationDate;
        timestamp = builder.timestamp;
        monthsDuration = builder.monthsDuration;
        sourceType = builder.sourceType;
        balance = builder.balance;
        autoRenewal = builder.autoRenewal;
        memberAccountId = builder.memberAccountId;
        website = builder.website;
        membershipType = builder.membershipType;
        currencyCode = builder.currencyCode;
        name = builder.name;
        lastName = builder.lastName;
        userId = builder.userId;
    }

    public long getId() {
        return id;
    }

    public String getWebsite() {
        return website;
    }

    public String getStatus() {
        return status;
    }

    public String getAutoRenewal() {
        return autoRenewal;
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public LocalDateTime getActivationDate() {
        return activationDate;
    }

    public BigDecimal getUserCreditCardId() {
        return userCreditCardId;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Integer getMonthsDuration() {
        return monthsDuration;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getSourceType() {
        return sourceType;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public static MembershipDTOBuilder builder() {
        return new MembershipDTOBuilder();
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipDTO that = (MembershipDTO) o;
        return new EqualsBuilder()
                .append(id, that.id)
                .append(memberAccountId, that.memberAccountId)
                .append(website, that.website)
                .append(status, that.status)
                .append(autoRenewal, that.autoRenewal)
                .append(expirationDate, that.expirationDate)
                .append(activationDate, that.activationDate)
                .append(timestamp, that.timestamp)
                .append(userCreditCardId, that.userCreditCardId)
                .append(membershipType, that.membershipType)
                .append(balance, that.balance)
                .append(monthsDuration, that.monthsDuration)
                .append(productStatus, that.productStatus)
                .append(totalPrice, that.totalPrice)
                .append(currencyCode, that.currencyCode)
                .append(sourceType, that.sourceType)
                .append(recurringId, that.recurringId)
                .append(name, that.name)
                .append(lastName, that.lastName)
                .append(userId, that.userId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, website, status, autoRenewal, memberAccountId, expirationDate, activationDate, timestamp,
                userCreditCardId, membershipType, balance, monthsDuration, productStatus, totalPrice, currencyCode,
                sourceType, recurringId, name, lastName, userId);
    }
}
