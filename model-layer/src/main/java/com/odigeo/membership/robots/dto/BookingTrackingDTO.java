package com.odigeo.membership.robots.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

public class BookingTrackingDTO {

    private final long bookingId;
    private final long membershipId;
    private final LocalDateTime bookingDate;
    private final BigDecimal avoidFeeAmount;
    private final BigDecimal costFeeAmount;
    private final BigDecimal perksAmount;

    private BookingTrackingDTO(Builder builder) {
        this.avoidFeeAmount = builder.avoidFeeAmount;
        this.membershipId = builder.membershipId;
        this.perksAmount = builder.perksAmount;
        this.bookingId = builder.bookingId;
        this.costFeeAmount = builder.costFeeAmount;
        this.bookingDate = builder.bookingDate;
    }

    public static Builder builder() {
        return new Builder();
    }

    public long getBookingId() {
        return bookingId;
    }

    public long getMembershipId() {
        return membershipId;
    }

    public LocalDateTime getBookingDate() {
        return bookingDate;
    }

    public BigDecimal getAvoidFeeAmount() {
        return avoidFeeAmount;
    }

    public BigDecimal getCostFeeAmount() {
        return costFeeAmount;
    }

    public BigDecimal getPerksAmount() {
        return perksAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BookingTrackingDTO that = (BookingTrackingDTO) o;
        return bookingId == that.bookingId
                && membershipId == that.membershipId
                && Objects.equals(bookingDate, that.bookingDate) && Objects.equals(avoidFeeAmount, that.avoidFeeAmount)
                && Objects.equals(costFeeAmount, that.costFeeAmount) && Objects.equals(perksAmount, that.perksAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingId, membershipId, bookingDate, avoidFeeAmount, costFeeAmount, perksAmount);
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private long bookingId;
        private long membershipId;
        private LocalDateTime bookingDate;
        private BigDecimal avoidFeeAmount;
        private BigDecimal costFeeAmount;
        private BigDecimal perksAmount;

        public Builder bookingId(long bookingId) {
            this.bookingId = bookingId;
            return this;
        }

        public Builder membershipId(long membershipId) {
            this.membershipId = membershipId;
            return this;
        }

        public Builder bookingDate(LocalDateTime bookingDate) {
            this.bookingDate = bookingDate;
            return this;
        }

        public Builder avoidFeeAmount(BigDecimal avoidFeeAmount) {
            this.avoidFeeAmount = avoidFeeAmount;
            return this;
        }

        public Builder costFeeAmount(BigDecimal costFeeAmount) {
            this.costFeeAmount = costFeeAmount;
            return this;
        }

        public Builder perksAmount(BigDecimal perksAmount) {
            this.perksAmount = perksAmount;
            return this;
        }

        public BookingTrackingDTO build() {
            return new BookingTrackingDTO(this);
        }
    }
}
