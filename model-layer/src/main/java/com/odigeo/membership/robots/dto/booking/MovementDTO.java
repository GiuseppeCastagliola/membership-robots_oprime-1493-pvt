package com.odigeo.membership.robots.dto.booking;

import java.util.Objects;

public class MovementDTO {
    private final String status;
    private final String action;

    public String getStatus() {
        return status;
    }

    public String getAction() {
        return action;
    }

    public MovementDTO(Builder builder) {
        this.status = builder.status;
        this.action = builder.action;
    }

    public static Builder builder() {
        return new Builder();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private String status;
        private String action;

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder action(String action) {
            this.action = action;
            return this;
        }

        public MovementDTO build() {
            return new MovementDTO(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MovementDTO that = (MovementDTO) o;
        return Objects.equals(status, that.status) && Objects.equals(action, that.action);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, action);
    }
}
