package com.odigeo.membership.robots.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.StringJoiner;

public class SearchMembershipsDTO {
    private final LocalDate fromExpirationDate;
    private final LocalDate toExpirationDate;
    private final String status;
    private final String autoRenewal;
    private final BigDecimal minBalance;
    private final LocalDate fromActivationDate;
    private final boolean withMemberAccount;
    private final String website;
    private final SearchMemberAccountsDTO searchMemberAccountsDTO;

    private SearchMembershipsDTO(SearchMembershipsDTOBuilder builder) {
        autoRenewal = builder.autoRenewal;
        withMemberAccount = builder.withMemberAccount;
        fromExpirationDate = builder.fromExpirationDate;
        toExpirationDate = builder.toExpirationDate;
        status = builder.status;
        minBalance = builder.minBalance;
        fromActivationDate = builder.fromActivationDate;
        website = builder.website;
        searchMemberAccountsDTO = builder.searchMemberAccountsDTO;
    }

    public static SearchMembershipsDTOBuilder builder() {
        return new SearchMembershipsDTOBuilder();
    }

    public LocalDate getFromExpirationDate() {
        return fromExpirationDate;
    }

    public LocalDate getToExpirationDate() {
        return toExpirationDate;
    }

    public String getStatus() {
        return status;
    }

    public String getAutoRenewal() {
        return autoRenewal;
    }

    public LocalDate getFromActivationDate() {
        return fromActivationDate;
    }

    public BigDecimal getMinBalance() {
        return minBalance;
    }

    public boolean isWithMemberAccount() {
        return withMemberAccount;
    }

    public String getWebsite() {
        return website;
    }

    public SearchMemberAccountsDTO getSearchMemberAccountsDTO() {
        return searchMemberAccountsDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SearchMembershipsDTO that = (SearchMembershipsDTO) o;
        return new EqualsBuilder()
                .append(withMemberAccount, that.withMemberAccount)
                .append(fromExpirationDate, that.fromExpirationDate)
                .append(toExpirationDate, that.toExpirationDate)
                .append(status, that.status)
                .append(autoRenewal, that.autoRenewal)
                .append(minBalance, that.minBalance)
                .append(fromActivationDate, that.fromActivationDate)
                .append(website, that.website)
                .append(searchMemberAccountsDTO, that.searchMemberAccountsDTO)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(fromExpirationDate)
                .append(toExpirationDate)
                .append(status)
                .append(autoRenewal)
                .append(minBalance)
                .append(fromActivationDate)
                .append(withMemberAccount)
                .append(website)
                .append(searchMemberAccountsDTO)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SearchMembershipsDTO.class.getSimpleName() + "[", "]")
                .add("fromExpirationDate=" + fromExpirationDate)
                .add("toExpirationDate=" + toExpirationDate)
                .add("status='" + status + "'")
                .add("autoRenewal='" + autoRenewal + "'")
                .add("minBalance=" + minBalance)
                .add("fromActivationDate=" + fromActivationDate)
                .add("withMemberAccount=" + withMemberAccount)
                .add("website='" + website + "'")
                .add("searchMemberAccountsDTO=" + searchMemberAccountsDTO)
                .toString();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class SearchMembershipsDTOBuilder {
        private BigDecimal minBalance;
        private LocalDate fromExpirationDate;
        private LocalDate toExpirationDate;
        private String status;
        private String autoRenewal;
        private LocalDate fromActivationDate;
        private boolean withMemberAccount;
        private String website;
        private SearchMemberAccountsDTO searchMemberAccountsDTO;

        public SearchMembershipsDTOBuilder fromExpirationDate(LocalDate fromExpirationDate) {
            this.fromExpirationDate = fromExpirationDate;
            return this;
        }

        public SearchMembershipsDTOBuilder toExpirationDate(LocalDate toExpirationDate) {
            this.toExpirationDate = toExpirationDate;
            return this;
        }

        public SearchMembershipsDTOBuilder status(String status) {
            this.status = status;
            return this;
        }

        public SearchMembershipsDTOBuilder autoRenewal(String autoRenewal) {
            this.autoRenewal = autoRenewal;
            return this;
        }

        public SearchMembershipsDTOBuilder minBalance(BigDecimal minBalance) {
            this.minBalance = minBalance;
            return this;
        }

        public SearchMembershipsDTOBuilder withMemberAccount(boolean withMemberAccount) {
            this.withMemberAccount = withMemberAccount;
            return this;
        }

        public SearchMembershipsDTOBuilder fromActivationDate(LocalDate fromActivationDate) {
            this.fromActivationDate = fromActivationDate;
            return this;
        }

        public SearchMembershipsDTOBuilder website(String website) {
            this.website = website;
            return this;
        }

        public SearchMembershipsDTOBuilder searchMemberAccountsDTO(SearchMemberAccountsDTO searchMemberAccountsDTO) {
            this.searchMemberAccountsDTO = searchMemberAccountsDTO;
            return this;
        }

        public SearchMembershipsDTO build() {
            return new SearchMembershipsDTO(this);
        }

    }
}
