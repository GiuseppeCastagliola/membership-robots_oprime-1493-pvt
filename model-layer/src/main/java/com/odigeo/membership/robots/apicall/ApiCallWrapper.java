package com.odigeo.membership.robots.apicall;

import org.slf4j.Logger;

import java.util.Objects;
import java.util.StringJoiner;

public class ApiCallWrapper<T, U> extends ApiCall {
    private final T response;
    private final U request;

    public T getResponse() {
        return response;
    }

    public U getRequest() {
        return request;
    }

    @Override
    public void logResultAndMessageWithPrefix(Logger logger, String prefix) {
        if (isSuccessful()) {
            logger.info("{} {} {}", prefix, getResult(), getMessage());
        } else {
            logger.error("{} {} {}", prefix, getResult(), getMessage(), getThrowable());
        }
    }

    public ApiCallWrapper(ApiCallWrapperBuilder<T, U> builder) {
        super(builder.getResult(), builder.getThrowable(), builder.getMessage(), builder.getEndpointId());
        response = builder.getResponse();
        request = builder.getRequest();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ApiCallWrapper.class.getSimpleName() + "[", "]")
                .add("result=" + getResult())
                .add("message='" + getMessage() + "'")
                .add("response=" + response)
                .add("request=" + request)
                .toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && getClass() == o.getClass() && super.equals(o)) {
            ApiCallWrapper<?, ?> that = (ApiCallWrapper<?, ?>) o;
            return Objects.equals(response, that.response) && Objects.equals(request, that.request);
        }
        return false;
    }


    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), response, request);
    }
}
