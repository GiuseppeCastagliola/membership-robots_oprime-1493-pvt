package com.odigeo.membership.robots.dto.booking;

import java.util.Objects;

public class CollectionAttemptDTO {
    private final Integer id;
    private final String recurringId;
    private final MovementDTO lastMovement;

    public CollectionAttemptDTO(Builder builder) {
        this.recurringId = builder.recurringId;
        this.lastMovement = builder.lastMovement;
        this.id = builder.id;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public MovementDTO getLastMovement() {
        return lastMovement;
    }

    public Integer getId() {
        return id;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CollectionAttemptDTO that = (CollectionAttemptDTO) o;
        return Objects.equals(id, that.id)
                && Objects.equals(recurringId, that.recurringId)
                && Objects.equals(lastMovement, that.lastMovement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, recurringId, lastMovement);
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private MovementDTO lastMovement;
        private String recurringId;
        private Integer id;

        public Builder recurringId(String recurringId) {
            this.recurringId = recurringId;
            return this;
        }

        public Builder lastMovement(MovementDTO lastMovement) {
            this.lastMovement = lastMovement;
            return this;
        }

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public CollectionAttemptDTO build() {
            return new CollectionAttemptDTO(this);
        }
    }
}
