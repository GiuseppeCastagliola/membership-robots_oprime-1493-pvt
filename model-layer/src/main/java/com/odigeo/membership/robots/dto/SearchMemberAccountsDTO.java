package com.odigeo.membership.robots.dto;

import java.util.Objects;
import java.util.StringJoiner;

public class SearchMemberAccountsDTO {
    private final String firstName;
    private final String lastName;
    private final Long userId;

    public SearchMemberAccountsDTO(SearchMemberAccountsDTOBuilder builder) {
        userId = builder.userId;
        firstName = builder.firstName;
        lastName = builder.lastName;
    }

    public static SearchMemberAccountsDTOBuilder builder() {
        return new SearchMemberAccountsDTOBuilder();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SearchMemberAccountsDTO that = (SearchMemberAccountsDTO) o;
        return Objects.equals(firstName, that.firstName)
                && Objects.equals(lastName, that.lastName)
                && Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, userId);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SearchMemberAccountsDTO.class.getSimpleName() + "[", "]")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("userId=" + userId)
                .toString();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class SearchMemberAccountsDTOBuilder {
        private String lastName;
        private String firstName;
        private Long userId;

        public SearchMemberAccountsDTOBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public SearchMemberAccountsDTOBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public SearchMemberAccountsDTOBuilder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public SearchMemberAccountsDTO build() {
            return new SearchMemberAccountsDTO(this);
        }

    }

}
