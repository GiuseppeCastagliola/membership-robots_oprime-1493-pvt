package com.odigeo.membership.robots.apicall;

import org.slf4j.Logger;

import java.util.Objects;

public abstract class ApiCall {
    public enum Result {
        SUCCESS, FAIL
    }

    public enum Endpoint {
        SEARCH_MEMBERSHIPS, CREATE_PENDING_TO_COLLECT, EXPIRE_MEMBERSHIP, GET_OFFER, SEARCH_BOOKINGS, GET_BOOKING, UPDATE_BOOKING,
        BOOKING_PROCESS_REMNANT_FEE, CONSUME_MEMBERSHIP_REMNANT_BALANCE, PROCESS_REMNANT_FEE, GET_MEMBERSHIP, DISCARD_MEMBERSHIP,
        DEACTIVATE_MEMBERSHIP, ENABLE_AUTO_RENEWAL, SAVE_RECURRING_ID, ACTIVATE_MEMBERSHIP, GET_BOOKING_TRACKING, DELETE_BOOKING_TRACKING, TRACK_BOOKING
    }

    private final Endpoint endpoint;
    private final Result result;
    private final Throwable throwable;
    private final String message;

    protected ApiCall(Result result, Throwable throwable, String message, Endpoint endpoint) {
        this.result = result;
        this.throwable = throwable;
        this.message = message;
        this.endpoint = endpoint;
    }

    public Result getResult() {
        return result;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public String getMessage() {
        return message;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public boolean isSuccessful() {
        return result == Result.SUCCESS;
    }

    public boolean isFailed() {
        return result == Result.FAIL;
    }

    public abstract void logResultAndMessageWithPrefix(Logger logger, String prefix);

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ApiCall apiCall = (ApiCall) o;
        return endpoint == apiCall.endpoint
                && result == apiCall.result
                && Objects.equals(throwable, apiCall.throwable)
                && Objects.equals(message, apiCall.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(endpoint, result, throwable, message);
    }
}
