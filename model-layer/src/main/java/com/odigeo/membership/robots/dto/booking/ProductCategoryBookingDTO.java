package com.odigeo.membership.robots.dto.booking;

import java.math.BigDecimal;
import java.util.Objects;

public class ProductCategoryBookingDTO {
    private final Long id;
    private final String productType;
    private final Long feeContainerId;
    private final BigDecimal price;

    protected ProductCategoryBookingDTO(Builder builder) {
        id = builder.id;
        productType = builder.productType;
        feeContainerId = builder.feeContainerId;
        price = builder.price;
    }

    public Long getId() {
        return this.id;
    }

    public String getProductType() {
        return this.productType;
    }

    public Long getFeeContainerId() {
        return this.feeContainerId;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProductCategoryBookingDTO that = (ProductCategoryBookingDTO) o;
        return Objects.equals(id, that.id)
                && Objects.equals(feeContainerId, that.feeContainerId)
                && Objects.equals(productType, that.productType)
                && Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, feeContainerId, productType, price);
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static class Builder {

        private Long id;
        private String productType;
        private Long feeContainerId;
        private BigDecimal price;

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder productType(String productType) {
            this.productType = productType;
            return this;
        }

        public Builder feeContainerId(Long feeContainerId) {
            this.feeContainerId = feeContainerId;
            return this;
        }

        public Builder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public ProductCategoryBookingDTO build() {
            return new ProductCategoryBookingDTO(this);
        }
    }
}
