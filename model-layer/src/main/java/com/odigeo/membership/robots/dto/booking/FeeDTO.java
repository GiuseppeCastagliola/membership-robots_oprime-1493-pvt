
package com.odigeo.membership.robots.dto.booking;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.nonNull;

public class FeeDTO {
    private final long id;
    private final String feeLabel;
    private final String subCode;
    private final BigDecimal amount;
    private final String currency;
    private final Date creationDate;

    private FeeDTO(Builder builder) {
        this.id = builder.id;
        this.feeLabel = builder.feeLabel;
        this.subCode = builder.subCode;
        this.amount = builder.amount;
        this.currency = builder.currency;
        this.creationDate = builder.creationDate;
    }

    public long getId() {
        return id;
    }

    public String getFeeLabel() {
        return feeLabel;
    }

    public String getSubCode() {
        return subCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public Date getCreationDate() {
        return Optional.ofNullable(creationDate).map(date -> new Date(date.getTime()))
                .orElse(null);
    }

    public static Builder builder() {
        return new Builder();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private long id;
        private String feeLabel;
        private String subCode;
        private BigDecimal amount;
        private String currency;
        private Date creationDate;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder feeLabel(String feeLabel) {
            this.feeLabel = feeLabel;
            return this;
        }

        public Builder subCode(String subCode) {
            this.subCode = subCode;
            return this;
        }

        public Builder amount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder creationDate(Date creationDate) {
            if (nonNull(creationDate)) {
                this.creationDate = new Date(creationDate.getTime());
            }
            return this;
        }

        public FeeDTO build() {
            return new FeeDTO(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FeeDTO feeDTO = (FeeDTO) o;
        return id == feeDTO.id
                && Objects.equals(feeLabel, feeDTO.feeLabel) && Objects.equals(subCode, feeDTO.subCode)
                && Objects.equals(amount, feeDTO.amount) && Objects.equals(currency, feeDTO.currency)
                && Objects.equals(creationDate, feeDTO.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, feeLabel, subCode, amount, currency, creationDate);
    }
}
